package com.DarkAPI.data.enums;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.region.Players;

import java.util.Arrays;

public enum Quests {

    COOKS_ASSIST("Cook's Assistant", 1,false, 1, VarType.VARP, 29, 2, 0),
    X_MARKS_SPOT("X Marks the Spot", 2, false, 1, VarType.VARBIT, 8063, 8, 0),
    RESTLESS_GHOST("The Restless Ghost", 3, false, 1, VarType.VARP, 107, 5, 0),
    RUNE_MYSTERIES("Rune Mysteries", 4, false, 1, VarType.VARP, 63, 6, 0),
    IMP_CATCHER("Imp Catcher",5, false, 1, VarType.VARP, 160, 2, 0),
    WITCH_POTION("Witch's Potion", 6, false, 1, VarType.VARP, 67, 3, 0),
    CLIENT_OF_KOUREND("Client of Kourend", 7, 1, VarType.VARP, 1566, 1991, 0),
    DWARF_CANNON("Dwarf Cannon", 8, 1, VarType.VARP, 0, 11, 0),
    WATERFALL_QUEST("Waterfall Quest", 9, 1, VarType.VARP, 65, 10, 1),
    TREE_GNOME_VILLAGE("Tree Gnome Village", 10, 2, VarType.VARP, 111, 9, 1),
    MONKS_FRIEND("Monk's Friend", 11, 1, VarType.VARP, 30, 80, 0),
    HAZEEL_CULT("Hazeel Cult", 12, 1, VarType.VARP, 223, 9, 0),
    MURDER_MYSTERY("Murder Mystery", 13, 3, VarType.VARP, 192, 2, 0),
    MERLIN_CRYSTAL("Merlin's Crystal", 14, 6, VarType.VARP, 14, 7, 1),
    HOLY_GRAIL("Holy Grail", 15, 2, VarType.VARP, 5, 10, 1),
    DRUIDIC_RITUAL("Druidic Ritual", 16, 4, VarType.VARP, 80, 4, 0),
    WITCH_HOUSE("Witch's House", 17, 4, VarType.VARP, 226, 7, 1),
    BLACK_KNIGHT_FORTRESS("Black Knights' Fortress", 18, false, 3, VarType.VARP, 130, 4, 0),
    RECRUITMENT_DRIVE("Recruitment Drive", 19, 1, VarType.VARBIT, 657, 2, 0), //female only, determine logic to make female if need
    OBSERVATORY_QUEST("Observatory Quest", 20, 2, VarType.VARP, 112, 7, 0),
    PRIEST_PERIL("Priest in Peril", 21, 1, VarType.VARP, 302, 61, 0),
    RAG_BONE_MAN_I("Rag and Bone Man I", 22, 1, VarType.VARP, 714, 6, 0),
    NATURE_SPIRIT("Nature Spirit", 23, 2, VarType.VARP, 307, 110, 0),
    SCORPION_CATCHER("Scorpion Catcher", 24, 1, VarType.VARP, 76, 6, 0),
    PLAGUE_CITY("Plague City", 25, 1, VarType.VARP, 165, 30, 1),
    BIOHAZARD("Biohazard", 26, 3, VarType.VARP, 68, 16, 1),
    FIGHT_ARENA("Fight Arena", 27, 2, VarType.VARP, 17, 14, 2),
    GERTRUDE_CAT("Gertrude's cat", 28, 1, VarType.VARP, 180, 6, 0),
    JUNGLE_POTION("Jungle Potion", 29, 1, VarType.VARP, 175, 13, 0),
    VAMPYRE_SLAY("Vampyre Slayer", 30, 3, VarType.VARP, 178, 3, 0),
    //need to do
    PORCINE_OF_INTEREST("A Porcine of Interest", 31, 1, VarType.VARBIT, 10582, -1, 0),
    DEATH_PLATEAU("Death Plateau", 32, 1, VarType.VARP, 314, 80, 0),
    GOBLIN_DIPLOMACY("Goblin Diplomacy", 33, false, 5, VarType.VARBIT, 2378, 6, 0),
    QUEEN_THIEVES("The Queen of Thieves", 34, 1, VarType.VARBIT, 6037, 13, 0),
    DEPTHS_DESPAIR("The Depths of Despair", 35, 1, VarType.VARBIT, 6027, 11, 0), //Hosidius 20% favour req
    MOUNTAIN_DAUGHTER("Mountain Daughter", 36, 2, VarType.VARBIT, 260, 70, 1),
    LITTLE_HELPER("Icthlarin's Little Helper", 37, 2, VarType.VARBIT, 418, 26, 1),
    GRAND_TREE("The Grand Tree", 38, 5, VarType.VARP, 150, 160, 3),
    //returned 0 as complete
    TRIBAL_TOTEM("Tribal Totem", 39, 1, VarType.VARBIT, 200, -1, 1),
    DIG_SITE("The Dig Site", 40, 2, VarType.VARP, 131, 9, 1),
    THE_GOLEM("The Golem", 41, 1, VarType.VARBIT, 346, 10, 1),
    KNIGHT_SWORD("The Knight's sword", 42, false, 1, VarType.VARP, 122, 7, 1),
    //ELEMENTAL_I("Elemental Workshop I", 43, true, 1, -1, -1, false, 0, 0),
    //RFD_COOKS("RFD - Another Cooks Quest", 44, 1, VarType.VARBIT, 1850, -1, 5),
    //RFD_GOBLINS("RFD - Goblin Generals", 45, 1, VarType.VARBIT, 1850, -1, 5),
    DEMON_SLAYER("Demon Slayer", 46, false, 3, VarType.VARBIT, 2561, 3, 0),
    PRINCE_ALI_RESCUE("Prince Ali Rescue", 47, false, 3, VarType.VARP, 273, 110, 0),
    SHADOW_STORM("Shadow of the Storm", 48, 1, VarType.VARBIT, 1372, 125, 1),
    ELEMENTAL_II("Elemental Workshop II", 49, 1, VarType.VARBIT, 2639, 11, 1),
    LOST_CITY("Lost City", 50, 3,  VarType.VARP, 147, 6, 2),
    FAIRYTALE_I("Fairytale I : Growing pains", 51, 2, VarType.VARBIT, 1803, 90, 2),
    //SHIELD_ARRAV("Shield of Arrav", 52, false, 1, -1, -1, false, 0, 0), //cant add 2player
    FENKENSTRAIN("Creature of Frenkenstrain", 53, 2, VarType.VARP, 399, 9, 1),
    SOUL_BANE("A Soul's Bane", 54, 1, VarType.VARBIT, 2011, 13, 0),
    LOST_TRIBE("The Lost Tribe", 55, 1, VarType.VARBIT, 532, 12, 1),
    DEATH_DORGESHUUN("Death to the Dorgeshuun", 56, 1, VarType.VARBIT, 2258, 13, 1),
    GIANT_DWARF("The Giant Dwarf", 57, 2, VarType.VARBIT, 571, 50, 1),
    //returned 0
    SLICE_HAM("Another Slice of H.A.M.", 58, 1, VarType.VARBIT, 3350, -1, 1),
    MAKING_HISTORY("Making History", 59, 3, VarType.VARBIT, 1383, 4, 1),
    SEARCH_MYREQUE("In Search of Myreque", 60, 2, VarType.VARP, 387, 110, 0),
    SHADES_MORTTON("Shades of Mort'ton", 61, 3, VarType.VARP, 339, 85, 1),
    AID_MYREQUE("In Aid of Myreque", 62, 2, VarType.VARBIT, 1990, 430, 1),
    BONE_VOYAGE("Bone Voyage", 63, 1, VarType.VARBIT, 5795, 50, 1), //100 kudos to complete
    WANTED("Wanted!", 64, 1, VarType.VARBIT, 1051, 11, 1),
    THE_FEUD("The Feud", 65, 1, VarType.VARBIT, 334, 28, 1),
    TROLL_STRONGHOLD("Troll Stronghold", 66, 1, VarType.VARP, 317, 50, 2),
    TROLL_ROMANCE("Troll Romance", 67, 2, VarType.VARP, 385, 45, 2),
    DRAGON_SLAYER_I("Dragon Slayer I", 68, false, 2, VarType.VARP, 176, 10, 2),
    //returned 0
    HORROR_DEEP("Horror from the Deep", 69, 2, VarType.VARP, 34, -1, 2),
    ERNEST_CHICKEN("Ernest the Chicken", 70, false, 4, VarType.VARP, 32, 3, 0),
    ANIMAL_MAGNETISM("Animal Magnetism", 71, 1, VarType.VARBIT, 3185, 240, 1),
    SHILO_VILLAGE("Shilo Village", 72,  2, VarType.VARP, 116, 15, 2),
    DORIC_QUEST("Doric's Quest", 73, false, 1, VarType.VARP, 31, 100, 0),
    SPIRITS_ELID("Spirits of the Elid", 74,  2, VarType.VARBIT, 1444, 60, 1),
    DARK_HALLOWVALE("Darkness of Hallowvale", 75, 2, VarType.VARBIT, 2573, 320, 1),
    TOWER_LIFE("Tower of Life", 76, 2, VarType.VARBIT, 3337, 18, 0),
    FISH_CONTEST("Fishing Contest", 77,  1, VarType.VARP, 11, 5, 0),
    //RFD_DWARF("RFD - Dwarf", 78, 1, VarType.VARBIT, -1, -1, 5),
    GHOSTS_AHOY("Ghosts Ahoy", 79, 2, VarType.VARBIT, 217, 8, 0),
    FORGETTABLE_TALE("Forgettable Tale...", 80, 2, VarType.VARBIT, 822, 140, 1),
    GARDEN_TRANQUIL("Garden of Tranquility", 81, 2, VarType.VARBIT, 961, 60, 1),
    ENLIGHTENED_JOURNEY("Enlightened Journey", 82, 1, VarType.VARBIT, 2866, 200, 1),
    //RFD_EVIL_DAVE("RFD - Evil Dave", 83, 1, VarType.VARBIT, -1, -1, 5),
    CHOMPY_BIRD("Big Chompy Bird Hunting", 84, 2, VarType.VARP, 293, 65, 1),
    //returned 0
    ZOGRE_FLESH("Zogre Flesh Eaters", 85, 1, VarType.VARP, 487, -1, 1),
    //RFD_PIRATE("RFD - Pirate Pete", 86, 1, VarType.VARBIT, -1, -1, 5),
    TAI_BWO_WANNAI_TRIO("Tai Bwo Wannai Trio", 87, 2, VarType.VARP, 320, 6, 1),
    TOURIST_TRAP("The Tourist Trap", 88, 2, VarType.VARP, 197, 30, 1),
    EADGAR_RUSE("Eadgar's Ruse", 89, 1, VarType.VARP, 335, 110, 2),
    ARMS_ADVENTURE("My Arm's Big Adventure", 90, 1, VarType.VARBIT, 2790, 230, 1), //60% tai bwo favour
    FREM_TRIALS("The Fremennik Trials", 91, 3, VarType.VARP, 347, 10, 1),
    //returned 0
    FREM_ISLES("The Fremennik Isles", 92, 1, VarType.VARP, 3311, -1, 2),
    //RFD_LUMB_GUIDE("RFD - Lumbridge Guide", 93, 1, VarType.VARBIT, -1, -1, 5),
    //RFD_SKRACH("RFD - Skrach Uglogwee", 94, 1, VarType.VARBIT, -1, -1, 5),
    HAUNTED_MINE("Haunted Mine", 95, 2, VarType.VARP, 382, 11, 2),
    WATCHTOWER("Watchtower", 96, 4, VarType.VARP, 212, 14, 1),
    CONTACT("Contact!", 97, 1, VarType.VARBIT , 3274, 130, 3),
    EYES_GLOUPHRIE("The Eyes of Glouphrie", 98, 2, VarType.VARBIT, 2497, 60, 1),
    SEA_SLUG("Sea Slug", 99, 1, VarType.VARP, 159, 12, 1),
    //returned 0
    OLAF_QUEST("Olaf's Quest", 100, 1, VarType.VARBIT, 3354, -1, 1),
    TEARS_GUTHIX("Tears of Guthix", 101, 1, VarType.VARBIT, 451, 2, 1),
    RATCATCHERS("Ratcatchers", 102, 2, VarType.VARBIT, 1404, 127, 1),
    TEMPLE_IKOV("Temple of Ikov", 103, 1, VarType.VARP, 26, 80, 2),
    SMALL_FAVOUR("One Small Favour", 104, 2, VarType.VARP, 416, 285, 2),
    TAIL_TWO_CATS("A Tail of Two Cats", 105, 2, VarType.VARBIT, 1028, 70, 1),
    BETWEEN_ROCK("Between A Rock...", 106, 2, VarType.VARBIT, 299, 110, 2),
    SLUG_MENACE("The Slug Menace", 107, 1, VarType.VARBIT, 2610, 14, 1),
    MONKEY_MADNESS_I("Monkey Madness I", 108, 3, VarType.VARP, 365, 10, 3),
    //returned 0
    GETTING_AHEAD("Getting Ahead", 109, 1, VarType.VARBIT, 693, -1, 1),
    COLD_WAR("Cold War", 110, 1, VarType.VARBIT, 3293, 135, 1),
    //returned 0 - not done iircc
    ASCENT_ARCEUUS("The Ascent of Arceuus", 111, 1, VarType.VARBIT, 7856, -1, 0), //20% arceuus favour
    EAGLES_PEAK("Eagle's Peak", 112, 2, VarType.VARBIT, 2780, 40, 0),
    UNDERGROUND_PASS("Underground Pass", 113, 5, VarType.VARP, 161, 11, 1),
    RAG_BONE_MAN_II("Rag and Bone Man II", 114, 1, VarType.VARP, 714, 6, 1),
    RUM_DEAL("Rum Deal", 115, 2, VarType.VARP, 600, 19, 2),
    SHEEP_SHEAR("Sheep Shearer", 116, false, 1, VarType.VARP, 60, 3, 0),
    MISTHALIN_MYSTERY("Misthalin Mystery", 117, false, 1, VarType.VARBIT, 3468, 135, 0),
    PIRATE_TREASURE("Pirate's Treasure", 118, false, 2, VarType.VARP, 71, 4, 0),
    CABIN_FEVER("Cabin Fever", 119, 2, VarType.VARP, 655, 140, 2),
    BRAIN_ROBBERY("The Great Brain Robbery", 120, 2, VarType.VARP, 980, 130, 2),
    HAND_SAND("Hand in the Sand", 121, 1, VarType.VARBIT, 1527, 160, 1),
    ENAKHRA_LAMENT("Enakhra's Lament", 122, true, 2, VarType.VARBIT, 1560, 70, 2),
    HERO_QUEST("Hero's Quest", 123, 1, VarType.VARP, 188, 15, 2),//2 player
    THRONE_MISC("Throne of Miscellania", 124, 1, VarType.VARP, 359, 100, 5),
    ROYAL_TROUBLE("Royal Trouble", 125, 1, VarType.VARBIT, 2140, 30, 2),
    DESERT_TREASURE("Desert Treasure", 126, 3, VarType.VARBIT, 358, 15, 3),
    //returned 0
    TASTE_HOPE("A Taste of Hope", 127, 1, VarType.VARBIT, 6396, -1, 3),
    FAMILY_CREST("Family Crest", 128, 1, VarType.VARP, 148, 11, 2),
    LEGENDS_QUEST("Legends Quest", 129, 4, VarType.VARP, 139, 75, 3),
    //RFD_SIR_AMIK("RFD - Sir Amik Varze", 130, 1, VarType.VARBIT, -1, -1, 5),
    FAIRYTALE_II("Fairytale II - Cure a Queen", 131, 2, VarType.VARBIT, 2326, 100, 2),
    //RFD_AWOWOGEI("RFD - Awowogei", 132, 1, VarType.VARBIT, -1, -1, 5),
    REGICIDE("Regicide", 133, 3, VarType.VARP, 328, 15, 3),
    SHEEP_HERDER("Sheep Herder", 134, 4, VarType.VARP, 60, 3, 0),//not sure codeable
    ROVING_ELVES("Roving Elves", 135, 1, VarType.VARP, 403, 1490, 3),
    MEP_I("Mourning's End - Part 1", 136, 2, VarType.VARP, 517, 9, 3),
    MEP_II("Mourning's End - Part 2", 137, 2, VarType.VARBIT, 1103, 60, 3),
    LUNAR_DIPLOMACY("Lunar Diplomacy", 138, 2, VarType.VARBIT, 2448, 190, 2),
    WHAT_LIES_BELOW("What Lies Below", 139, 1, VarType.VARBIT, 3523, 150, 1),
    KINGS_RANSOM("King's Ransom", 140, 1, VarType.VARBIT, 3888, 90, 2),
    SWAN_SONG("Swan Song", 141, 2, VarType.VARBIT, 2098, 200, 3),
    //RFD_F_BATTLE("RFD - The Final Battle", 142, 1, VarType.VARBIT, -1, -1, 5),
    GRIM_TALES("Grim Tales", 143, 1, VarType.VARBIT, 2783, 60, 3),
    DREAM_MENTOR("Dream Mentor", 144, 2, VarType.VARBIT, 3618, 28, 3),
    //returned 0
    TALE_RIGHTEOUS("Tale of the Righteous", 145, 1, VarType.VARBIT, 6358, -1, 0),//20% shayzien req
    FORSAKEN_TOWER("The Forsaken Tower", 146, 1, VarType.VARBIT, 7796, -1, 0),//20% lova req

    DEVIOUS_MINDS("Devious Minds", 147, 1, VarType.VARBIT, 1465, 80, 2),
    //returned 0 (not done)
    FREM_EXILES("The Fremennik Exiles", 148, 2, VarType.VARBIT, 9459, -1, 3),
    SOTF("Sins of the Father", 149, 2, VarType.VARBIT, 7255, -1, 3),

    R_J("Romeo and Juliet", 150, false, 5, VarType.VARP, 144, 100, 0),
    //returned 0
    FRIEND_MY_ARM("Making Friends with My Arm", 151, 2, VarType.VARBIT, 6528, -1, 3),
    MONKEY_MADNESS_II("Monkey Madness II", 152, 4, VarType.VARBIT, 5027, 200, 4),
    DRAGON_SLAY_II("Dragon Slayer II", 153, 5, VarType.VARBIT, 6104, 215, 4),
    SOTE("Song of the Elves", 154, 4, VarType.VARBIT, 9016, 200, 4),
    CORSAIR_CURSE("The Corsair Curse", 155, false, 2, VarType.VARBIT, 6071, 60, 0),
    CLOCK_TOWER("Clock Tower", 156, 1, VarType.VARP, 10, 8, 0),
    //returned 0
    BELOW_ICE_MOUNTAIN("Below Ice Mountain", 157, false, 1, VarType.VARBIT, 12063, -1, 0),

    ;

    private final String questName;
    private final int id;
    private final boolean isP2P;
    private final int qpGain;
    private final VarType varType;
    private final int index;
    private final int finalValue;
    private final String difficulty;

    Quests(String questName, int id, int qpGain, VarType varType, int index, int finalValue, int diff) {
        this(questName, id, true, qpGain, varType, index, finalValue, diff);
    }

    Quests(String questName, int id, boolean isP2P, int qpGain, VarType varType, int index, int finalValue, int diff) {
        this.questName = questName;
        this.id = id;
        this.isP2P = isP2P;
        this.qpGain = qpGain;
        this.varType = varType;
        this.index = index;
        this.finalValue = finalValue;
        this.difficulty = setDiff(diff);
    }

    public static String[] getAll() {
        return Arrays.stream(values()).map(Quests::getQuestName).toArray(String[]::new);
    }

    public static Quests getQuest(String quest) {
        return Arrays.stream(values()).filter((val) -> val.getQuestName().equals(quest)).findFirst().orElse(null);
    }

    public Quest.Status getStatus() {
        int value;
        if (Players.getLocal() != null) {
            if (varType.equals(VarType.VARP)) {
                value = getVarpValue(index);
            } else {
                value = getVarbitValue(index);
            }
            return getValue(value, finalValue);
        }
        return Quest.Status.UNKNOWN;
    }

    private static Quest.Status getValue(int value, int finalValue) {
        if (value == -1)
            return Quest.Status.UNKNOWN;
        if (value == 0)
            return Quest.Status.NOT_STARTED;
        if (value == finalValue)
            return Quest.Status.COMPLETE;
        if (value > 0 && value < finalValue)
            return Quest.Status.IN_PROGRESS;
        return Quest.Status.UNKNOWN;
    }

    private static int getVarbitValue(int index) {
        Varbit varbit = Varbits.load(index);
        if (varbit != null)
            return varbit.getValue();
        return -1;
    }

    private static int getVarpValue(int index) {
        Varp varp = Varps.getAt(index);
        return varp.getValue();
    }

    public boolean isP2P() {
        return isP2P;
    }

    public int getFinalValue() {
        return finalValue;
    }

    public int getId() {
        return id;
    }

    public int getIndex() {
        return index;
    }

    public int getQpGain() {
        return qpGain;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public String getQuestName() {
        return questName;
    }

    public VarType getVarType() {
        return varType;
    }

    private static String setDiff(int value) {
        switch (value) {
            case 0:
                return "Novice";
            case 1:
                return "Intermediate";
            case 2:
                return "Experienced";
            case 3:
                return "Master";
            case 4:
                return "Grandmaster";
            case 5:
                return "Special";
        }
        return null;
    }

    public enum VarType {
        VARP,
        VARBIT
    }

}
