package com.DarkAPI.util;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.GameObjects;

import java.util.*;

public class VorkathArea {

    //Vork is 5x5
    private static Coordinate vorkathPos;
    private static Area.Rectangular vorkathArea;
    private static Area.Rectangular lootArea;
    private static Area.Rectangular southSide;
    private static Area.Rectangular fullArea;

    public static void buildArea(Coordinate vorkPos) {
        vorkathPos = vorkPos;
        vorkathArea = buildVorkathArea(vorkathPos);
        lootArea = buildLootArea(vorkathPos);
        setFullArea(buildFullArea(lootArea));
        southSide = buildSouth(getFullArea());
    }

    public static Area.Rectangular buildFightArea(Coordinate vorkPos) {
        buildArea(vorkPos);
        return fullArea;
    }

    public static boolean isInArea(Coordinate vorkPos, Player local) {
        if (vorkPos != null ) {
            buildArea(vorkPos);
            if (getFullArea() != null)
                return getFullArea().contains(local);
            return false;
        }
        return false;
    }

    public static Map.Entry<List<Coordinate>, Integer> getAcidPath(Coordinate vorkPos, Player local) {
        buildArea(vorkPos);
        return getBestPath(mergePaths(getPathsX(checkForAcid(getCoordinates(true))), getPathsY(checkForAcid(getCoordinates(false)))), local);
    }

    public static Map.Entry<List<Coordinate>, Integer> getAcidPath(Area.Rectangular fightArea, Player local) {
        setSouthSide(fightArea);
        return getBestPath(mergePaths(getPathsX(checkForAcid(getCoordinates(true))), getPathsY(checkForAcid(getCoordinates(false)))), local);
    }

    private static void setSouthSide(Area.Rectangular area) {
        southSide = area;
    }

    private static List<Coordinate> getCurrentRow(int y) {
        Coordinate[] coordinates = southSide.getCoordinates().stream().filter((val) -> val.getY() == y).toArray(Coordinate[]::new);
        return Arrays.asList(coordinates);
    }

    public static boolean isWalkRight(Coordinate vorkPos, Coordinate current, boolean isAcid) {
        buildArea(vorkPos);
        List<Coordinate> list = getCurrentRow(current.getY());
        int value = isAcid ? 9 : 5;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(current))
                return (21 - i) >= value;
        }
        return false;
    }

    private static Map.Entry<List<Coordinate>, Integer> getBestPath(HashMap<List<Coordinate>, Integer> paths, Player local) {
        HashMap<Map.Entry<List<Coordinate>, Integer>, Double> adjusted = new HashMap<>(0);
        adjusted.putAll(merge(paths, local));
        for (Map.Entry<Map.Entry<List<Coordinate>, Integer>, Double> entry : adjusted.entrySet()) {
            adjusted.put(entry.getKey(), adjustDist(entry));
        }
        return getClosestPath(adjusted);
    }

    private static Map.Entry<List<Coordinate>, Integer> getClosestPath(HashMap<Map.Entry<List<Coordinate>, Integer>, Double> start) {
        Double[] order = start.values().stream().sorted().toArray(Double[]::new);
        HashMap<Map.Entry<List<Coordinate>, Integer>, Double> temp = new HashMap<>(0);
        for (Double dub : order) {
            Map.Entry<Map.Entry<List<Coordinate>, Integer>, Double> entry = start.entrySet().stream().filter((val) -> val.getValue().equals(dub)).findFirst().get();
            temp.put(entry.getKey(), entry.getValue());
            start.remove(entry.getKey());
        }
        if (temp.size() > 0)
            return temp.entrySet().stream().findFirst().get().getKey();
        return null;
    }

    private static Double adjustDist(Map.Entry<Map.Entry<List<Coordinate>, Integer>, Double> entry) {
        int size = entry.getKey().getValue();
        size = size / 2;
        double temp = entry.getValue() - size;
        return temp < 0 ? 0.0 : temp;
    }

    private static HashMap<Map.Entry<List<Coordinate>, Integer>, Double> merge(HashMap<List<Coordinate>, Integer> start, Player player) {
        HashMap<Map.Entry<List<Coordinate>, Integer>, Double> temp = new HashMap<>(0);
        HashMap<List<Coordinate>, Integer> bySize = new HashMap<>(0);
        HashMap<List<Coordinate>, Double> byDist = new HashMap<>(0);
        HashMap<List<Coordinate>, Integer> clone = new HashMap<>(start);
        bySize.putAll(sortBySize(start));
        byDist.putAll(sortByDistance(clone, player));
        for (Map.Entry<List<Coordinate>, Integer> entry : bySize.entrySet()) {
            temp.put(entry, byDist.get(entry.getKey()));
        }
        return temp;
    }

    private static HashMap<List<Coordinate>, Integer> mergePaths(HashMap<List<Coordinate>, Integer> map1, HashMap<List<Coordinate>, Integer> map2) {
        HashMap<List<Coordinate>, Integer> temp = new HashMap<>(0);
        temp.putAll(map1);
        temp.putAll(map2);
        return temp;
    }

    private static HashMap<List<Coordinate>, Double> sortByDistance(HashMap<List<Coordinate>, Integer> start, Player local) {
        HashMap<List<Coordinate>, Double> temp = new HashMap<>(0);
        if (local != null) {
            for (List<Coordinate> list : start.keySet()) {
                Area.Rectangular area = new Area.Rectangular(list.get(0), list.get(list.size() - 1));
                double dist = area.getCenter().distanceTo(local.getPosition());
                temp.put(list, dist);
            }
        } else {
            String name = "VorkathArea";
            Environment.getLogger().warn("Fatal Error : local player null (" + name + "#sortByDistance)");
        }
        return ordered(temp);
    }

    private static HashMap<List<Coordinate>, Double> ordered(HashMap<List<Coordinate>, Double> start) {
        HashMap<List<Coordinate>, Double> temp = new HashMap<>(0);
        Double[] order = start.values().stream().sorted().toArray(Double[]::new);
        for (Double dub : order) {
            Map.Entry<List<Coordinate>, Double> entry = start.entrySet().stream().filter((val) -> val.getValue().equals(dub)).findFirst().get();
            temp.put(entry.getKey(), entry.getValue());
            start.remove(entry.getKey());
        }
        return temp;
    }

    private static HashMap<List<Coordinate>, Integer> sortBySize(HashMap<List<Coordinate>, Integer> start) {
        HashMap<List<Coordinate>, Integer> temp = new HashMap<>(0);
        Integer[] sizes = start.values().stream().sorted(Comparator.reverseOrder()).toArray(Integer[]::new);
        for (Integer size : sizes) {
            Map.Entry<List<Coordinate>, Integer> entry = start.entrySet().stream().filter((val) -> val.getValue().equals(size)).findFirst().get();
            temp.put(entry.getKey(), entry.getValue());
            start.remove(entry.getKey());
        }
        return temp;
    }

    private static HashMap<List<Coordinate>, Integer> getPathsX(HashMap<Coordinate, Boolean> holder) {
        HashMap<List<Coordinate>, Integer> xPaths = new HashMap<>(0);
        Integer[] yValues = getRows(holder);
        for (Integer y : yValues) {
            xPaths.putAll(buildPathsX(holder, y));
        }
        return xPaths;
    }

    private static HashMap<List<Coordinate>, Integer> buildPathsX(HashMap<Coordinate, Boolean> holder, Integer y) {
        HashMap<List<Coordinate>, Integer> xPaths = new HashMap<>(0);
        HashMap<Coordinate, Boolean> temp = new HashMap<>(0);
        for (Coordinate coordinate : holder.keySet()) {
            if (coordinate.getY() == y)
                temp.put(coordinate, holder.get(coordinate));
        }
        Coordinate[] walkable = sortByValuesX(temp.entrySet().stream().filter((val) -> !val.getValue()).map(Map.Entry::getKey).toArray(Coordinate[]::new));
        Coordinate prev = null;
        List<Coordinate> path = new ArrayList<>(0);
        for (Coordinate coordinate : walkable) {
            if (prev != null && prev.getX() + 1 != coordinate.getX()) {
                if (path.size() > 0) {
                    if (path.size() >= 3) {
                        xPaths.put(path, path.size());
                    }
                    path = new ArrayList<>();
                }
            } else {
                path.add(coordinate);
            }
            prev = coordinate;
        }
        return xPaths;
    }

    private static Coordinate[] sortByValuesX(Coordinate[] start) {
        return Arrays.stream(start).sorted(Comparator.comparingInt(Coordinate::getX)).toArray(Coordinate[]::new);
    }

    private static Integer[] getRows(HashMap<Coordinate, Boolean> all) {
        HashMap<Integer, Integer> yValues = new HashMap<>(0);
        for (Coordinate coordinate : all.keySet()) {
            yValues.put(coordinate.getY(), 0);
        }
        return yValues.keySet().stream().sorted().toArray(Integer[]::new);
    }

    private static HashMap<List<Coordinate>, Integer> getPathsY(HashMap<Coordinate, Boolean> holder) {
        HashMap<List<Coordinate>, Integer> yPaths = new HashMap<>(0);
        Integer[] xValues = getColumns(holder);
        for (Integer x : xValues) {
            yPaths.putAll(buildPathsY(holder, x));
        }
        return yPaths;
    }

    private static HashMap<List<Coordinate>, Integer> buildPathsY(HashMap<Coordinate, Boolean> holder, Integer x) {
        HashMap<List<Coordinate>, Integer> yPaths = new HashMap<>(0);
        HashMap<Coordinate, Boolean> temp = new HashMap<>(0);
        for (Coordinate coordinate : holder.keySet()) {
            if (coordinate.getX() == x)
                temp.put(coordinate, holder.get(coordinate));
        }
        Coordinate[] walkable = sortByValuesY(temp.entrySet().stream().filter((val) -> !val.getValue()).map(Map.Entry::getKey).toArray(Coordinate[]::new));
        Coordinate prev = null;
        List<Coordinate> path = new ArrayList<>(0);
        for (Coordinate coordinate : walkable) {
            if (prev != null && prev.getY() + 1 != coordinate.getY()) {
                if (path.size() > 0) {
                    if (path.size() >= 3)
                        yPaths.put(path, path.size());
                    path = new ArrayList<>();
                }
            } else {
                path.add(coordinate);
            }
            prev = coordinate;
        }
        return yPaths;
    }

    private static Coordinate[] sortByValuesY(Coordinate[] start) {
        return Arrays.stream(start).sorted(Comparator.comparingInt(Coordinate::getY)).toArray(Coordinate[]::new);
    }

    private static Integer[] getColumns(HashMap<Coordinate, Boolean> all) {
        HashMap<Integer, Integer> xValues = new HashMap<>(0);
        for (Coordinate coordinate : all.keySet()) {
            xValues.put(coordinate.getX(), 0);
        }
        return xValues.keySet().stream().sorted().toArray(Integer[]::new);
    }

    private static HashMap<Coordinate, Boolean> checkForAcid(HashMap<Coordinate, Boolean> temp) {
        LocatableEntityQueryResults<GameObject> pools = GameObjects.newQuery().within(southSide).ids(32000).results();
        Coordinate[] poolsPos = getCoordinates(pools);
        for (Coordinate coordinate : temp.keySet()) {
            if (Arrays.asList(poolsPos).contains(coordinate)) {
                temp.put(coordinate, true);
            }
        }
        return temp;
    }

    private static Coordinate[] getCoordinates(LocatableEntityQueryResults<GameObject> pools) {
        List<Coordinate> temp = new ArrayList<>(0);
        for (GameObject object : pools) {
            temp.add(object.getPosition());
        }
        return temp.toArray(new Coordinate[0]);
    }

    private static HashMap<Coordinate, Boolean> getCoordinates(boolean isByX) {
        HashMap<Coordinate, Boolean> map = new HashMap<>(0);
        List<Coordinate> zoneList = isByX ? sortedByX(getArea()) : sortedByY(getArea());
        for (Coordinate coordinate : zoneList) {
            map.put(coordinate, false);
        }
        return map;
    }

    private static List<Coordinate> sortedByX(List<Coordinate> coordinates) {
        List<Coordinate> list = new ArrayList<>(coordinates.size());
        Integer[] rows = getRowValuesY(coordinates);
        for (Integer y : rows) {
            list.addAll(buildRow(coordinates, y));
        }
        return list;
    }

    private static List<Coordinate> sortedByY(List<Coordinate> coordinates) {
        List<Coordinate> list = new ArrayList<>(coordinates.size());
        Integer[] rows = getColumnValuesX(coordinates);
        for (Integer x : rows) {
            list.addAll(buildColumn(coordinates, x));
        }
        return list;
    }

    private static List<Coordinate> buildRow(List<Coordinate> coordinates, Integer yValue) {
        List<Coordinate> list = new ArrayList<>(coordinates.size());
        HashMap<Coordinate, Integer> row = new HashMap<>(0);
        for (Coordinate coordinate : coordinates) {
            if (coordinate.getY() == yValue)
                row.put(coordinate, coordinate.getX());
        }
        Integer[] xValues = row.values().stream().sorted().toArray(Integer[]::new);
        for (Integer x : xValues) {
            list.add(new Coordinate(x, yValue, 0));
        }
        return list;
    }

    private static List<Coordinate> buildColumn(List<Coordinate> coordinates, Integer xValue) {
        List<Coordinate> list = new ArrayList<>(coordinates.size());
        HashMap<Coordinate, Integer> column = new HashMap<>(0);
        for (Coordinate coordinate : coordinates) {
            if (coordinate.getX() == xValue)
                column.put(coordinate, coordinate.getY());
        }
        Integer[] yValues = column.values().stream().sorted().toArray(Integer[]::new);
        for (Integer y : yValues) {
            list.add(new Coordinate(xValue, y, 0));
        }
        return list;
    }

    private static Integer[] getRowValuesY(List<Coordinate> input) {
        HashMap<Integer, String> rows = new HashMap<>(0);
        for (Coordinate coordinate : input) {
            int y = coordinate.getY();
            rows.put(y, "Y");
        }
        return rows.keySet().stream().sorted().toArray(Integer[]::new);
    }

    private static Integer[] getColumnValuesX(List<Coordinate> input) {
        HashMap<Integer, String> columns = new HashMap<>(0);
        for (Coordinate coordinate : input) {
            int x = coordinate.getX();
            columns.put(x, "X");
        }
        return columns.keySet().stream().sorted().toArray(Integer[]::new);
    }

    private static List<Coordinate> getArea() {
        return southSide.getCoordinates();
    }

    private static Area.Rectangular buildVorkathArea(Coordinate pos) {
        return new Area.Rectangular(pos, pos.derive(+4, +4));
    }

    private static Area.Rectangular buildLootArea(Coordinate pos) {
        return new Area.Rectangular(pos.derive(-1, -1), pos.derive(+5, +5));
    }

    private static Area.Rectangular buildFullArea(Area.Rectangular lootArea) {
        Coordinate pos1 = lootArea.getBottomLeft().derive(-7, -7);
        Coordinate pos2 = lootArea.getTopRight().derive(+7, +7);
        return new Area.Rectangular(pos1, pos2);
    }

    private static Area.Rectangular buildSouth(Area.Rectangular fullArea) {
        Coordinate pos1 = fullArea.getBottomLeft();
        Coordinate pos2 = fullArea.getTopRight().derive(0, -13);
        return new Area.Rectangular(pos1, pos2);
    }

    public static Area.Rectangular getFullArea() {
        return fullArea;
    }

    public static void setFullArea(Area.Rectangular fullArea) {
        VorkathArea.fullArea = fullArea;
    }
}
