package com.DarkAPI.util;

import com.runemate.game.api.hybrid.local.Rune;
import com.runemate.game.api.osrs.local.RunePouch;

public class Pouch {

    public static boolean contains(String rune){
        String slotRune = "";
        Rune rune1;
        boolean slotone = false;
        boolean slottwo = false;
        boolean slotthree = false;
        if (RunePouch.getRune(RunePouch.Slot.ONE) != null){
            rune1 = RunePouch.getRune(RunePouch.Slot.ONE);
            if (rune1 != null)
                slotRune = rune1.getName();
            slotone = slotRune.equals(rune);
        }
        if (RunePouch.getRune(RunePouch.Slot.TWO) != null){
            rune1 = RunePouch.getRune(RunePouch.Slot.TWO);
            if (rune1 != null)
                slotRune = rune1.getName();
            slottwo = slotRune.equals(rune);
        }
        if (RunePouch.getRune(RunePouch.Slot.THREE) != null){
            rune1 = RunePouch.getRune(RunePouch.Slot.THREE);
            if (rune1 != null)
                slotRune = rune1.getName();
            slotthree = slotRune.equals(rune);
        }
        return slotone || slottwo || slotthree;
    }
}
