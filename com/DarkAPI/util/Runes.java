package com.DarkAPI.util;

import com.DarkAPI.data.enums.Combination;
import com.DarkAPI.data.enums.Spells;
import com.DarkAPI.data.enums.Staff;
import com.DarkAPI.data.enums.Standard;
import com.runemate.game.api.hybrid.util.Regex;

import java.util.regex.Pattern;

public class Runes {

    public Standard standard;
    public Combination combination;
    private Staff staff;
    private Spells spell;

    public Runes(Standard rune) {
        this.standard = rune;
    }

    public Runes(Combination rune) {
        this.combination = rune;
    }

    public String getRuneName() {
        if (combination != null)
            return combination.getRuneName();
        return standard.getRuneName();
    }

    public Pattern getRunePattern() {
        if (combination != null)
            return Regex.getPatternForExactString(combination.getRuneName());
        return Regex.getPatternForExactString(standard.getRuneName());
    }

}
