package com.DarkAPI.util;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.Varbit;
import com.runemate.game.api.hybrid.local.Varbits;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceContainers;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.queries.results.InterfaceComponentQueryResults;
import com.runemate.game.api.osrs.local.hud.interfaces.ControlPanelTab;
import com.runemate.game.api.script.Execution;

/**
 * @author Party
 * @version 1.0
 **/
public enum AutoCast {

    WIND_STRIKE(1, "Wind Strike"),
    WATER_STRIKE(2, "Water Strike"),
    EARTH_STRIKE(3, "Earth Strike"),
    FIRE_STRIKE(4, "Fire Strike"),
    WIND_BOLT(5, "Wind Bolt"),
    WATER_BOLT(6, "Water Bolt"),
    EARTH_BOLT(7, "Earth Bolt"),
    FIRE_BOLT(8, "Fire Bolt"),
    WIND_BLAST(9, "Wind Blast"),
    WATER_BLAST(10, "Water Blast"),
    EARTH_BLAST(11, "Earth Blast"),
    FIRE_BLAST(12, "Fire Blast"),
    WIND_WAVE(13, "Wind Wave"),
    WATER_WAVE(14, "Water Wave"),
    EARTH_WAVE(15, "Earth Wave"),
    FIRE_WAVE(16, "Fire Wave"),
    IBAN_BLAST(47, "Iban Blast"),
    SLAYER_DART(18, "Slayer Dart");

    private final int value;
    private final String name;

    AutoCast(int value) {
        this.value = value;
        this.name = getName(value);
    }
    AutoCast(int value, String name) {
        this.name = name;
        this.value = value;
    }

    public static boolean isOpen() {
        return !InterfaceContainers.getLoaded(201).isEmpty();
    }

    public static boolean open() {
        if (isOpen()) {
            return true;
        }
        if (!ControlPanelTab.COMBAT_OPTIONS.isOpen()) {
            ControlPanelTab.COMBAT_OPTIONS.open();
            return false;
        }
        final InterfaceComponentQueryResults results = Interfaces.newQuery().containers(593).actions("Choose spell").results();
        InterfaceComponent button = null;
        Environment.getLogger().debug("Found components: " + results);
        for (final InterfaceComponent result : results) {
            if (button == null || button.getIndex() < result.getIndex()) {
                button = result;
            }
        }
        return button != null && button.interact("Choose spell");
    }

    public static boolean isAutoCasting() {
        final Varbit value = Varbits.load(275);
        return value != null && value.getValue() == 1;
    }

    public static AutoCast getAutoCastedSpell() {
        final Varbit varbit = Varbits.load(276);
        final int value = varbit == null ? 0 : varbit.getValue();
        for (final AutoCast cast : values()) {
            if (cast.value == value) {
                return cast;
            }
        }
        return null;
    }

    public static boolean canAutoCast() {
        final Varbit varbit = Varbits.load(357);
        return varbit != null && varbit.getValue() == 18;
    }

    public boolean select() {
        if (isSelected()) {
            return true;
        }
        if (!isOpen()) {
            open();
            return false;
        }
        final InterfaceComponent component = getComponent();
        return component != null && component.interact(name) && Execution.delayUntil(this::isSelected, 600, 1200);
    }

    private InterfaceComponent getComponent() {
        return Interfaces.newQuery().containers(201).actions(name).results().first();
    }

    public String getName(int value) {
        for (AutoCast cast : values()) {
            if (cast.value == value)
                return cast.name;
        }
        return null;
    }

    public boolean isSelected() {
        final Varbit varbit = Varbits.load(276);
        return varbit != null && varbit.getValue() == value;
    }
}