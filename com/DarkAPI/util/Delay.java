package com.DarkAPI.util;

import com.regal.utility.awesome_navigation.playersense.CustomPlayerSense;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Varbit;
import com.runemate.game.api.hybrid.local.Varbits;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class Delay {

    public static final int reactmin = CustomPlayerSense.Key.DARK_REACTION_MIN.getAsInteger();
    public static final int reactmax = CustomPlayerSense.Key.DARK_REACTION_MAX.getAsInteger();
    public static final int onetick = CustomPlayerSense.Key.DARK_TWO_TICK.getAsInteger();
    public static final int twotick = CustomPlayerSense.Key.DARK_TWO_TICK.getAsInteger();
    public static final int threetick = CustomPlayerSense.Key.DARK_THREE_TICk.getAsInteger();
    public static final int fourtick = CustomPlayerSense.Key.DARK_FOUR_TICK.getAsInteger();
    public static final int fivetick = CustomPlayerSense.Key.DARK_FIVE_TCIK.getAsInteger();
    public static final int sixtick = CustomPlayerSense.Key.DARK_SIX_TICK.getAsInteger();
    public static final int afkmin = CustomPlayerSense.Key.DARK_MIN_AFK.getAsInteger();
    public static final int afkmax = CustomPlayerSense.Key.DARK_MAX_AFK.getAsInteger();


    public static Callable<Boolean> afk(){
        return ()-> Execution.delay(afkmin, afkmax);
    }

    public static Callable<Boolean> untilVarbit(int varbit, int endValue){
        Player local = Players.getLocal();
        Varbit bit = Varbits.load(varbit);
        if (local != null && bit != null)
            return ()-> Execution.delayUntil(()-> bit.getValue() == endValue, ()-> local.isMoving() || local.getAnimationId() != -1, twotick, threetick);
        return ()-> Execution.delay(onetick, twotick);
    }
    public static Callable<Boolean> untilArea(Area area){
        Player local = Players.getLocal();
        if (local != null)
            return ()-> Execution.delayUntil(()-> area.contains(local), local::isMoving, threetick,fourtick);
        return ()-> Execution.delay(onetick,twotick);
    }
    public static Callable<Boolean> herbClean(String name){
        return () -> Execution.delayUntil(()-> !Inventory.contains(name) || ChatDialog.isOpen(), 35000);
    }
    public static Callable<Boolean> bankOpen(){
        Player local = Players.getLocal();
        return ()-> Execution.delayUntil(Bank::isOpen, local::isMoving, threetick,fourtick);
    }
    public static Callable<Boolean> bankDeposit(String item){
        return ()-> Execution.delayUntil(()-> !Inventory.contains(item),reactmin,reactmax);
    }
    public static Callable<Boolean> bankDeposit(Pattern item){
        return ()-> Execution.delayUntil(()-> !Inventory.contains(item),reactmin,reactmax);
    }
    public static Callable<Boolean> bankDeposit(int item){
        return ()-> Execution.delayUntil(()-> !Inventory.contains(item),reactmin,reactmax);
    }
    public static Callable<Boolean> bankWithdraw(String item){
        return ()-> Execution.delayUntil(()-> Inventory.contains(item),reactmin,reactmax);
    }
    public static Callable<Boolean> bankWithdraw(Pattern item){
        return ()-> Execution.delayUntil(()-> Inventory.contains(item),reactmin,reactmax);
    }
    public static Callable<Boolean> bankWithdraw(int item){
        return ()-> Execution.delayUntil(()-> Inventory.contains(item),reactmin,reactmax);
    }
    public static Callable<Boolean> bankWithdraw(String[] items){
        return ()-> Execution.delayUntil(()-> Inventory.containsAnyOf(items),reactmin,reactmax);
    }
    public static Callable<Boolean> bankWithdraw(String item, int amount){
        return ()-> Execution.delayUntil(()-> Inventory.getQuantity(item) >= amount,reactmin,reactmax);
    }
    public static Callable<Boolean> bankWithdraw(Pattern item, int amount){
        return ()-> Execution.delayUntil(()-> Inventory.getQuantity(item) >= amount,reactmin,reactmax);
    }
    public static Callable<Boolean> bankWithdraw(int item, int amount){
        return ()-> Execution.delayUntil(()-> Inventory.getQuantity(item) >= amount,reactmin,reactmax);
    }
    public static Callable<Boolean> reaction(){
        return ()-> Execution.delay(reactmin,reactmax);
    }
    public static Callable<Boolean> animating2T(String item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, twotick,threetick);
    }
    public static Callable<Boolean> animating2T(Pattern item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, twotick,threetick);
    }
    public static Callable<Boolean> animating2T(int item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, twotick,threetick);
    }
    public static Callable<Boolean> animating2T(String item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, twotick,threetick);
    }
    public static Callable<Boolean> animating2T(Pattern item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, twotick,threetick);
    }
    public static Callable<Boolean> animating2T(int item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, twotick,threetick);
    }
    public static Callable<Boolean> animating3T(){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()-> ChatDialog.getContinue()!=null, ()-> player.getAnimationId() != -1 || player.isMoving(), threetick,fourtick);
    }
    public static Callable<Boolean> animating3T(String item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.getContinue() != null, ()-> player.getAnimationId() != -1, threetick,fourtick);
    }
    public static Callable<Boolean> animating3T(Pattern item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, threetick,fourtick);
    }
    public static Callable<Boolean> animating3T(int item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, threetick,fourtick);
    }
    public static Callable<Boolean> animating3T(String item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, threetick,fourtick);
    }
    public static Callable<Boolean> animating3T(Pattern item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, threetick,fourtick);
    }
    public static Callable<Boolean> animating3T(int item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, threetick,fourtick);
    }
    public static Callable<Boolean> animating4T(String item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, fourtick, fivetick);
    }
    public static Callable<Boolean> animating4T(Pattern item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, fourtick, fivetick);
    }
    public static Callable<Boolean> animating4T(int item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, fourtick, fivetick);
    }
    public static Callable<Boolean> animating4T(String item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, fourtick,fivetick);
    }
    public static Callable<Boolean> animating4T(Pattern item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, fourtick,fivetick);
    }
    public static Callable<Boolean> animating4T(int item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1, fourtick,fivetick);
    }
    public static Callable<Boolean> animating5T(String item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, fivetick, sixtick);
    }
    public static Callable<Boolean> animating5T(Pattern item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, fivetick, sixtick);
    }
    public static Callable<Boolean> animating5T(int item){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->!Inventory.contains(item) || ChatDialog.isOpen(),()-> player.getAnimationId() != -1, fivetick, sixtick);
    }
    public static Callable<Boolean> animating5T(String item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1,fivetick,sixtick);
    }
    public static Callable<Boolean> animating5T(Pattern item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1,fivetick,sixtick);
    }
    public static Callable<Boolean> animating5T(int item, int amount){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->Inventory.getQuantity(item) < amount || ChatDialog.isOpen(), ()->player.getAnimationId() != -1,fivetick,sixtick);
    }
    public static Callable<Boolean> ticks1(){
        return ()-> Execution.delay(onetick);
    }
    public static Callable<Boolean> ticks2(){
        return ()-> Execution.delay(twotick);
    }
    public static Callable<Boolean> ticks3(){
        return ()-> Execution.delay(threetick);
    }
    public static Callable<Boolean> ticks4(){
        return ()-> Execution.delay(fourtick);
    }
    public static Callable<Boolean> ticks5(){
        return ()-> Execution.delay(fivetick);
    }
    public static Callable<Boolean> ticks6(){
        return ()-> Execution.delay(sixtick);
    }
    public static Callable<Boolean> teleportArea(Area area){
        Player player = Players.getLocal();
        return ()-> Execution.delayUntil(()->area.contains(player),()-> player.getAnimationId() != -1 || player.isMoving(),fourtick,fivetick);
    }
}
