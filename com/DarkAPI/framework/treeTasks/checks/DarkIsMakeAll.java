package com.DarkAPI.framework.treeTasks.checks;

import com.DarkAPI.framework.treeTasks.executable.DarkMakeAll;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.osrs.local.hud.interfaces.MakeAllInterface;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class DarkIsMakeAll extends BranchTask {

    private final Pattern pattern;
    private final int quantity;
    private final Callable<Boolean> delay;
    private final TreeTask treeTask;

    public DarkIsMakeAll(int quantity, String string, Callable<Boolean> delay, TreeTask treeTask) {
        this(quantity, toPattern(string), delay, treeTask);
    }

    public DarkIsMakeAll(int quantity, Pattern pattern, Callable<Boolean> delay, TreeTask treeTask) {
        this.quantity = quantity;
        this.pattern = pattern;
        this.delay = delay;
        this.treeTask = treeTask;
    }

    private static Pattern toPattern(String string) {
        return Regex.getPatternForExactString(string);
    }

    @Override
    public boolean validate() {
        return MakeAllInterface.isOpen();
    }

    @Override
    public TreeTask successTask() {
        return new DarkMakeAll(quantity, pattern, delay);
    }

    @Override
    public TreeTask failureTask() {
        return treeTask;
    }
}
