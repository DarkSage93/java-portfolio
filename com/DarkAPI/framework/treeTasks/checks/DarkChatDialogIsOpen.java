package com.DarkAPI.framework.treeTasks.checks;

import com.DarkAPI.framework.treeTasks.executable.DarkChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

public class DarkChatDialogIsOpen extends BranchTask {

    private final String[] options;
    private final TreeTask failTask;

    public DarkChatDialogIsOpen(TreeTask failTask, String... options) {
        this.options = options;
        this.failTask = failTask;
    }

    @Override
    public boolean validate() {
        return ChatDialog.isOpen() || ChatDialog.getContinue() != null || ChatDialog.getOptions().size() > 0;
    }

    @Override
    public TreeTask successTask() {
        return new DarkChatDialog(options);
    }

    @Override
    public TreeTask failureTask() {
        return failTask;
    }
}
