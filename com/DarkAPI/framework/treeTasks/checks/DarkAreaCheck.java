package com.DarkAPI.framework.treeTasks.checks;

import com.DarkAPI.framework.treeTasks.executable.DarkTraversal;
import com.regal.regal_bot.RegalBot;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

public class DarkAreaCheck extends BranchTask {

    private final Area area;
    private final Coordinate coordinate;
    private final String areaName;
    private final TreeTask treeTask;
    private final boolean isWebUsed;
    private final RegalBot bot;

    public DarkAreaCheck(Coordinate coordinate, String areaName, boolean isWebUsed, RegalBot bot, TreeTask treeTask) {
        this(null, coordinate, areaName, isWebUsed, bot, treeTask);
    }

    public DarkAreaCheck(Coordinate coordinate, String areaName, RegalBot bot, TreeTask treeTask) {
        this(null, coordinate, areaName, true, bot, treeTask);
    }

    public DarkAreaCheck(Area area, String areaName, boolean isWebUsed, RegalBot bot, TreeTask treeTask) {
        this(area, null, areaName, isWebUsed, bot, treeTask);
    }

    public DarkAreaCheck(Area area, String areaName, RegalBot bot, TreeTask treeTask) {
        this(area, null, areaName, true, bot, treeTask);
    }

    public DarkAreaCheck(Area area, Coordinate coordinate, String areaName, boolean isWebUsed, RegalBot bot, TreeTask treeTask) {
        this.area = area;
        this.coordinate = coordinate;
        this.areaName = areaName;
        this.isWebUsed = isWebUsed;
        this.bot = bot;
        this.treeTask = treeTask;
    }

    @Override
    public boolean validate() {
        Player local = Players.getLocal();
        if (local != null) {
            if (coordinate != null)
                return coordinate.equals(local.getPosition());
            return area.contains(local);
        }
        getLogger().warn("Player was null...");
        return false;
    }

    @Override
    public TreeTask successTask() {
        return treeTask;
    }

    @Override
    public TreeTask failureTask() {
        if (coordinate != null)
            return new DarkTraversal(coordinate, areaName, isWebUsed, bot);
        return new DarkTraversal(area, areaName, isWebUsed, bot);
    }
}
