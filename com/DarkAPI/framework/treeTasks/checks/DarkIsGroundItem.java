package com.DarkAPI.framework.treeTasks.checks;

import com.DarkAPI.framework.treeTasks.executable.DarkTakeGroundItem;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.GroundItemQueryBuilder;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

import java.util.Arrays;
import java.util.regex.Pattern;

public class DarkIsGroundItem extends BranchTask {

    private final TreeTask failTask;
    private final Pattern[] patterns;
    private final int[] ids;
    private final Coordinate coordinate;

    public DarkIsGroundItem(Coordinate coordinate, TreeTask failTask) {
        this(null, null, coordinate, failTask);
    }

    public DarkIsGroundItem(int[] ids, TreeTask failTask) {
        this(null, ids, null, failTask);
    }

    public DarkIsGroundItem(int[] ids, Coordinate coordinate, TreeTask failTask) {
        this(null, ids, coordinate, failTask);
    }

    public DarkIsGroundItem(int id, TreeTask failTask) {
        this(null, toArray(id), null, failTask);
    }

    public DarkIsGroundItem(int id, Coordinate coordinate, TreeTask failTask) {
        this(null, toArray(id), coordinate, failTask);
    }

    public DarkIsGroundItem(Pattern[] patterns, TreeTask failTask) {
        this(patterns, null, null, failTask);
    }

    public DarkIsGroundItem(Pattern[] patterns, Coordinate coordinate, TreeTask failTask) {
        this(patterns, null, coordinate, failTask);
    }

    public DarkIsGroundItem(Pattern pattern, TreeTask failTask) {
        this(toArray(pattern), null, null, failTask);
    }

    public DarkIsGroundItem(Pattern pattern, Coordinate coordinate, TreeTask failTask) {
        this(toArray(pattern), null, coordinate, failTask);
    }

    public DarkIsGroundItem(String name, TreeTask failTask) {
        this(toArray(toPattern(name)), null, null, failTask);
    }

    public DarkIsGroundItem(String name, Coordinate coordinate, TreeTask failTask) {
        this(toArray(toPattern(name)), null, coordinate, failTask);
    }

    public DarkIsGroundItem(String[] names, TreeTask failTask) {
        this(toPattern(names), null, null, failTask);
    }

    public DarkIsGroundItem(String[] names, Coordinate coordinate, TreeTask failTask) {
        this(toPattern(names), null, coordinate, failTask);
    }

    public DarkIsGroundItem(Pattern[] patterns, int[] ids, Coordinate coordinate, TreeTask failTask) {
        this.patterns = patterns;
        this.ids = ids;
        this.coordinate = coordinate;
        this.failTask = failTask;
    }

    private static int[] toArray(int id) {
        return new int[] { id };
    }

    private static Pattern[] toArray(Pattern pattern) {
        return new Pattern[] { pattern };
    }

    private static Pattern toPattern(String text) {
        return Regex.getPatternForExactString(text);
    }

    public static Pattern[] toPattern(String[] strings){
        return Arrays.stream(strings).map(Regex::getPatternForExactString).toArray(Pattern[]::new);
    }

    @Override
    public boolean validate() {
        GroundItemQueryBuilder builder = GroundItems.newQuery();
        if (patterns != null)
            builder = builder.names(patterns);
        if (ids != null)
            builder = builder.ids(ids);
        if (coordinate != null)
            builder = builder.on(coordinate);
        return builder.results().size() > 0;
    }

    @Override
    public TreeTask successTask() {
        if (coordinate != null) {
            if (ids != null)
                return new DarkTakeGroundItem(ids, coordinate);
            if (patterns != null)
                return new DarkTakeGroundItem(patterns, coordinate);
            return new DarkTakeGroundItem(coordinate);
        }
        if (ids != null)
            return new DarkTakeGroundItem(ids);
        return new DarkTakeGroundItem(patterns);
    }

    @Override
    public TreeTask failureTask() {
        return failTask;
    }
}
