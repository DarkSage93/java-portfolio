package com.DarkAPI.framework.treeTasks.checks;

import com.DarkAPI.framework.treeTasks.executable.DarkInventoryItemAction;
import com.DarkAPI.framework.treeTasks.executable.DarkItemSelect;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

import java.util.regex.Pattern;

public class DarkIsItemSelected extends BranchTask {

    private final TreeTask treeTask;
    private final Pattern item;
    private final int id;

    public DarkIsItemSelected(int id, TreeTask treeTask) {
        this(null, id, treeTask);
    }

    public DarkIsItemSelected(String item, TreeTask treeTask) {
        this(toPattern(item), -1, treeTask);
    }

    public DarkIsItemSelected(Pattern item, TreeTask treeTask) {
        this(item, -1, treeTask);
    }

    public DarkIsItemSelected(Pattern item, int id, TreeTask treeTask) {
        this.treeTask = treeTask;
        this.item = item;
        this.id = id;
    }

    private static Pattern toPattern(String text) {
        if (text != null)
            return Regex.getPatternForExactString(text);
        return null;
    }

    @Override
    public boolean validate() {
        SpriteItem holder = Inventory.getSelectedItem();
        ItemDefinition itemDef;
        if (holder != null) {
            if ((itemDef = holder.getDefinition()) != null) {
                if (id != -1)
                    return itemDef.getId() == id;
                if (item != null)
                    return item.matcher(itemDef.getName()).find();
            }
        }
        return false;
    }

    @Override
    public TreeTask successTask() {
        return treeTask;
    }

    @Override
    public TreeTask failureTask() {
        if (Inventory.getSelectedItem() != null) {
            if (item != null)
                return new DarkInventoryItemAction(item);
            return new DarkInventoryItemAction(id);
        }
        if (item != null)
            return new DarkItemSelect(item);
        return new DarkItemSelect(id);
    }
}
