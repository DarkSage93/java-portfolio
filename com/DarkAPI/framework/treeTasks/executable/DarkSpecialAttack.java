package com.DarkAPI.framework.treeTasks.executable;

import com.regal.utility.SpecialAttack;
import com.runemate.game.api.script.framework.tree.LeafTask;

public class DarkSpecialAttack extends LeafTask {

    private final boolean isActivate;

    public DarkSpecialAttack(boolean isActivate) {
        this.isActivate = isActivate;
    }

    @Override
    public void execute() {
        String log = isActivate ? "Activating Special Attack" : "Disabling Special Attack";
        handle(log);
    }

    private boolean handle(String log) {
        if (SpecialAttack.isToggleable()) {
            if (SpecialAttack.isActivated() != isActivate) {
                getLogger().info(log);
                return isActivate ? SpecialAttack.activate() : SpecialAttack.deactivate();
            }
        } else {
            getLogger().debug("Current weapon lacks a Special attack");
        }
        return false;
    }
}
