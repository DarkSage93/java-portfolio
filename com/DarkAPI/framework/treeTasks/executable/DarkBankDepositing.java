package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.HashMap;
import java.util.regex.Pattern;

public class DarkBankDepositing extends LeafTask {

    private final HashMap<Pattern, Integer> items;
    private final boolean depositGear;
    private final boolean depositInv;

    public DarkBankDepositing(HashMap<Pattern, Integer> items, boolean depositGear, boolean depositInv) {
        this.items = items;
        this.depositGear = depositGear;
        this.depositInv = depositInv;
    }

    @Override
    public void execute() {
        if (depositGear)
            depositGear();
        if (depositInv)
            depositInv();
        if (items != null && items.size() > 0) {
            items.forEach(this::depositItem);
        } else {
            getLogger().debug("Deposit items map is empty or null");
        }
    }

    private void depositItem(Pattern pattern, int qty) {
        if (Inventory.contains(pattern)) {
            getLogger().info("Depositing " + pattern);
            if (Bank.deposit(pattern, qty)) {
                try {
                    Delay.bankDeposit(pattern).call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void depositGear() {
        if (!Equipment.isEmpty()) {
            getLogger().info("Depositing all worn equipment");
            if (Bank.depositEquipment()) {
                try {
                    Delay.ticks1().call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void depositInv() {
        if (!Inventory.isEmpty()) {
            getLogger().info("depositing Inventory");
            if (Bank.depositInventory()) {
                try {
                    Delay.ticks1().call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
