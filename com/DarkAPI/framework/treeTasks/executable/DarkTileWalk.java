package com.DarkAPI.framework.treeTasks.executable;

import com.runemate.game.api.hybrid.input.Keyboard;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.concurrent.Callable;

public class DarkTileWalk extends LeafTask {

    private final Coordinate coordinate;
    private final boolean isCtrlPressed;
    private final Callable<Boolean> delay;

    public DarkTileWalk(Coordinate coordinate, Callable<Boolean> delay) {
        this(coordinate, false, delay);
    }

    public DarkTileWalk(Coordinate coordinate, boolean isCtrlPressed, Callable<Boolean> delay) {
        this.coordinate = coordinate;
        this.isCtrlPressed = isCtrlPressed;
        this.delay = delay;
    }

    @Override
    public void execute() {
        String log = "Clicking \"Walk-here\" on tile : " + coordinate;
        if (isCtrlPressed) {
            if (pressCtrl()) getLogger().info(log + " Pressed CTRL to run");
        } else {
            getLogger().info(log);
        }
        if (coordinate.interact("Walk-here")) {
            try {
                delay.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (isCtrlPressed) {
            if (releaseCtrl()) getLogger().info("Released CTRL key");
        }
    }

    public static boolean isCtrlPressed() {
        return Keyboard.isPressed(18);
    }

    public static boolean pressCtrl() {
        if (!isCtrlPressed())
            return Keyboard.pressKey(18);
        return false;
    }

    public static boolean releaseCtrl() {
        if (isCtrlPressed())
            return Keyboard.releaseKey(18);
        return false;
    }
}
