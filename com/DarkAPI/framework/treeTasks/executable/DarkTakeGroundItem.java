package com.DarkAPI.framework.treeTasks.executable;

import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.GroundItemQueryBuilder;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DarkTakeGroundItem extends LeafTask {

    private final Pattern[] names;
    private final int[] ids;
    private final Coordinate coordinate;

    public DarkTakeGroundItem(Pattern[] names) {
        this(names, null, null);
    }

    public DarkTakeGroundItem(Pattern[] names, Coordinate coordinate) {
        this(names, null, coordinate);
    }

    public DarkTakeGroundItem(Pattern pattern) {
        this(toArray(pattern), null, null);
    }

    public DarkTakeGroundItem(Pattern pattern, Coordinate coordinate) {
        this(toArray(pattern), null, coordinate);
    }

    public DarkTakeGroundItem(String name) {
        this(toArray(name), null, null);
    }

    public DarkTakeGroundItem(String name, Coordinate coordinate) {
        this(toArray(name), null, coordinate);
    }

    public DarkTakeGroundItem(String[] names) {
        this(toPattern(names), null, null);
    }

    public DarkTakeGroundItem(String[] names, Coordinate coordinate) {
        this(toPattern(names), null, coordinate);
    }

    public DarkTakeGroundItem(int[] ids) {
        this(null, ids, null);
    }

    public DarkTakeGroundItem(int[] ids, Coordinate coordinate) {
        this(null, ids, coordinate);
    }

    public DarkTakeGroundItem(int id) {
        this(null, toArray(id), null);
    }

    public DarkTakeGroundItem(int id, Coordinate coordinate) {
        this(null, toArray(id), coordinate);
    }

    public DarkTakeGroundItem(Coordinate coordinate) {
        this(null, null, coordinate);
    }

    public DarkTakeGroundItem(Pattern[] names, int[] ids, Coordinate coordinate) {
        this.names = names;
        this.ids = ids;
        this.coordinate = coordinate;
    }

    private static Pattern toPattern(String text) {
        return Regex.getPatternForExactString(text);
    }

    private static Pattern[] toArray(Pattern pattern) {
        return new Pattern[] { pattern };
    }

    private static Pattern[] toArray(String text) {
        return new Pattern[] { toPattern(text) };
    }

    private static int[] toArray(int id) {
        return new int[] { id };
    }

    public static Pattern[] toPattern(String[] strings){
        return Arrays.stream(strings).map(Regex::getPatternForExactString).toArray(Pattern[]::new);
    }

    @Override
    public void execute() {
        GroundItemQueryBuilder builder = GroundItems.newQuery();
        String loggerDef = "";
        if (names != null) {
            builder = builder.names(names);
            loggerDef = "names = " + toString(names) + " ";
        }
        if (ids != null) {
            builder = builder.ids(ids);
            loggerDef = loggerDef + "ids = " + toString(ids) + " ";
        }
        if (coordinate != null) {
            builder = builder.on(coordinate);
            loggerDef = loggerDef + "Coordinate = " + coordinate + " ";
        }
        GroundItem item = builder.results().first();
        Player local = Players.getLocal();
        if (item != null) {
            if (local != null) {
                if (item.isVisible()) {
                    getLogger().info("Taking " + item);
                    if (item.interact("Take"))
                        Execution.delayUntil(()-> Inventory.contains(item.getId()), local::isMoving, 1800, 2400);
                } else {
                    getLogger().debug("Camera :: " + item);
                    Camera.turnTo(item);
                }
            } else {
                getLogger().debug("Local player is null");
            }
        } else {
            getLogger().debug("GroundItem is null = " + loggerDef);
        }
    }

    private String toString(Pattern[] patterns) {
        return Arrays.stream(patterns).map(Pattern::toString).collect(Collectors.joining(", "));
    }

    private String toString(int[] ints) {
        return Arrays.stream(ints).mapToObj(String::valueOf).reduce((a, b) -> a.concat(",").concat(b)).get();
    }
}
