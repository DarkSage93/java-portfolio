package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.data.general.DarkPlayerSense;
import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.queries.SpriteItemQueryBuilder;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.regex.Pattern;

public class DarkItemSelect extends LeafTask {

    private final Pattern pattern;
    private final int id;

    public DarkItemSelect(String name) {
        this(toPattern(name), -1);
    }

    public DarkItemSelect(Pattern pattern) {
        this(pattern, -1);
    }

    public DarkItemSelect(int id) {
        this(null, id);
    }

    public DarkItemSelect(Pattern pattern, int id) {
        this.pattern = pattern;
        this.id = id;
    }

    private static Pattern toPattern(String name) {
        return Regex.getPatternForExactString(name);
    }

    @Override
    public void execute() {
        SpriteItem selected = Inventory.getSelectedItem();
        SpriteItem item;
        if (selected != null) {
            getLogger().debug(selected + " is selected");
        } else {
            SpriteItemQueryBuilder builder = Inventory.newQuery();
            if (pattern != null)
                builder = builder.names(pattern);
            if (id != -1)
                builder = builder.ids(id);
            if (DarkPlayerSense.Key.SELECT_ORDER.getAsBoolean()) {
                item = builder.results().sortByIndex().first();
            } else {
                item = builder.results().sortByIndex().last();
            }
            if (item != null) {
                getLogger().info("Selecting " + item);
                if (item.interact("Use")) {
                    try {
                        Delay.reaction().call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                getLogger().debug("Item to be selected was null");
            }
        }
    }
}
