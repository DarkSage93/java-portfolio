package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.local.Spell;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceWindows;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.concurrent.Callable;

public class DarkCastSpell extends LeafTask {

    private final Spell spell;
    private final Callable<Boolean> delay;

    public DarkCastSpell(Spell spell, Callable<Boolean> delay) {
        this.spell = spell;
        this.delay = delay;
    }

    @Override
    public void execute() {
        if (!InterfaceWindows.getMagic().isOpen()) {
            getLogger().info("Opening Magic Tab");
            if (InterfaceWindows.getMagic().open()) {
                try {
                    Delay.reaction().call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            getLogger().info("Casting " + spell);
            if (spell.activate()) {
                try {
                    delay.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
