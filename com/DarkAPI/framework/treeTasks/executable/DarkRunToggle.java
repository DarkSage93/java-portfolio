package com.DarkAPI.framework.treeTasks.executable;

import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;

public class DarkRunToggle extends LeafTask {

    private final boolean isRun;

    public DarkRunToggle(boolean isRun) {
        this.isRun = isRun;
    }

    @Override
    public void execute() {
        String log = isRun ? "Enable run" : "Disable run";
        if (handle(log))
            Execution.delay(100, 200);
    }

    private boolean handle(String log) {
        if (toggleNeeded())
            return toggle(log);
        return false;
    }

    private boolean toggleNeeded() {
        return Traversal.isRunEnabled() != isRun;
    }

    private boolean toggle(String log) {
        getLogger().info(log);
        return Traversal.toggleRun();
    }

}
