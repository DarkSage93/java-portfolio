package com.DarkAPI.framework.treeTasks.executable;

import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.Arrays;
import java.util.regex.Pattern;

public class DarkEquipItems extends LeafTask {

    private final Pattern[] patterns;

    public DarkEquipItems(String... items){
        this(stringToPattern(items));
    }

    public DarkEquipItems(Pattern... patterns) {
        this.patterns = patterns;
    }

    public static Pattern[] stringToPattern(String[] strings){
        return Arrays.stream(strings).map(Regex::getPatternForExactString).toArray(Pattern[]::new);
    }

    @Override
    public void execute() {
        if (patterns != null && patterns.length > 0) {
            for (Pattern value : patterns) {
                if (!Equipment.contains(value) && Inventory.contains(value)) {
                    SpriteItem item = Inventory.newQuery().names(value).results().first();
                    if (item != null) {
                        getLogger().info("Equipping " + value);
                        if (Inventory.equip(item))
                            Execution.delayUntil(() -> !Inventory.contains(value), 1200, 1800);
                    } else {
                        getLogger().debug("SpriteItem " + value + " was null");
                    }
                }
            }
        }
    }
}
