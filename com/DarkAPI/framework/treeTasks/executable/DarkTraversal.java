package com.DarkAPI.framework.treeTasks.executable;

import com.regal.regal_bot.RegalBot;
import com.regal.utility.awesome_navigation.Pathing;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.tree.LeafTask;

public class DarkTraversal extends LeafTask {

    private final Area area;
    private final String areaName;
    private final Coordinate coordinate;
    private final boolean isWebUsed;
    private final RegalBot bot;

    public DarkTraversal(Coordinate coordinate, String areaName, RegalBot bot) {
        this(null, coordinate, areaName, true, bot);
    }

    public DarkTraversal(Coordinate coordinate, String areaName, boolean isWebUsed, RegalBot bot) {
        this(null, coordinate, areaName, isWebUsed, bot);
    }

    public DarkTraversal(Area area, String areaName, RegalBot bot) {
        this(area, null, areaName, true, bot);
    }

    public DarkTraversal(Area area, String areaName, boolean isWebUsed, RegalBot bot) {
        this(area, null, areaName, isWebUsed, bot);
    }

    public DarkTraversal(Area area, Coordinate coordinate, String areaName, boolean isWebUsed, RegalBot bot) {
        this.area = area;
        this.coordinate = coordinate;
        this.areaName = areaName;
        this.isWebUsed = isWebUsed;
        this.bot = bot;
    }

    @Override
    public void execute() {
        getLogger().info("Walking to "+areaName);
        getLogger().debug("web = "+isWebUsed);
        if (isWebUsed) {
            if (coordinate != null)
                bot.getPathing().webPathTo(coordinate);
            if (area != null)
                bot.getPathing().webPathTo(area);
        } else {
            if (coordinate != null)
                Pathing.pathTo(coordinate);
            if (area != null)
                Pathing.pathTo(area);
        }
    }
}
