package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.script.framework.tree.LeafTask;

public class DarkChatDialog extends LeafTask {

    private final String[] options;

    public DarkChatDialog() {
        this((String) null);
    }

    public DarkChatDialog(String... options) {
        this.options = options;
    }

    @Override
    public void execute() {
        ChatDialog.Continue cont = ChatDialog.getContinue();
        if (ChatDialog.isOpen() || cont != null || !ChatDialog.getOptions().isEmpty()) {
            if (cont != null){
                getLogger().info("Selecting " + cont);
                if (cont.select()) {
                    try {
                        Delay.reaction().call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (options != null) {
                    for (String s : options) {
                        ChatDialog.Option option = ChatDialog.getOption(s);
                        if (option != null) {
                            getLogger().info("Selecting " + option);
                            if (option.select()) {
                                try {
                                    Delay.reaction().call();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            getLogger().debug("ChatDialog is not open");
        }
    }
}
