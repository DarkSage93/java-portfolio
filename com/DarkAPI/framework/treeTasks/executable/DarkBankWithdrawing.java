package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.regal.regal_bot.RegalBot;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.HashMap;
import java.util.regex.Pattern;

public class DarkBankWithdrawing extends LeafTask {

    private final HashMap<Pattern, Integer> items;
    private final HashMap<Pattern, Integer> goals;
    private final boolean isClose;
    private final boolean isStop;
    private final boolean isNoted;
    private final Bank.DefaultQuantity bankQuantity;
    private final RegalBot bot;

    public DarkBankWithdrawing(HashMap<Pattern, Integer> items, HashMap<Pattern, Integer> goals, boolean isClose, boolean isNoted, boolean isStop, Bank.DefaultQuantity bankQuantity, RegalBot bot) {
        this.items = items;
        this.goals = goals;
        this.isClose = isClose;
        this.isNoted = isNoted;
        this.isStop = isStop;
        this.bankQuantity = bankQuantity;
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (isClose)
            closeBank();
        if (bankQuantity != null && Bank.getDefaultQuantity() != bankQuantity)
            setBankQuantity(bankQuantity);
        Bank.WithdrawMode mode = isNoted ? Bank.WithdrawMode.NOTE : Bank.WithdrawMode.ITEM;
        setWithdrawMode(mode);
        if (items != null && items.size() > 0) {
            if (goals != null && goals.size() >0) {
                items.forEach(this::goalWithdraw);
            } else {
                items.forEach(this::withdrawItem);
            }
        } else {
            getLogger().debug("Items map is empty or null");
        }
    }

    private void goalWithdraw(Pattern key, Integer qty) {
        Integer goal = goals.get(key);
        withdrawItem(key, qty, goal);
    }

    private void withdrawItem(Pattern pattern, int quantity) {
        if (Bank.contains(pattern)) {
            getLogger().info("Withdrawing " + quantity + " of " + pattern);
            if (Bank.withdraw(pattern, quantity)) {
                try {
                    Delay.bankWithdraw(pattern, Inventory.getEmptySlots()).call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (!Inventory.contains(pattern))
                if (isStop) {
                    getLogger().info("We are out of " + pattern);
                    bot.getUI().setStopBot(true, "we are out of " + pattern);
                }
        }
    }

    private void withdrawItem(Pattern pattern, int quantity, int goal) {
        if (Bank.getQuantity(pattern) >= goal) {
            getLogger().info("Withdrawing " + quantity + " of " + pattern);
            int x = goal - Inventory.getQuantity(pattern);
            if (x != 0)
                if (Bank.withdraw(pattern, x)) {
                    try {
                        Delay.bankWithdraw(pattern, goal).call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        } else {
            if (!Inventory.contains(pattern))
                if (isStop) {
                    getLogger().info("We are out of " + pattern);
                    bot.getUI().setStopBot(true, "We are out of " + pattern);
                }
        }
    }

    private void setWithdrawMode(Bank.WithdrawMode mode) {
        if (Bank.getWithdrawMode() != mode) {
            getLogger().info("Setting Withdraw mode to : " + mode);
            if (Bank.setWithdrawMode(mode))
                try {
                    Delay.reaction().call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    private void closeBank() {
        getLogger().info("Closing Bank");
        if (Bank.close())
            try {
                Delay.ticks1().call();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    private void setBankQuantity(Bank.DefaultQuantity quantity) {
        if (Bank.getDefaultQuantity() != quantity) {
            getLogger().info("setting Default withdraw quantity : " + quantity);
            if (Bank.setDefaultQuantity(quantity))
                try {
                    Delay.reaction().call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}
