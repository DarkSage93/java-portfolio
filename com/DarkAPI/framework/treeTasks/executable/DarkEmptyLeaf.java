package com.DarkAPI.framework.treeTasks.executable;

import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.concurrent.Callable;

public class DarkEmptyLeaf extends LeafTask {

    private final String reason;
    private final Callable<Boolean> delay;

    public DarkEmptyLeaf() {
        this(null, null);
    }

    public DarkEmptyLeaf(Callable<Boolean> delay) {
        this(null, delay);
    }

    public DarkEmptyLeaf(String reason) {
        this(reason, null);
    }

    public DarkEmptyLeaf(String reason, Callable<Boolean> delay) {
        this.reason = reason;
        this.delay = delay;
    }

    StringBuilder logger = new StringBuilder();
    @Override
    public void execute() {
        logger.append("We have reached ").append(this.getClass().getName());
        if (reason != null)
            logger.append(". Reason supplied : ").append(reason);
        if (delay != null)
            logger.append(". Delay supplied :").append(delay);
        getLogger().info(logger.toString());
        if (delay != null)
            try {
                delay.call();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
