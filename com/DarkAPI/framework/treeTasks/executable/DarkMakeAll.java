package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.osrs.local.hud.interfaces.MakeAllInterface;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class DarkMakeAll extends LeafTask {

    private final int quantity;
    private final Pattern pattern;
    private final Callable<Boolean> delay;

    public DarkMakeAll(int quantity, Pattern pattern, Callable<Boolean> delay) {
        this.quantity = quantity;
        this.pattern = pattern;
        this.delay = delay;
    }

    @Override
    public void execute() {
        if (MakeAllInterface.getSelectedQuantity() != quantity) {
            getLogger().info("Setting Make All interface quantity to " + quantity);
            if (MakeAllInterface.setSelectedQuantity(quantity)) {
                try {
                    Delay.reaction().call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (pattern != null) {
            getLogger().info("Making " + pattern);
            if (MakeAllInterface.selectItem(pattern)) {
                try {
                    delay.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
