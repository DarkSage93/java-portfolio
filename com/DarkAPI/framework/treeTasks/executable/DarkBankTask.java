package com.DarkAPI.framework.treeTasks.executable;

import com.regal.regal_bot.RegalBot;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

import java.util.HashMap;
import java.util.regex.Pattern;

public class DarkBankTask extends BranchTask {

    private final HashMap<Pattern, Integer> withdraw;
    private final HashMap<Pattern, Integer> deposit;
    private final HashMap<Pattern, Integer> goals;
    private final boolean depositInv;
    private final boolean depositGear;
    private final boolean isNoted;
    private final boolean isClose;
    private final boolean isStop;
    private final Bank.DefaultQuantity bankQty;
    private final RegalBot bot;
    private final boolean depositOnly;

    public DarkBankTask(boolean depositOnly, boolean isClose) {
        this(null, null, depositOnly, null, false, false, false, isClose, false, null, null);
    }

    public DarkBankTask(boolean depositOnly, boolean depositInv, boolean depositGear) {
        this(null, null, depositOnly, null, depositInv, depositGear, false, false, false, null, null);
    }

    public DarkBankTask(boolean depositOnly, HashMap<Pattern, Integer> deposit) {
        this(null, null, depositOnly, deposit, false, false, false, false, false, null, null);
    }

    public DarkBankTask(HashMap<Pattern, Integer> withdraw, Bank.DefaultQuantity bankQty, RegalBot bot) {
        this(withdraw, null, false, null, false, false, false, false, true, bankQty, bot);
    }

    public DarkBankTask(HashMap<Pattern, Integer> withdraw, boolean isNoted, boolean isStop, Bank.DefaultQuantity bankQty, RegalBot bot) {
        this(withdraw, null, false, null, false, false, isNoted, false, isNoted, bankQty, bot);
    }

    public DarkBankTask(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> deposit, Bank.DefaultQuantity bankQty, RegalBot bot) {
        this(withdraw, null, false, deposit, false, false, false, false, true, bankQty, bot);
    }

    public DarkBankTask(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> deposit, boolean isStop, Bank.DefaultQuantity bankQty, RegalBot bot) {
        this(withdraw, null, false, deposit, false, false, false, false, isStop, bankQty, bot);
    }

    public DarkBankTask(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> deposit, boolean isNoted, boolean isStop, Bank.DefaultQuantity bankQty, RegalBot bot) {
        this(withdraw, null, false, deposit, false, false, isNoted, false, isStop, bankQty, bot);
    }

    public DarkBankTask(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> goals, HashMap<Pattern, Integer> deposit, boolean isNoted, boolean isStop, Bank.DefaultQuantity bankQty, RegalBot bot) {
        this(withdraw, goals, false, deposit, false, false, isNoted, false, isStop, bankQty, bot);
    }

    //Following shouldn't need to be used but here as redundancy
    public DarkBankTask(Pattern[] patterns, Integer[] quantities, Integer[] goals, boolean depositOnly, Pattern[] dPatterns, Integer[] dQuantities, boolean depositInv, boolean depositGear, boolean isNoted, boolean isClose, boolean isStop, Bank.DefaultQuantity bankQty, RegalBot bot) {
        this(toMap(patterns, quantities), toMap(patterns, goals), depositOnly, toMap(dPatterns, dQuantities), depositInv, depositGear, isNoted, isClose, isStop, bankQty, bot);
    }

    //Primary Constructor
    public DarkBankTask(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> goals, boolean depositOnly, HashMap<Pattern, Integer> deposit, boolean depositInv, boolean depositGear, boolean isNoted, boolean isClose, boolean isStop, Bank.DefaultQuantity bankQty, RegalBot bot) {
        this.withdraw = withdraw;
        this.goals = goals;
        this.depositOnly = depositOnly;
        this.deposit = deposit;
        this.depositInv = depositInv;
        this.depositGear = depositGear;
        this.isNoted = isNoted;
        this.isClose = isClose;
        this.isStop = isStop;
        this.bankQty = bankQty;
        this.bot = bot;
    }

    private static HashMap<Pattern, Integer> toMap(Pattern[] keys, Integer[] values) {
        HashMap<Pattern, Integer> map = new HashMap<>(0);
        for (int i = 0; i < keys.length; i++) {
            map.putIfAbsent(keys[i], values[i]);
        }
        return map;
    }

    private static HashMap<Pattern, Integer> toMap(String[] keys, Integer[] values) {
        HashMap<Pattern, Integer> map = new HashMap<>(0);
        for (int i = 0; i < keys.length; i++) {
            map.putIfAbsent(toPattern(keys[i]), values[i]);
        }
        return map;
    }

    private static HashMap<Pattern, Integer> toMap(String text, Integer value) {
        HashMap<Pattern, Integer> map = new HashMap<>(0);
        map.putIfAbsent(toPattern(text), value);
        return map;
    }

    private static HashMap<Pattern, Integer> toMap(Pattern key, Integer value) {
        HashMap<Pattern, Integer> map = new HashMap<>(0);
        map.putIfAbsent(key, value);
        return map;
    }

    private static Pattern toPattern(String text) {
        return Regex.getPatternForExactString(text);
    }

    @Override
    public boolean validate() {
        return !depositOnly && (deposit == null || !Inventory.containsAnyOf(toArray())) && (!depositGear || Equipment.isEmpty()) && (!depositInv || Inventory.isEmpty());
    }

    @Override
    public TreeTask successTask() {
        return new DarkBankWithdrawing(withdraw, goals, isClose, isNoted, isStop, bankQty, bot);
    }

    @Override
    public TreeTask failureTask() {
        return new DarkBankDepositing(deposit, depositGear, depositInv);
    }

    private Pattern[] toArray() {
        if (deposit != null) {
            Pattern[] patterns = new Pattern[deposit.size()];
            int i = 0;
            for (Pattern pattern : deposit.keySet()) {
                patterns[i] = pattern;
                i++;
            }
            return patterns;
        }
        return null;
    }
}
