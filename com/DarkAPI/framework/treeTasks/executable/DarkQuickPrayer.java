package com.DarkAPI.framework.treeTasks.executable;

import com.runemate.game.api.osrs.local.hud.interfaces.Prayer;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.Arrays;
import java.util.List;

public class DarkQuickPrayer extends LeafTask {

    private final boolean isEnable;
    private final Prayer[] prayers;

    public DarkQuickPrayer(Prayer... prayers) {
        this(false, prayers);
    }

    public DarkQuickPrayer(boolean isEnable) {
        this(isEnable, new Prayer[]{});
    }

    public DarkQuickPrayer(boolean isEnable, Prayer... prayers) {
        this.isEnable = isEnable;
        this.prayers = prayers;
    }

    @Override
    public void execute() {
        if (prayers != null && prayers.length > 0) {
            setQuickPrayers();
        } else {
            String log = isEnable ? "Enabling Quick Prayers" : "Disabling Quick prayers";
            if (handleQuickPray(log)) Execution.delay(100, 200);
        }
    }

    private void setQuickPrayers() {
       if (!compareQuickPrayers()) {
           getLogger().info("Setting up Quick Prayers");
           if (Prayer.setQuickPrayers(prayers))
               Execution.delay(400, 800);
       }
    }

    private boolean compareQuickPrayers() {
        List<Prayer> quickPrayers = Prayer.getSelectedQuickPrayers();
        return quickPrayers.size() == prayers.length && quickPrayers.containsAll(Arrays.asList(prayers));
    }

    private boolean handleQuickPray(String log) {
        if (isToggleNeeded()) return toggle(log);
        return false;
    }

    private boolean isToggleNeeded() {
        return Prayer.isQuickPraying() != isEnable;
    }

    private boolean toggle(String log) {
        getLogger().info(log);
        return Prayer.toggleQuickPrayers();
    }
}
