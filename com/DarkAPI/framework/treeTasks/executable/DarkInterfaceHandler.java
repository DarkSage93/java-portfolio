package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.queries.InterfaceComponentQueryBuilder;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.concurrent.Callable;

public class DarkInterfaceHandler extends LeafTask {

    private final int container;
    private final int index;
    private final int subIndex;
    private final int id;
    private final String action;
    private final String text;
    private final String name;
    private final boolean isQuery;
    private final boolean isExtraDelay;
    private final Callable<Boolean> delay;

    public DarkInterfaceHandler(String text, String action) {
        this(true, -1, -1, action, text, null, null);
    }

    public DarkInterfaceHandler(String text, String action, Callable<Boolean> delay) {
        this(true, -1, -1, action, text, null, delay);
    }

    public DarkInterfaceHandler(int container, String text) {
        this(true, container, -1, null, text, null, null);
    }

    public DarkInterfaceHandler(int container, String text, Callable<Boolean> delay) {
        this(true, container, -1, null, text, null, delay);
    }

    public DarkInterfaceHandler(int container, String action, String text) {
        this(true, container, -1, action, text, null, null);
    }

    public DarkInterfaceHandler(int container, String action, String text, Callable<Boolean> delay) {
        this(true, container, -1, action, text, null, delay);
    }

    public DarkInterfaceHandler(int container, String action, String text, boolean isExtraDelay, Callable<Boolean> delay) {
        this(true, container, -1, -1, -1, action, text, null, isExtraDelay, delay);
    }

    public DarkInterfaceHandler(int container, String action, String text, String name) {
        this(true, container, -1, action, text, name, null);
    }

    public DarkInterfaceHandler(int container, String action, String text, String name, Callable<Boolean> delay) {
        this(true, container, -1, action, text, name, delay);
    }

    public DarkInterfaceHandler(int container, int id, String text, String name) {
        this(true, container, id, null, text, name, null);
    }

    public DarkInterfaceHandler(int container, int id, String text, String name, Callable<Boolean> delay) {
        this(true, container, id, null, text, name, delay);
    }

    public DarkInterfaceHandler(int container, int id, String action, String text, String name) {
        this(true, container, id, action, text, name, null);
    }

    public DarkInterfaceHandler(int container, int id, String action, String text, String name, Callable<Boolean> delay) {
        this(true, container, id, action, text, name, delay);
    }

    public DarkInterfaceHandler(boolean isQuery, int container, int id, String action, String text, String name, Callable<Boolean> delay) {
        this(isQuery, container, -1, -1, id, action, text, name, false, delay);
    }

    public DarkInterfaceHandler(boolean isQuery, int container, int index) {
        this(isQuery, container, index, -1, null);
    }

    public DarkInterfaceHandler(boolean isQuery, int container, int index, Callable<Boolean> delay) {
        this(isQuery, container, index, -1, delay);
    }

    public DarkInterfaceHandler(boolean isQuery, int container, int index, int subIndex) {
        this(isQuery, container, index, subIndex, null);
    }

    public DarkInterfaceHandler(boolean isQuery, int container, int index, int subIndex, Callable<Boolean> delay) {
        this(isQuery, container, index, subIndex, -1, null, null, null, false, delay);
    }

    public DarkInterfaceHandler(boolean isQuery, int container, int index, int subIndex, int id, String action, String text, String name, boolean isExtraDelay, Callable<Boolean> delay) {
        this.isQuery = isQuery;
        this.container = container;
        this.index = index;
        this.subIndex = subIndex;
        this.id = id;
        this.action = action;
        this.text = text;
        this.name = name;
        this.isExtraDelay = isExtraDelay;
        this.delay = delay;
    }

    StringBuilder logDef = new StringBuilder(0);

    @Override
    public void execute() {
        InterfaceComponent component;
        if (isQuery) component = buildQuery(); else  component = get();
        if (component != null) {
            selectComponent(component);
        }else {
            getLogger().debug("Component : " + logDef.toString() + " was null");
        }
        if (isExtraDelay)
            try {
                Delay.ticks1().call();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    private void selectComponent(InterfaceComponent component) {
        boolean success = false;
        if (action == null) {
            getLogger().info("Clicking " + component);
            if (component.click()) success = true;
        } else {
            getLogger().info("Using " + action + " on " + component);
            if (component.interact(action)) success = true;
        }
        if (success) {
            try {
                if (delay != null) {
                    delay.call();
                } else {
                    Delay.ticks1().call();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        getLogger().debug("Component interaction failed");
    }

    private InterfaceComponent buildQuery() {
        InterfaceComponentQueryBuilder builder = Interfaces.newQuery();
        if (container != -1) {
            builder = builder.containers(container);
            logDef.append("container = ").append(container).append(", ");
        }
        if (id != -1) {
            builder = builder.ids(id);
            logDef.append("id = ").append(id).append(", ");
        }
        if (text != null) {
            builder = builder.textContains(text);
            logDef.append("text = ").append(text).append(", ");
        }
        if (name != null) {
            builder = builder.names(name);
            logDef.append("name = ").append(name).append(", ");
        }
        if (action != null) {
            builder = builder.actions(action);
            logDef.append("action = ").append(action).append(", ");
        }
        return builder.results().first();
    }

    private InterfaceComponent get() {
        logDef.append("Container = ").append(container).append(" : index = ").append(index);
        if (subIndex != -1) {
            logDef.append(" : subIndex = ").append(subIndex);
            return Interfaces.getAt(container, index, subIndex);
        }
        return Interfaces.getAt(container, index);
    }
}
