package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceWindows;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.queries.SpriteItemQueryBuilder;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class DarkInventoryItemAction extends LeafTask {

    private final Pattern item;
    private final int id;
    private final String action;
    private final Callable<Boolean> delay;

    public DarkInventoryItemAction(Pattern item) {
        this(item, -1, null, null);
    }

    public DarkInventoryItemAction(Pattern item, String action) {
        this(item, -1, action, null);
    }

    public DarkInventoryItemAction(Pattern item, String action, Callable<Boolean> delay) {
        this(item, -1, action, delay);
    }

    public DarkInventoryItemAction(String item) {
        this(toPattern(item), -1, null, null);
    }

    public DarkInventoryItemAction(String item, String action) {
        this(toPattern(item), -1, action, null);
    }

    public DarkInventoryItemAction(String item, String action, Callable<Boolean> delay) {
        this(toPattern(item), -1, action, delay);
    }

    public DarkInventoryItemAction(int id) {
        this(null, id, null, null);
    }

    public DarkInventoryItemAction(int id, String action) {
        this(null, id, action, null);
    }

    public DarkInventoryItemAction(int id, String action, Callable<Boolean> delay) {
        this(null, id, action, delay);
    }

    public DarkInventoryItemAction(Pattern item, int id, String action, Callable<Boolean> delay) {
        this.item = item;
        this.id = id;
        this.action = action;
        this.delay = delay;
    }

    private static Pattern toPattern(String text) {
        return Regex.getPatternForExactString(text);
    }

    @Override
    public void execute() {
        SpriteItemQueryBuilder builder = Inventory.newQuery();
        String loggerDef = "";
        if (item != null) {
            builder = builder.names(item);
            loggerDef = "Regex = " + item + " ";
        }
        if (id != -1) {
            builder = builder.ids(id);
            loggerDef = "ID = " + id + " ";
        }
        if (action != null) {
            builder = builder.actions(action);
            loggerDef = loggerDef + "action(s) = " + action + " ";
        }
        if (!InterfaceWindows.getInventory().isOpen()) {
            if (InterfaceWindows.getInventory().open())
                try {
                    Delay.reaction().call();
                } catch (Exception e) {
                    getLogger().info("Delay failed = " + e);
                }
        }
        SpriteItem sprite = builder.results().first();
        if (sprite != null) {
            if (action != null) {
                getLogger().info("Using " + action + "on " + item);
                if (sprite.interact(action)) {
                    try {
                        if (delay != null) {
                            delay.call();
                        } else {
                            Delay.ticks1().call();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    getLogger().warn("Interaction on Sprite Item Failed = " + action + " = action : " + item);
                }
            } else {
                getLogger().info("Clicking on " + item);
                if (sprite.click()) {
                    try {
                        if (delay != null) {
                            delay.call();
                        } else {
                            Delay.ticks1().call();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    getLogger().warn("\".click()\" on SpriteItem Failed = " + item);
                }
            }
        } else {
            getLogger().debug("sprite = null == " + loggerDef);
        }
    }
}
