package com.DarkAPI.framework.treeTasks.executable;

import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.GroundItemQueryBuilder;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.Arrays;
import java.util.regex.Pattern;

public class DarkGroundItemTake extends LeafTask {

    private final Pattern[] patterns;
    private final int[] ids;
    private final Coordinate coordinate;
    private final Area area;

    public DarkGroundItemTake(Area area) {
        this(null, null, null, area);
    }

    public DarkGroundItemTake(String[] names, Area area) {
        this(stringToPattern(names), area);
    }

    public DarkGroundItemTake(String[] names, Coordinate coordinate) {
        this(stringToPattern(names), coordinate);
    }

    public DarkGroundItemTake(int[] ids, Area area) {
        this(null, ids, null, area);
    }

    public DarkGroundItemTake(int[] ids, Coordinate coordinate) {
        this(null, ids, coordinate, null);
    }

    public DarkGroundItemTake(Pattern[] patterns, Area area) {
        this(patterns, null, null, area);
    }

    public DarkGroundItemTake(Pattern[] patterns, Coordinate coordinate) {
        this(patterns, null, coordinate, null);
    }

    public DarkGroundItemTake(Pattern[] patterns, int[] ids, Coordinate coordinate, Area area) {
        this.patterns = patterns;
        this.ids = ids;
        this.coordinate = coordinate;
        this.area = area;
    }

    public static Pattern[] stringToPattern(String[] strings){
        return Arrays.stream(strings).map(Regex::getPatternForExactString).toArray(Pattern[]::new);
    }

    @Override
    public void execute() {
        GroundItemQueryBuilder builder = GroundItems.newQuery();
        String loggerDef = "";
        if (patterns != null) {
            builder = builder.names(patterns);
            loggerDef = "Patterns : " + patterns;
        }
        if (ids != null) {
            builder = builder.ids(ids);
            loggerDef = "Ids : " + ids;
        }
        if (coordinate != null) {
            builder = builder.on(coordinate);
        }
        if (area != null) {
            builder = builder.within(area);
        }
        GroundItem item = builder.results().nearest();
        Player local = Players.getLocal();
        if (item != null && local != null){
            getLogger().info("Taking " + item);
            if (item.interact("Take"))
                Execution.delayUntil(()-> Inventory.contains(item.getId()), local::isMoving, 1800, 2400);
        } else {
            getLogger().debug("GroundItem " + loggerDef + " null");
        }
    }
}
