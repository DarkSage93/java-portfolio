package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.NpcQueryBuilder;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class DarkNpcInteract extends LeafTask {

    private final Pattern name;
    private final int id;
    private final String action;
    private final Area area;
    private final Coordinate coordinate;
    private final Callable<Boolean> delay;

    public DarkNpcInteract(int id, String action, Coordinate coordinate, Callable<Boolean> delay) {
        this(null, id, action, null, coordinate, delay);
    }

    public DarkNpcInteract(int id, String action, Coordinate coordinate) {
        this(null, id, action, null, coordinate, null);
    }

    public DarkNpcInteract(int id, Coordinate coordinate, Callable<Boolean> delay) {
        this(null, id, null, null, coordinate, delay);
    }

    public DarkNpcInteract(int id, Coordinate coordinate) {
        this(null, id, null, null, coordinate, null);
    }

    public DarkNpcInteract(int id, String action, Callable<Boolean> delay) {
        this(null, id, action, null, null, delay);
    }

    public DarkNpcInteract(int id, String action) {
        this(null, id, action, null, null, null);
    }

    public DarkNpcInteract(int id, String action, Area area, Callable<Boolean> delay) {
        this(null, id, action, area, null, delay);
    }

    public DarkNpcInteract(int id, String action, Area area) {
        this(null, id, action, area, null, null);
    }

    public DarkNpcInteract(int id, Area area, Callable<Boolean> delay) {
        this(null, id, null, area, null, delay);
    }

    public DarkNpcInteract(int id, Area area) {
        this(null, id, null, area, null, null);
    }

    public DarkNpcInteract(int id, Callable<Boolean> delay) {
        this(null, id, null, null, null, delay);
    }

    public DarkNpcInteract(int id) {
        this(null, id, null, null, null, null);
    }

    public DarkNpcInteract(Pattern name, String action, Coordinate coordinate, Callable<Boolean> delay) {
        this(name, -1, action, null, coordinate, delay);
    }

    public DarkNpcInteract(Pattern name, String action, Coordinate coordinate) {
        this(name, -1, action, null, coordinate, null);
    }

    public DarkNpcInteract(Pattern name, Coordinate coordinate, Callable<Boolean> delay) {
        this(name, -1, null, null, coordinate, delay);
    }

    public DarkNpcInteract(Pattern name, Coordinate coordinate) {
        this(name, -1, null, null, coordinate, null);
    }

    public DarkNpcInteract(Pattern name, String action, Area area, Callable<Boolean> delay) {
        this(name, -1, action, area, null, delay);
    }

    public DarkNpcInteract(Pattern name, String action, Area area) {
        this(name, -1, action, area, null, null);
    }

    public DarkNpcInteract(Pattern name, Area area, Callable<Boolean> delay) {
        this(name, -1, null, area, null, delay);
    }

    public DarkNpcInteract(Pattern name, Area area) {
        this(name, -1, null, area, null, null);
    }

    public DarkNpcInteract(Pattern name, String action, Callable<Boolean> delay) {
        this(name, -1, action, null, null, delay);
    }

    public DarkNpcInteract(Pattern name, String action) {
        this(name, -1, action, null, null, null);
    }
    public DarkNpcInteract(Pattern name, Callable<Boolean> delay) {
        this(name, -1, null, null, null, delay);
    }

    public DarkNpcInteract(Pattern name) {
        this(name, -1, null, null, null, null);
    }

    public DarkNpcInteract(String name, String action, Coordinate coordinate, Callable<Boolean> delay) {
        this(toPattern(name), -1, action, null, coordinate, delay);
    }

    public DarkNpcInteract(String name, String action, Coordinate coordinate) {
        this(toPattern(name), -1, action, null, coordinate, null);
    }

    public DarkNpcInteract(String name, Coordinate coordinate, Callable<Boolean> delay) {
        this(toPattern(name), -1, null, null, coordinate, delay);
    }

    public DarkNpcInteract(String name, Coordinate coordinate) {
        this(toPattern(name), -1, null, null, coordinate, null);
    }

    public DarkNpcInteract(String name, String action, Area area, Callable<Boolean> delay) {
        this(toPattern(name), -1, action, area, null, delay);
    }

    public DarkNpcInteract(String name, String action, Area area) {
        this(toPattern(name), -1, action, area, null, null);
    }

    public DarkNpcInteract(String name, Area area, Callable<Boolean> delay) {
        this(toPattern(name), -1, null, area, null, delay);
    }

    public DarkNpcInteract(String name, Area area) {
        this(toPattern(name), -1, null, area, null, null);
    }

    public DarkNpcInteract(String name, String action, Callable<Boolean> delay) {
        this(toPattern(name), -1, action, null, null, delay);
    }

    public DarkNpcInteract(String name, String action) {
        this(toPattern(name), -1, action, null, null, null);
    }
    public DarkNpcInteract(String name, Callable<Boolean> delay) {
        this(toPattern(name), -1, null, null, null, delay);
    }

    public DarkNpcInteract(String name) {
        this(toPattern(name), -1, null, null, null, null);
    }

    public DarkNpcInteract(Pattern name, int id, String action, Area area, Coordinate coordinate, Callable<Boolean> delay) {
        this.name = name;
        this.id = id;
        this.action = action;
        this.area = area;
        this.coordinate = coordinate;
        this.delay = delay;
    }

    private static Pattern toPattern(String text) {
        return Regex.getPatternForExactString(text);
    }

    private StringBuilder logDef;
    @Override
    public void execute() {
        NpcQueryBuilder builder = Npcs.newQuery();
        if (name != null) {
            builder = builder.names(name);
          //  logDef.append("Names : ").append(name).append(", ");
        }
        if (id != -1) {
            builder = builder.ids(id);
         //   logDef.append("Ids : ").append(id).append(", ");
        }
        if (action != null) {
            builder = builder.actions(action);
          //  logDef.append("Actions : ").append(action).append(", ");
        }
        if (area != null) {
            builder = builder.within(area);
          //  logDef.append("Area : ").append(area);
        }
        if (coordinate != null) {
            builder = builder.on(coordinate);
           // logDef.append("Coordinate : ").append(coordinate);
        }
        Npc npc = builder.results().first();
        if (npc != null) {
            if (npc.isVisible()) {
                boolean isSuccess = false;
                if (action != null) {
                    getLogger().info("Using " + action + " on " + npc);
                    if (npc.interact(action))
                        isSuccess = true;
                } else {
                    getLogger().info("Clicking on " + npc);
                    if (npc.click())
                        isSuccess = true;
                }
                if (isSuccess) {
                    try {
                        if (delay != null) {
                            delay.call();
                        } else {
                            Delay.ticks1().call();
                        }
                    } catch (Exception e) {
                        getLogger().debug(getClass().getName() + " delay failed : " + e.getCause());
                        e.printStackTrace();
                    }
                }
            } else {
                getLogger().debug("Camera : " + npc);
                Camera.turnTo(npc);
            }
        } else {
            getLogger().debug("Npc " + " was null");
        }
    }
}
