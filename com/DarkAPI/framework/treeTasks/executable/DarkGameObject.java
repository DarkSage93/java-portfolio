package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.GameObjectQueryBuilder;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class DarkGameObject extends LeafTask {

    private final Pattern[] names;
    private final int[] ids;
    private final String action;
    private final Area area;
    private final Coordinate coordinate;
    private final Callable<Boolean> delay;

    public DarkGameObject(Callable<Boolean> delay, Area area, String action, int... ids) {
        this(delay, area, null, action, ids);
    }

    public DarkGameObject(Area area, String action, int... ids) {
        this(null, area, null, action, ids);
    }

    public DarkGameObject(Callable<Boolean> delay, Coordinate coordinate, String action, int... ids) {
        this(delay, null, coordinate, action, ids);
    }

    public DarkGameObject(Coordinate coordinate, String action, int... ids) {
        this(null, null, coordinate, action, ids);
    }

    public DarkGameObject(Callable<Boolean> delay, String action, int... ids) {
        this(delay, null, null, action, ids);
    }

    public DarkGameObject(String action, int... ids) {
        this(null, null, null, action, ids);
    }

    public DarkGameObject(Callable<Boolean> delay, Area area, int... ids) {
        this(delay, area, null, null, ids);
    }

    public DarkGameObject(Area area, int... ids) {
        this(null, area, null, null, ids);
    }

    public DarkGameObject(Callable<Boolean> delay, Coordinate coordinate, int... ids) {
        this(delay, null, coordinate, null, ids);
    }

    public DarkGameObject(Coordinate coordinate, int... ids) {
        this(null, null, coordinate, null, ids);
    }

    public DarkGameObject(Callable<Boolean> delay, int... ids) {
        this(delay, null, null, null, ids);
    }

    public DarkGameObject(int... ids) {
        this(null, null, null, null, ids);
    }

    public DarkGameObject(Callable<Boolean> delay, Area area, String action, Pattern... names) {
        this(delay, area, null, action, null, names);
    }

    public DarkGameObject(Area area, String action, Pattern... names) {
        this(null, area, null, action, null, names);
    }

    public DarkGameObject(Callable<Boolean> delay, Area area, Pattern... names) {
        this(delay, area, null, null, null, names);
    }

    public DarkGameObject(Area area, Pattern... names) {
        this(null, area, null, null, null, names);
    }

    public DarkGameObject(Callable<Boolean> delay, Pattern... names) {
        this(delay, null, null, null, null, names);
    }

    public DarkGameObject(Pattern... names) {
        this(null, null, null, null, null, names);
    }

    public DarkGameObject(Callable<Boolean> delay, Coordinate coordinate, String action, Pattern... names) {
        this(delay, null, coordinate, action, null, names);
    }

    public DarkGameObject(Coordinate coordinate, String action, Pattern... names) {
        this(null, null, coordinate, action, null, names);
    }

    public DarkGameObject(Callable<Boolean> delay, Coordinate coordinate, Pattern... names) {
        this(delay, null, coordinate, null, null, names);
    }

    public DarkGameObject(Coordinate coordinate, Pattern... names) {
        this(null, null, coordinate, null, null, names);
    }
    //end

    public DarkGameObject(Callable<Boolean> delay, Area area, String action, String... names) {
        this(delay, area, null, action, null, toPattern(names));
    }

    public DarkGameObject(Area area, String action, String... names) {
        this(null, area, null, action, null, toPattern(names));
    }

    public DarkGameObject(Callable<Boolean> delay, Area area, String... names) {
        this(delay, area, null, null, null, toPattern(names));
    }

    public DarkGameObject(Area area, String... names) {
        this(null, area, null, null, null, toPattern(names));
    }

    public DarkGameObject(Callable<Boolean> delay, String... names) {
        this(delay, null, null, null, null, toPattern(names));
    }

    public DarkGameObject(Callable<Boolean> delay, String action, String... names) {
        this(delay, null, null, action, null, toPattern(names));
    }

    public DarkGameObject(String... names) {
        this(null, null, null, null, null, toPattern(names));
    }

    public DarkGameObject(Callable<Boolean> delay, Coordinate coordinate, String action, String... names) {
        this(delay, null, coordinate, action, null, toPattern(names));
    }

    public DarkGameObject(Coordinate coordinate, String action, String... names) {
        this(null, null, coordinate, action, null, toPattern(names));
    }

    public DarkGameObject(Callable<Boolean> delay, Coordinate coordinate, String... names) {
        this(delay, null, coordinate, null, null, toPattern(names));
    }

    public DarkGameObject(Coordinate coordinate, String... names) {
        this(null, null, coordinate, null, null, toPattern(names));
    }

    public DarkGameObject(Callable<Boolean> delay, Area area, Coordinate coordinate, String action, int[] ids, Pattern... names) {
        this.delay = delay;
        this.area = area;
        this.coordinate = coordinate;
        this.action = action;
        this.ids = ids;
        this.names = names;
    }

    private static Pattern[] toPattern(String... names) {
        return Arrays.stream(names).map(Regex::getPatternForExactString).toArray(Pattern[]::new);
    }

    @Override
    public void execute() {
        GameObjectQueryBuilder builder = GameObjects.newQuery();
        if (area != null)
            builder = builder.within(area);
        if (coordinate != null)
            builder = builder.on(coordinate);
        if (action != null)
            builder = builder.actions(action);
        if (ids != null)
            builder = builder.ids(ids);
        if (names != null)
            builder = builder.names(names);
        GameObject object = builder.results().first();
        if (object != null) {
            if (action != null ) {
                getLogger().info("Using " + action + " on " + object);
                if (object.interact(action)) {
                    try {
                        if (delay != null) {
                            delay.call();
                        } else {
                            Delay.ticks3().call();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                getLogger().info("Clicking on " + object);
                if (object.click()) {
                    try {
                        if (delay != null) {
                            delay.call();
                        } else {
                            Delay.ticks3().call();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            getLogger().debug("GameObject was null");
        }
    }
}
