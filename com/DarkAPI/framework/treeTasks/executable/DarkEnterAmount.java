package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.local.hud.interfaces.EnterAmountDialog;
import com.runemate.game.api.script.framework.tree.LeafTask;

public class DarkEnterAmount extends LeafTask {

    private final int quantity;

    public DarkEnterAmount(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public void execute() {
        if (EnterAmountDialog.isOpen()) {
            getLogger().info("Typing " + quantity);
            if (EnterAmountDialog.enterAmount(quantity)) {
                try {
                    Delay.reaction().call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            getLogger().debug("EnterAmountDialog is not open");
        }
    }
}
