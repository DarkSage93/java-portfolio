package com.DarkAPI.framework.treeTasks.executable;

import com.DarkAPI.util.Delay;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceWindows;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class DarkEquippedItemAction extends LeafTask {

    private final Pattern pattern;
    private final int itemID;
    private final String action;
    private final Callable<Boolean> delay;

    public DarkEquippedItemAction(int itemID, String action) {
        this(itemID, action, null);
    }

    public DarkEquippedItemAction(int itemID, String action, Callable<Boolean> delay) {
        this(null, itemID, action, delay);
    }

    public DarkEquippedItemAction(String text, String action) {
        this(toPattern(text), action, null);
    }

    public DarkEquippedItemAction(String text, String action, Callable<Boolean> delay) {
        this(toPattern(text), action, delay);
    }

    public DarkEquippedItemAction(Pattern pattern, String action) {
        this(pattern, action, null);
    }

    public DarkEquippedItemAction(Pattern pattern, String action, Callable<Boolean> delay) {
        this(pattern, -1, action, delay);
    }

    public DarkEquippedItemAction(Pattern pattern, int itemID, String action, Callable<Boolean> delay) {
        this.pattern = pattern;
        this.action = action;
        this.itemID = itemID;
        this.delay = delay;
    }

    private static Pattern toPattern(String text) {
        return Regex.getPatternForExactString(text);
    }

    @Override
    public void execute() {
        if (!InterfaceWindows.getEquipment().isOpen()) {
            getLogger().info("Opening Equipment Tab");
            if (InterfaceWindows.getEquipment().open()) {
                try {
                    Delay.reaction().call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        SpriteItem item;
        if (pattern != null) {
            item = Equipment.newQuery().names(pattern).results().first();
        } else {
            item = Equipment.newQuery().ids(itemID).results().first();
        }
        if (item != null) {
            getLogger().info("Using " + action + " on " + item);
            if (item.interact(action)) {
                try {
                    if (delay != null) {
                        delay.call();
                    } else {
                        Delay.ticks1().call();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            getLogger().debug("Equipment : SpriteItem : null");
        }
    }
}
