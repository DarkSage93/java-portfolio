package com.DarkAPI.common.net.OSRSBox;

import com.DarkAPI.common.net.HTTP;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.runemate.game.api.hybrid.Environment;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class OsrsBoxSearch {
    private static final Map<Integer, ItemProperties> cache = Collections.synchronizedMap(new HashMap<>());
    private static final String BASE_URL = "https://www.osrsbox.com/osrsbox-db/";
    private static final String ITEM_FOLDER = "items-json/";

    @Nullable
    public static ItemProperties searchItem(int id){
        ItemProperties properties = cache.get(id);
        if(properties == null) {
            Environment.getLogger().debug(String.format("Fetching item %d from OSRSBox", id));
            String json = HTTP.httpGET(BASE_URL + ITEM_FOLDER + id + ".json");
            Gson gson = new GsonBuilder().registerTypeAdapter(ItemProperties.class, ItemProperties.getAdapter()).create();
            try {
                properties = gson.fromJson(json, ItemProperties.class);
                if(properties != null){
                    cache.put(id, properties);
                }
            } catch (JsonSyntaxException e) {
                Environment.getLogger().debug(String.format("Gson parsing of item %d from OSRSBox failed", id));
                e.printStackTrace();
            }
        }
        return properties;
    }
}
