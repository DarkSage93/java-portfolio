package com.DarkAPI.common.net.OSRSBox;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;

public class ItemProperties {
    //Unique OSRS item ID number.
    public int id;

    @Nonnull
    //The name of the item.
    public String name = "";

    @Nonnull
    //The last time (UTC) the item was updated (in ISO8601 date format).
    public String last_updated = "";

    //If the item has incomplete wiki data.
    public boolean incomplete;

    //If the item is a members-only.
    public boolean members;

    //If the item is tradeable (between players and on the GE).
    public boolean tradeable;

    //If the item is tradeable (only on GE).
    public boolean tradeable_on_ge;

    //If the item is stackable (in inventory).
    public boolean stackable;

    @Nullable
    //If the item is stacked, indicated by the stack count.
    public Integer stacked;

    //If the item is noted.
    public boolean noted;

    //If the item is noteable.
    public boolean noteable;

    @Nullable
    //The linked ID of the actual item (if noted/placeholder).
    public Integer linked_id_item;

    @Nullable
   //The linked ID of an item in noted form.
    public Integer linked_id_noted;

    @Nullable
    //The linked ID of an item in placeholder form.
    public Integer linked_id_placeholder;

    //If the item is a placeholder.
    public boolean placeholder;

    //If the item is equipable (based on right-click menu entry).
    public boolean equipable;

    //If the item is equipable in-game by a player.
    public boolean equipable_by_player;

    //If the item is an equipable weapon.
    public boolean equipable_weapon;

    //The store price of an item.
    public int cost;

    @Nullable
    //The low alchemy value of the item (cost * 0.4).
    public Integer lowalch;

    @Nullable
    //The high alchemy value of the item (cost * 0.6).
    public Integer highalch;

    @Nullable
    //The weight (in kilograms) of the item.
    public Float weight;

    @Nullable
    //The Grand Exchange buy limit of the item.
    public Integer buy_limit;

    //If the item is associated with a quest.
    public boolean quest_item;

    @Nullable
    //Date the item was released (in ISO8601 format).
    public String release_date;

    //If the item is a duplicate.
    public boolean duplicate;

    @Nullable
    //The examine text for the item.
    public String examine;

    @Nonnull
    //The item icon (in base64 encoding).
    public String icon = "";

    @Nullable
    //The OSRS Wiki name for the item.
    public String wiki_name;

    @Nullable
    //The OSRS Wiki URL (possibly including anchor link).
    public String wiki_url;

    @Nullable
    //The equipment bonuses of equipable armour/weapons.
    public EquipmentProperties equipment;

    @Nullable
    //The weapon bonuses including attack speed, type and stance.
    public WeaponProperties weapon;



    public static TypeAdapter<ItemProperties> getAdapter(){
        return new TypeAdapter<ItemProperties>() {
            @Override
            public void write(JsonWriter jsonWriter, ItemProperties itemProperties) throws IOException {
                throw new IOException("The adapter for ItemProperties has not been implemented");
            }

            @Override
            public ItemProperties read(JsonReader jsonReader) throws IOException {
                ItemProperties itemProperties = new ItemProperties();
                jsonReader.beginObject();
                while (jsonReader.hasNext()){
                    String name = jsonReader.nextName();
                    if(jsonReader.peek().equals(JsonToken.NULL)){
                        jsonReader.skipValue();
                        continue;
                    }
                    switch (name){
                        case "id":
                            itemProperties.id = jsonReader.nextInt();
                            break;
                        case "name":
                            itemProperties.name = jsonReader.nextString();
                            break;
                        case "last_updated":
                            itemProperties.last_updated = jsonReader.nextString();
                            break;
                        case "incomplete":
                            itemProperties.incomplete = jsonReader.nextBoolean();
                            break;
                        case "members":
                            itemProperties.members = jsonReader.nextBoolean();
                            break;
                        case "tradeable":
                            itemProperties.tradeable = jsonReader.nextBoolean();
                            break;
                        case "tradeable_on_ge":
                            itemProperties.tradeable_on_ge = jsonReader.nextBoolean();
                            break;
                        case "stackable":
                            itemProperties.stackable = jsonReader.nextBoolean();
                            break;
                        case "stacked":
                            itemProperties.stacked = jsonReader.nextInt();
                            break;
                        case "noted":
                            itemProperties.noted = jsonReader.nextBoolean();
                            break;
                        case "noteable":
                            itemProperties.noteable = jsonReader.nextBoolean();
                            break;
                        case "linked_id_item":
                            itemProperties.linked_id_item = jsonReader.nextInt();
                            break;
                        case "linked_id_noted":
                            itemProperties.linked_id_noted = jsonReader.nextInt();
                            break;
                        case "linked_id_placeholder":
                            itemProperties.linked_id_placeholder = jsonReader.nextInt();
                            break;
                        case "placeholder":
                            itemProperties.placeholder = jsonReader.nextBoolean();
                            break;
                        case "equipable":
                            itemProperties.equipable = jsonReader.nextBoolean();
                            break;
                        case "equipable_by_player":
                            itemProperties.equipable_by_player = jsonReader.nextBoolean();
                            break;
                        case "equipable_weapon":
                            itemProperties.equipable_weapon = jsonReader.nextBoolean();
                            break;
                        case "cost":
                            itemProperties.cost = jsonReader.nextInt();
                            break;
                        case "lowalch":
                            itemProperties.lowalch = jsonReader.nextInt();
                            break;
                        case "highalch":
                            itemProperties.highalch = jsonReader.nextInt();
                            break;
                        case "weight":
                            itemProperties.weight = (float)jsonReader.nextDouble();
                            break;
                        case "buy_limit":
                            itemProperties.buy_limit = jsonReader.nextInt();
                            break;
                        case "quest_item":
                            itemProperties.quest_item = jsonReader.nextBoolean();
                            break;
                        case "release_date":
                            itemProperties.release_date = jsonReader.nextString();
                            break;
                        case "duplicate":
                            itemProperties.duplicate = jsonReader.nextBoolean();
                            break;
                        case "examine":
                            itemProperties.examine = jsonReader.nextString();
                            break;
                        case "icon":
                            itemProperties.icon = jsonReader.nextString();
                            break;
                        case "wiki_name":
                            itemProperties.wiki_name = jsonReader.nextString();
                            break;
                        case "wiki_url":
                            itemProperties.wiki_url = jsonReader.nextString();
                            break;
                        case "equipment":
                            itemProperties.equipment = EquipmentProperties.getAdapter().read(jsonReader);
                            break;
                        case "weapon":
                            itemProperties.weapon = WeaponProperties.getAdapter().read(jsonReader);
                            break;
                        default:
                            jsonReader.skipValue();
                            break;
                    }
                }
                jsonReader.endObject();
                return itemProperties;
            }
        };
    }


}
