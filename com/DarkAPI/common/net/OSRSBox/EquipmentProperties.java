package com.DarkAPI.common.net.OSRSBox;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EquipmentProperties {
    //The attack stab bonus of the item.
    public int attack_stab;

    //The attack slash bonus of the item.
    public int attack_slash;

    //The attack crush bonus of the item.
    public int attack_crush;

    //The attack magic bonus of the item.
    public int attack_magic;

    //The attack ranged bonus of the item.
    public int attack_ranged;

    //The defence stab bonus of the item.
    public int defence_stab;

    //The defence slash bonus of the item.
    public int defence_slash;

    //The defence crush bonus of the item.
    public int defence_crush;

    //The defence magic bonus of the item.
    public int defence_magic;

    //The defence ranged bonus of the item.
    public int defence_ranged;

    //The melee strength bonus of the item.
    public int melee_strength;

    //The ranged strength bonus of the item.
    public int ranged_strength;

    //The magic damage bonus of the item.
    public int magic_damage;

    //The prayer bonus of the item.
    public int prayer;

    @Nonnull
    //The equipment slot associated with the item (e.g., head).
    public Equipment.Slot slot;

    public boolean isTwoHanded;

    @Nullable
    //An object of requirements {skill: level}.
    public Map<String, Integer> requirements;

    public static TypeAdapter<EquipmentProperties> getAdapter() {
        return new TypeAdapter<EquipmentProperties>() {
            @Override
            public void write(JsonWriter jsonWriter, EquipmentProperties equipmentProperties) throws IOException {
                throw new IOException("The adapter for ItemEquipment has not been implemented");
            }

            @Override
            public EquipmentProperties read(JsonReader jsonReader) throws IOException {
                EquipmentProperties equipmentProperties = new EquipmentProperties();
                jsonReader.beginObject();
                while (jsonReader.hasNext()){
                    String name = jsonReader.nextName();
                    if(jsonReader.peek().equals(JsonToken.NULL)){
                        jsonReader.skipValue();
                        continue;
                    }
                    switch (name){
                        case "attack_stab" :
                            equipmentProperties.attack_stab = jsonReader.nextInt();
                            break;
                        case "attack_slash" :
                            equipmentProperties.attack_slash = jsonReader.nextInt();
                            break;
                        case "attack_crush" :
                            equipmentProperties.attack_crush = jsonReader.nextInt();
                            break;
                        case "attack_magic" :
                            equipmentProperties.attack_magic = jsonReader.nextInt();
                            break;
                        case "attack_ranged" :
                            equipmentProperties.attack_ranged = jsonReader.nextInt();
                            break;
                        case "defence_stab" :
                            equipmentProperties.defence_stab = jsonReader.nextInt();
                            break;
                        case "defence_slash" :
                            equipmentProperties.defence_slash = jsonReader.nextInt();
                            break;
                        case "defence_crush" :
                            equipmentProperties.defence_crush = jsonReader.nextInt();
                            break;
                        case "defence_magic" :
                            equipmentProperties.defence_magic = jsonReader.nextInt();
                            break;
                        case "defence_ranged" :
                            equipmentProperties.defence_ranged = jsonReader.nextInt();
                            break;
                        case "melee_strength" :
                            equipmentProperties.melee_strength = jsonReader.nextInt();
                            break;
                        case "ranged_strength" :
                            equipmentProperties.ranged_strength = jsonReader.nextInt();
                            break;
                        case "magic_damage" :
                            equipmentProperties.magic_damage = jsonReader.nextInt();
                            break;
                        case "prayer" :
                            equipmentProperties.prayer = jsonReader.nextInt();
                            break;
                        case "slot" :
                            String slot = jsonReader.nextString();
                            equipmentProperties.slot = osrsboxSlotToRunemate.get(slot);
                            equipmentProperties.isTwoHanded = isTwoHanded(slot);
                            break;
                        case "requirements":
                        default:
                            jsonReader.skipValue();
                            break;

                    }
                }
                jsonReader.endObject();
                return equipmentProperties;
            }
        };
    }

    public static Map<String, Equipment.Slot> osrsboxSlotToRunemate = new HashMap<>();
    static {
        osrsboxSlotToRunemate.put("2h",Equipment.Slot.WEAPON);
        osrsboxSlotToRunemate.put("ammo",Equipment.Slot.AMMUNITION);
        osrsboxSlotToRunemate.put("body",Equipment.Slot.BODY);
        osrsboxSlotToRunemate.put("cape",Equipment.Slot.CAPE);
        osrsboxSlotToRunemate.put("feet",Equipment.Slot.FEET);
        osrsboxSlotToRunemate.put("hands",Equipment.Slot.HANDS);
        osrsboxSlotToRunemate.put("head",Equipment.Slot.HEAD);
        osrsboxSlotToRunemate.put("legs",Equipment.Slot.LEGS);
        osrsboxSlotToRunemate.put("neck",Equipment.Slot.NECK);
        osrsboxSlotToRunemate.put("ring",Equipment.Slot.RING);
        osrsboxSlotToRunemate.put("shield",Equipment.Slot.SHIELD);
        osrsboxSlotToRunemate.put("weapon",Equipment.Slot.WEAPON);
    }
    public static boolean isTwoHanded(String slot){
        return slot.equals("2h");
    }


}
