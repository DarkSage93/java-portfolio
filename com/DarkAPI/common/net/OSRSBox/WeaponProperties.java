package com.DarkAPI.common.net.OSRSBox;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WeaponProperties {
    //The attack speed of a weapon (in game ticks).
    public int attack_speed;

    @Nonnull
    //The weapon classification (e.g., axes)
    public String weapon_type = "null";

    @Nonnull
    //An array of weapon stance information.
    public List<WeaponStance> stances = new ArrayList<>();

    public static TypeAdapter<WeaponProperties> getAdapter() {
        return new TypeAdapter<WeaponProperties>() {
            @Override
            public void write(JsonWriter jsonWriter, WeaponProperties weaponProperties) throws IOException {
                throw new IOException("The adapter for ItemEquipment has not been implemented");
            }

            @Override
            public WeaponProperties read(JsonReader jsonReader) throws IOException {
                WeaponProperties weaponProperties = new WeaponProperties();
                jsonReader.beginObject();
                while (jsonReader.hasNext()){
                    switch (jsonReader.nextName()){
                        case "attack_speed":
                            weaponProperties.attack_speed = jsonReader.nextInt();
                            break;
                        case "weapon_type":
                            weaponProperties.weapon_type = jsonReader.nextString();
                            break;
                        case "stances":
                            jsonReader.beginArray();
                            while (jsonReader.hasNext()) {
                                weaponProperties.stances.add(WeaponStance.getAdapter().read(jsonReader));
                            }
                            jsonReader.endArray();
                            break;
                        default:
                            jsonReader.skipValue();
                            break;
                    }
                }
                jsonReader.endObject();
                return weaponProperties;
            }
        };
    }
}
