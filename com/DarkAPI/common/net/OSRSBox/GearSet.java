package com.DarkAPI.common.net.OSRSBox;

import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GearSet implements Serializable {

    private Map<Equipment.Slot, ItemProperties> gear;
    private DamageSpells autoCastSpell;
    private WeaponStance weaponStance;
    private final String name;
    private final static String EQUIPPED_GEAR_NAME = "Equipped";

    public GearSet(String name) {
        this.name = name;
        gear = new HashMap<>();
        for (Equipment.Slot slot : Equipment.Slot.values()) {
            gear.put(slot, null);
        }
    }

    public GearSet(GearSet other) {
        this.name = other.name;
        this.gear = new HashMap<>(other.gear);
        this.autoCastSpell = other.autoCastSpell;
        this.weaponStance = other.weaponStance;
    }

    public void copy(GearSet other) {
        this.gear = new HashMap<>(other.gear);
        this.autoCastSpell = other.autoCastSpell;
        this.weaponStance = other.weaponStance;
    }

    public ItemProperties getItem(@Nullable Equipment.Slot slot) {
        return gear.get(slot);
    }

    public void setItem(@Nullable EquipmentHelper.Slot slot, ItemProperties item) {
        if (slot != null) {
            gear.replace(slot.getCorrespondingSlot(), item);
            EquipmentProperties props = item.equipment;
            if (props != null) {
                if (props.isTwoHanded && props.slot == Equipment.Slot.WEAPON)
                    gear.replace(Equipment.Slot.SHIELD, null);
                if (props.isTwoHanded && props.slot == Equipment.Slot.SHIELD)
                    gear.replace(Equipment.Slot.WEAPON, null);
            }
        }
    }

    public List<ItemProperties> getItems() {
        return new ArrayList<>(gear.values());
    }

    public static GearSet getEquippedGearSet() {
        GearSet set = new GearSet(EQUIPPED_GEAR_NAME);
        for (Equipment.Slot slot : Equipment.Slot.values()) {
            SpriteItem spriteItem = Equipment.getItemIn(slot);
            if(spriteItem != null) {
                set.setItem(EquipmentHelper.Slot.fromCorrespondingSlot(slot), OsrsBoxSearch.searchItem(spriteItem.getId()));
            }
        }
        return set;
    }

    public boolean isEquipped(ItemProperties item) {
        EquipmentProperties props = item.equipment;
        Equipment.Slot slot = props != null ? props.slot : null;
        if (slot == null) return false;
        return Equipment.contains(item.id);
    }
}
