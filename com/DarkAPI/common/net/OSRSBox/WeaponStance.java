package com.DarkAPI.common.net.OSRSBox;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WeaponStance {
    @Nonnull
    public String combat_style = "null";

    @Nonnull
    public AttackType attack_type = AttackType.STAB;

    public enum AttackType {
        STAB("stab", false),
        RANGED("ranged", false),
        SLASH("slash", false),
        CRUSH("crush", false),
        MAGIC("magic", false),
        DEFENSIVE_CASTING("defensive casting", true),
        SPELLCASTING("spellcasting", true)
        ;
        public final String serializedName;
        public final boolean allowsAutocast;

        AttackType(String serializedName, boolean allowsAutocast) {
            this.serializedName = serializedName;
            this.allowsAutocast = allowsAutocast;
        }
        public static AttackType fromName(String name) {
            return Arrays.stream(values()).filter(type -> type.serializedName.equals(name)).findAny().orElse(null);
        }
    }

    @Nullable
    public String attack_style;

    @Nullable
    public String experience;

    @Nonnull
    public List<Boost> boosts = new ArrayList<>();

    public enum Boost {
        ATTACK_SPEED("attack speed by 1 tick"),
        ATTACK_RANGE("attack range by 2 squares"),
        ACCURACY_AND_DAMAGE("accuracy and damage"),
        ;
        private final String serializedName;

        Boost(String serializedName) {
            this.serializedName = serializedName;
        }
        public static Boost fromName(String name) {
            return Arrays.stream(values()).filter(boost -> boost.serializedName.equals(name)).findAny().orElse(null);
        }
    }

    public static TypeAdapter<WeaponStance> getAdapter() {
        return new TypeAdapter<WeaponStance>() {
            @Override
            public void write(JsonWriter jsonWriter, WeaponStance weaponStance) throws IOException {
                throw new IOException("The adapter for ItemEquipment has not been implemented");
            }

            @Override
            public WeaponStance read(JsonReader jsonReader) throws IOException {
                WeaponStance weaponStance = new WeaponStance();
                jsonReader.beginObject();
                while (jsonReader.hasNext()){
                    String name = jsonReader.nextName();
                    if(jsonReader.peek().equals(JsonToken.NULL)){
                        jsonReader.skipValue();
                        continue;
                    }
                    switch (name){
                        case "attack_style":
                            weaponStance.attack_style = jsonReader.nextString();
                            break;
                        case "attack_type":
                            weaponStance.attack_type = AttackType.fromName(jsonReader.nextString());
                            break;
                        case "combat_style":
                            weaponStance.combat_style = jsonReader.nextString();
                            break;
                        case "experience":
                            weaponStance.experience = jsonReader.nextString();
                            break;
                        case "boosts":
                            Boost boost = Boost.fromName(jsonReader.nextString());
                            if(boost != null){
                                weaponStance.boosts.add(boost);
                            }
                            break;
                        default:
                            jsonReader.skipValue();
                            break;
                    }
                }
                jsonReader.endObject();
                return weaponStance;
            }
        };
    }
}
