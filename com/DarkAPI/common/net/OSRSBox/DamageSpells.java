package com.DarkAPI.common.net.OSRSBox;

import com.DarkAPI.common.OSRSBox.Utils;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.Spell;
import com.runemate.game.api.osrs.local.hud.interfaces.Magic;

import java.awt.image.BufferedImage;

public enum DamageSpells {

    WIND_STRIKE(2, true, Magic.Book.STANDARD),
    WATER_STRIKE(4, true, Magic.Book.STANDARD),
    EARTH_STRIKE(6, true, Magic.Book.STANDARD),
    FIRE_STRIKE(8, true, Magic.Book.STANDARD),
    WIND_BOLT(9, true, Magic.Book.STANDARD),
    WATER_BOLT(10, true, Magic.Book.STANDARD),
    EARTH_BOLT(11, true, Magic.Book.STANDARD),
    FIRE_BOLT(12, true, Magic.Book.STANDARD),
    WIND_BLAST(13, true, Magic.Book.STANDARD),
    WATER_BLAST(14, true, Magic.Book.STANDARD),
    EARTH_BLAST(15, true, Magic.Book.STANDARD),
    FIRE_BLAST(16, true, Magic.Book.STANDARD),
    WIND_WAVE(17, true, Magic.Book.STANDARD),
    WATER_WAVE(18, true, Magic.Book.STANDARD),
    EARTH_WAVE(19, true, Magic.Book.STANDARD),
    FIRE_WAVE(20, true, Magic.Book.STANDARD),
    WIND_SURGE(21, true, Magic.Book.STANDARD),
    WATER_SURGE(22, true, Magic.Book.STANDARD),
    EARTH_SURGE(23, true, Magic.Book.STANDARD),
    FIRE_SURGE(24, true, Magic.Book.STANDARD),
    SMOKE_RUSH(14, false, Magic.Book.ANCIENT),
    SHADOW_RUSH(15, false, Magic.Book.ANCIENT),
    BLOOD_RUSH(16, false, Magic.Book.ANCIENT),
    ICE_RUSH(17, false, Magic.Book.ANCIENT),
    SMOKE_BURST(18, false, Magic.Book.ANCIENT),
    SHADOW_BURST(19, false, Magic.Book.ANCIENT),
    BLOOD_BURST(21, false, Magic.Book.ANCIENT),
    ICE_BURST(22, false, Magic.Book.ANCIENT),
    SMOKE_BLITZ(23, false, Magic.Book.ANCIENT),
    SHADOW_BLITZ(24, false, Magic.Book.ANCIENT),
    BLOOD_BLITZ(25, false, Magic.Book.ANCIENT),
    ICE_BLITZ(26, false, Magic.Book.ANCIENT),
    SMOKE_BARRAGE(27, false, Magic.Book.ANCIENT),
    SHADOW_BARRAGE(28, false, Magic.Book.ANCIENT),
    BLOOD_BARRAGE(29, false, Magic.Book.ANCIENT),
    ICE_BARRAGE(30, false, Magic.Book.ANCIENT),
    CRUMBLE_UNDEAD(15, false, Magic.Book.STANDARD),
    IBAN_BLAST(25, false, Magic.Book.STANDARD),
    SARADOMIN_STRIKE(20, false, Magic.Book.STANDARD),
    CLAWS_OF_GUTHIX(20, false, Magic.Book.STANDARD),
    FLAMES_OF_ZAMORAK(20, false, Magic.Book.STANDARD),
    SLAYER_DART(),
    TRIDENT_OF_THE_SEAS(),
    TRIDENT_OF_THE_SWAMP(),
    SALAMANDER_BLACK(),
    SALAMANDER_RED(),
    SALAMANDER_ORANGE(),
    SALAMANDER_SWAMP();

    private int maxHit;
    private boolean autocastable;
    private Magic.Book book;
    private BufferedImage icon;

    DamageSpells() {
    }

    DamageSpells(boolean autocastable) {
        this.autocastable = autocastable;
    }

    DamageSpells(int maxHit, boolean autocastable, Magic.Book book) {
        this.maxHit = maxHit;
        this.autocastable = autocastable;
        this.book = book;
    }


    public int getMaxHit() {
        switch(this){
            case SLAYER_DART: return 10 + Skill.MAGIC.getCurrentLevel()/10;
            case TRIDENT_OF_THE_SEAS:return Skill.MAGIC.getCurrentLevel()/3 - 5;
            case TRIDENT_OF_THE_SWAMP:return Skill.MAGIC.getCurrentLevel()/3 - 2;
            case SALAMANDER_BLACK:return (int)(0.5 + Skill.MAGIC.getCurrentLevel()*(64 + 92)/640);
            case SALAMANDER_RED:return (int)(0.5 + Skill.MAGIC.getCurrentLevel()*(64 + 77)/640);
            case SALAMANDER_ORANGE:return (int)(0.5 + Skill.MAGIC.getCurrentLevel()*(64 + 59)/640);
            case SALAMANDER_SWAMP:return (int)(0.5 + Skill.MAGIC.getCurrentLevel()*(64 + 56)/640);
        }
        return maxHit;
    }
    public static DamageSpells get(int index){
        if(index < 0 || index >= DamageSpells.values().length) return null;
        return DamageSpells.values()[index];
    }

    public Spell getSpell() {
        Magic.Book book = getBook();
        if (book != null) {
        if (book.equals(Magic.Book.STANDARD))
            return Magic.valueOf(this.toString());
        if (book.equals(Magic.Book.ANCIENT))
            return Magic.Ancient.valueOf(this.toString());
        }
        return null;
    }

    public Magic.Book getBook() {
        if (this.book != null) {
            if (this.book.equals(Magic.Book.STANDARD))
                return Magic.Book.STANDARD;
            if (this.book.equals(Magic.Book.ANCIENT))
                return Magic.Book.ANCIENT;
        }
        return null;
    }

    public boolean isAutocastable() {
        return autocastable;
    }

    public static void loadIcons(){
        for (DamageSpells spell : DamageSpells.values()) {
            spell.icon = Utils.loadImage("com/DarkAPI/common/images/Spells/" + spell.toString().toLowerCase() + ".png");
        }
    }

    public BufferedImage getIcon() {
        return icon;
    }
}
