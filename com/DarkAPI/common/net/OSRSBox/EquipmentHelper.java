package com.DarkAPI.common.net.OSRSBox;

import com.DarkAPI.common.OSRSBox.Item;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class EquipmentHelper {

    public List<Item> getItems(){
        List<Item> list = new ArrayList<>();
        Equipment.getItems().stream().filter(item -> item.getDefinition() != null).forEach(item-> list.add(new Item(item.getDefinition().getName())));
        return list;
    }

    public enum Slot {
        AMMUNITION(Equipment.Slot.AMMUNITION),
        AURA(Equipment.Slot.AURA),
        BODY(Equipment.Slot.BODY),
        CAPE(Equipment.Slot.CAPE),
        FEET(Equipment.Slot.FEET),
        HEAD(Equipment.Slot.HEAD),
        HANDS(Equipment.Slot.HANDS),
        LEGS(Equipment.Slot.LEGS),
        NECK(Equipment.Slot.NECK),
        POCKET(Equipment.Slot.POCKET),
        RING(Equipment.Slot.RING),
        SHIELD(Equipment.Slot.SHIELD),
        WEAPON(Equipment.Slot.WEAPON),
        TWO_HANDED(Equipment.Slot.WEAPON);

        Equipment.Slot correspondingSlot;
        Slot(Equipment.Slot slot){
            this.correspondingSlot = slot;
        }

        public Equipment.Slot getCorrespondingSlot() {
            return correspondingSlot;
        }
        public static EquipmentHelper.Slot fromCorrespondingSlot(Equipment.Slot correspondingSlot){
            for (Slot slot : Slot.values()) {
                if(slot.getCorrespondingSlot() == correspondingSlot){
                    return slot;
                }
            }
            return null;
        }

        public static EquipmentHelper.Slot getSlot(String slot) {
            String caps = slot.toUpperCase(Locale.ROOT);
            return Arrays.stream(values()).filter((val) -> val.name().equals(caps)).findFirst().orElse(null);
        }
    }
}
