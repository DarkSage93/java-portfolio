package com.DarkAPI.common.util;

import javafx.scene.control.TextFormatter;
import javafx.util.StringConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

public class TextFormatters {

    public static TextFormatter<String> getStringFormatter() {
        return getStringFormatter(Pattern.compile(".*"));
    }

    public static TextFormatter<String> getStringFormatter(Pattern validity){
        UnaryOperator<TextFormatter.Change> filter = c -> {
            String text = c.getControlNewText();
            if (validity.matcher(text).matches()) {
                return c;
            } else {
                return null ;
            }
        };
        return new TextFormatter<>(TextFormatter.IDENTITY_STRING_CONVERTER, "", filter);
    }

    public static TextFormatter<Double> getDoubleFormatter(int decimals){
        Pattern validEditingState = Pattern.compile("-?(([1-9][0-9]*)|0)?(\\.[0-9]*)?");

        UnaryOperator<TextFormatter.Change> filter = c -> {
            String text = c.getControlNewText();
            if (validEditingState.matcher(text).matches()) {
                return c;
            } else {
                return null ;
            }
        };

        StringConverter<Double> converter = new StringConverter<Double>() {
            @Override
            public Double fromString(String s) {
                if (s.isEmpty() || "-".equals(s) || ".".equals(s) || "-.".equals(s)) {
                    return 0.0 ;
                } else {
                    return Double.valueOf(s);
                }
            }
            @Override
            public String toString(Double d) {
                return String.format("%.0" + decimals + "f", d);
            }
        };

        return new TextFormatter<>(converter, 0.0, filter);
    }

    public static TextFormatter<Integer> getIntegerFormatter(){
        Pattern validEditingState = Pattern.compile("-?(([1-9][0-9]*)|0)?");

        UnaryOperator<TextFormatter.Change> filter = c -> {
            String text = c.getControlNewText();
            if (validEditingState.matcher(text).matches()) {
                return c ;
            } else {
                return null ;
            }
        };

        StringConverter<Integer> converter = new StringConverter<Integer>() {
            @Override
            public Integer fromString(String s) {
                if (s.isEmpty() || "-".equals(s)) {
                    return 0;
                } else {
                    return Integer.valueOf(s);
                }
            }
            @Override
            public String toString(Integer integer) {
                return integer.toString();
            }
        };

        return new TextFormatter<>(converter, 0, filter);
    }

    public static TextFormatter<Date> getDateFormatter(String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.getDateFormatSymbols().getLocalPatternChars();

        Calendar date = new GregorianCalendar();
        // reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        Date timeZero = date.getTime();

        UnaryOperator<TextFormatter.Change> filter = c -> {
            String text = c.getControlNewText();
            try {
                dateFormat.parse(text);
                return c;
            } catch (ParseException e) {
                return null;
            }
        };

        StringConverter<Date> converter = new StringConverter<Date>() {
            @Override
            public Date fromString(String s) {
                try {
                    return dateFormat.parse(s);
                } catch (ParseException e) {
                    return timeZero;
                }
            }
            @Override
            public String toString(Date date) {
                return dateFormat.format(date);
            }

        };

        return new TextFormatter<>(converter, timeZero, filter);
    }
}
