package com.DarkAPI.common.util;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.util.Resources;
import com.runemate.game.api.script.framework.AbstractBot;

import javax.annotation.Nullable;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class IconLoader {

    @Nullable
    public static BufferedImage loadCommonIcon(String name) {
        return loadCommonIcon(Environment.getBot(), name);
    }

    @Nullable
    public static BufferedImage loadCommonIcon(AbstractBot bot, String name){
        return loadIcon(bot, "com/DarkAPI/common/images/" + name);
    }

    @Nullable
    public static BufferedImage loadIcon(AbstractBot bot, String path){
        try {
            return Resources.getAsBufferedImage(bot, path);
        } catch (IOException e) {
            Environment.getLogger().debug(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

}
