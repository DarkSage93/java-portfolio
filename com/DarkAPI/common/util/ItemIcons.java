package com.DarkAPI.common.util;

import com.DarkAPI.common.OSRSBox.ItemProperties;
import com.DarkAPI.common.OSRSBox.OsrsBoxSearch;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class ItemIcons {

    private static final Map<Integer, BufferedImage> icons = new HashMap<>();
    private static final Map<Integer, Instant> latestUpdate = new HashMap<>();

    @Nullable
    public static BufferedImage get(int itemID){
        BufferedImage icon = icons.get(itemID);
        if(icon == null){
            Instant latest = latestUpdate.get(itemID);
            // if(latest == null || TimeUtils.instantIsOlder(latest, 10_000)) {
            if (latest == null) {
                ItemProperties item = OsrsBoxSearch.searchItem(itemID);
                latestUpdate.put(itemID, Instant.now());
                if (item != null) {
                    icon = convertImage(item.icon);
                    if (icon != null) {
                        icons.put(itemID, icon);
                    }
                }
            }
        }
        return icon;
    }

    private static BufferedImage convertImage(String base64ImageString){
        BufferedImage image = null;
        byte[] imageByte;
        if(base64ImageString != null){
            Base64.Decoder decoder = Base64.getDecoder();
            try {
                imageByte = decoder.decode(base64ImageString);
                ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
                image = ImageIO.read(bis);
                bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return image;
    }
}
