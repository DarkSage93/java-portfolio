package com.DarkAPI.common.util;

public class ResourcePaths {

    public static final String IMG_BASE_EQUIPMENT_SLOT = "com/DarkAPI/common/images/EquipmentSlots/";
    public static final String IMG_2H_SLOT = "2hSlot.png";
    public static final String IMG_AMMO_SLOT = "AmmoSlot.png";
    public static final String IMG_BODY_SLOT = "BodySlot.png";
    public static final String IMG_CAPE_SLOT = "CapeSlot.png";
    public static final String IMG_FEET_SLOT = "FeetSlot.png";
    public static final String IMG_HANDS_SLOT = "HandsSlot.png";
    public static final String IMG_HEAD_SLOT = "HeadSlot.png";
    public static final String IMG_LEGS_SLOT = "LegsSlot.png";
    public static final String IMG_NECK_SLOT = "NeckSlot.png";
    public static final String IMG_OCCUPIED_SLOT = "OccupiedSlot.png";
    public static final String IMG_RING_SLOT = "RingSlot.png";
    public static final String IMG_SELECTED_SLOT = "SelectedSlot.png";
    public static final String IMG_SHIELD_SLOT = "ShieldSlot.png";
    public static final String IMG_WEAPON_SLOT = "WeaponSlot.png";

    public static final String IMG_CONTROLPANEL_TAB_BACKGROUND = "com/DarkAPI/common/images/ControlPanelTab/InventoryTab.png";
}
