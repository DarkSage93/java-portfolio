package com.DarkAPI.common.OSRSBox;

public class ItemDrop extends Item {

    private final int qty;
    private final float dropChance;
    private final boolean noted;

    public ItemDrop(String name, int qty, float dropChance, boolean noted) {
        super(name);
        this.qty = qty;
        this.dropChance = dropChance;
        this.noted = noted;
    }

    public ItemDrop(int id, int qty, float dropChance, boolean noted) {
        super(id);
        this.qty = qty;
        this.dropChance = dropChance;
        this.noted = noted;
    }

}
