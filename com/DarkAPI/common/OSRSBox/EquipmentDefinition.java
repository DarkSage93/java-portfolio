package com.DarkAPI.common.OSRSBox;

import com.DarkAPI.common.net.OSRSBox.EquipmentHelper;
import com.regal.utility.awesome_navigation.requirements.SkillRequirement;

import java.util.List;

public class EquipmentDefinition {

    private final EquipmentBonuses equipmentBonuses;
    private final EquipmentHelper.Slot slot;
    private final List<SkillRequirement> requirements;


    public EquipmentDefinition(int attackStab,
                               int attackSlash,
                               int attackCrush,
                               int attackMagic,
                               int attackRanged,
                               int defenceStab,
                               int defenceSlash,
                               int defenceCrush,
                               int defenceMagic,
                               int defenceRanged,
                               int meleeStrength,
                               int rangedStrength,
                               int magicDamage,
                               int prayer,
                               EquipmentHelper.Slot slot,
                               List<SkillRequirement> requirements) {
        this.equipmentBonuses = new EquipmentBonuses(
                attackStab,
                attackSlash,
                attackCrush,
                attackMagic,
                attackRanged,
                defenceStab,
                defenceSlash,
                defenceCrush,
                defenceMagic,
                defenceRanged,
                meleeStrength,
                rangedStrength,
                magicDamage,
                prayer);
        this.slot = slot;
        this.requirements = requirements;
    }

    public EquipmentBonuses getEquipmentBonuses() {
        return equipmentBonuses;
    }

    public EquipmentHelper.Slot getSlot() {
        return slot;
    }

    public List<SkillRequirement> getRequirements() {
        return requirements;
    }
}
