package com.DarkAPI.common.OSRSBox;

import java.util.List;

public class WeaponDefinition {

    private final int attackSpeed;
    private final WeaponType type;
    private final List<WeaponStance> stances;

    public WeaponDefinition(int attackSpeed, WeaponType type, List<WeaponStance> stances) {
        this.attackSpeed = attackSpeed;
        this.type = type;
        this.stances = stances;
    }

    public enum WeaponType {
        AXES(Enemy.AttackType.MELEE),
        BLUNT_WEAPONS(Enemy.AttackType.MELEE),
        BULWARKS(Enemy.AttackType.MELEE),
        CLAWS(Enemy.AttackType.MELEE),
        POLEARMS(Enemy.AttackType.MELEE),
        PICKAXES(Enemy.AttackType.MELEE),
        SCYTHES(Enemy.AttackType.MELEE),
        SLASHING_SWORDS(Enemy.AttackType.MELEE),
        SPEARS(Enemy.AttackType.MELEE),
        SPIKED_WEAPONS(Enemy.AttackType.MELEE),
        STABBING_SWORDS(Enemy.AttackType.MELEE),
        TWO_HANDED_SWORDS(Enemy.AttackType.MELEE),
        WHIPS(Enemy.AttackType.MELEE),
        BOWS(Enemy.AttackType.RANGED),
        CHINCHOMPAS(Enemy.AttackType.RANGED),
        CROSSBOWS(Enemy.AttackType.RANGED),
        THROWN_WEAPONS(Enemy.AttackType.RANGED),
        STAVES(Enemy.AttackType.MAGIC),
        BLADED_STAVES(Enemy.AttackType.MAGIC),
        POWERED_STAVES(Enemy.AttackType.MAGIC),
        BANNERS,
        BLASTERS,
        GUNS,
        POLESTAVES(Enemy.AttackType.MELEE),
        SALAMANDERS(Enemy.AttackType.MELEE, Enemy.AttackType.RANGED, Enemy.AttackType.MAGIC),
        UNARMED(Enemy.AttackType.MELEE),
        UNKOWN;

        private final Enemy.AttackType[] attackType;
        WeaponType(Enemy.AttackType... attackType){
            this.attackType = attackType;
        }
        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
        public static WeaponType getEnum(String string){
            if(string == null){
                return null;
            }
            for (WeaponType val : values()) {
                if(val.toString().equals(string)){
                    return val;
                }
            }
            System.out.println(string + " was an unknown weapon type");
            return UNKOWN;
        }
    }

    public int getAttackSpeed() {
        return attackSpeed;
    }

    public WeaponType getType() {
        return type;
    }

    public List<WeaponStance> getStances() {
        return stances;
    }
}
