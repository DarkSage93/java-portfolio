package com.DarkAPI.common.OSRSBox;

import com.runemate.game.api.hybrid.util.Resources;

import javax.annotation.Nonnull;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
    public static BufferedImage loadImage(String resource){
        try {
            return Resources.getAsBufferedImage(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static <T> List<T> concatList(List<T> list1, List<T> list2){
        return Stream.concat(list1.stream(), list2.stream()).collect(Collectors.toList());
    }
    public static <T> List<T> concatList(List<T> list1, T... list2){
        return concatList(list1, Arrays.asList(list2));
    }
    public static boolean arrayMatchesString(String[] stringArray, @Nonnull String regex){
        return Arrays.stream(stringArray).filter(Objects::nonNull).anyMatch(string -> string.matches(regex));
    }
}
