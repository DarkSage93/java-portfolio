package com.DarkAPI.common.OSRSBox;

public class WeaponStance {

    private final String stanceName;
    private final AttackStyle attackStyle;
    private final AttackType attackType;

    public WeaponStance(String stanceName, AttackStyle attackStyle, AttackType attackType) {
        this.stanceName = stanceName;
        this.attackStyle = attackStyle;
        this.attackType = attackType;
    }

    public String getStanceName() {
        return stanceName;
    }

    public AttackStyle getAttackStyle() {
        return attackStyle;
    }

    public AttackType getAttackType() {
        return attackType;
    }

    public enum AttackType {
        CRUSH,
        SLASH,
        STAB,
        DEFENSIVE_CASTING,
        SPELLCASTING,
        UNKNOWN;

        @Override
        public String toString() {
            return super.toString().toLowerCase().replaceAll("_", " ");
        }
        public static AttackType getEnum(String string){
            if(string == null){
                return null;
            }
            for (AttackType val : values()) {
                if(val.toString().equals(string)){
                    return val;
                }
            }
            System.out.println("\"" + string + "\"" + " was an unknown attack type");
            return UNKNOWN;
        }
    }

    public enum AttackStyle {
        MELEE_ACCURATE(3,0,0,0,4,0,0,0,0,0),
        MELEE_AGGRESSIVE(0,3,0,0,0,4,0,0,0,0),
        MELEE_CONTROLLED(1,1,1,0,1.33,1.33,1.33,0,0,0),
        MELEE_DEFENSIVE(0,0,3,0,0,0,4,0,0,0),
        RANGED_ACCURATE(0,0,0,3,0,0,0,4,0,0),
        RANGED_RAPID(0,0,0,0,0,0,0,4,-1,0),
        RANGED_LONGRANGE(0,0,3,0,0,0,2,2,0,2),
        MAGIC,
        UNKNOWN;

        private int attackBonus;
        private int strengthBonus;
        private int defenceBonus;
        private int rangedBonus;
        private double attackExp;
        private double strengthExp;
        private double defenceExp;
        private double rangedExp;
        private int attackSpeed;
        private int rangeBonus;

        AttackStyle() {}

        AttackStyle(int attackBonus, int strengthBonus, int defenceBonus, int rangedBonus, double attackExp, double strengthExp, double defenceExp, double rangedExp, int attackSpeed, int rangeBonus) {
            this.attackBonus = attackBonus;
            this.strengthBonus = strengthBonus;
            this.defenceBonus = defenceBonus;
            this.rangedBonus = rangedBonus;
            this.attackExp = attackExp;
            this.strengthExp = strengthExp;
            this.defenceExp = defenceExp;
            this.rangedExp = rangedExp;
            this.attackSpeed = attackSpeed;
            this.rangeBonus = rangeBonus;
        }

        public int getAttackBonus() {
            return attackBonus;
        }
        public int getStrengthBonus() {
            return strengthBonus;
        }
        public int getDefenceBonus() {
            return defenceBonus;
        }
        public int getRangedBonus() {
            return rangedBonus;
        }
        public double getAttackExp() {
            return attackExp;
        }
        public double getStrengthExp() {
            return strengthExp;
        }
        public double getDefenceExp() {
            return defenceExp;
        }
        public double getRangedExp() {
            return rangedExp;
        }
        public int getAttackSpeed() {
            return attackSpeed;
        }
        public int getRangeBonus() {
            return rangeBonus;
        }

        @Override
        public String toString() {
            return super.toString().replaceAll("RANGED_","").replaceAll("MELEE_", "").toLowerCase();
        }
        public static AttackStyle getEnum(String string){
            if(string == null){
                return null;
            }
            for (AttackStyle val : values()) {
                if(val.toString().equals(string)){
                    return val;
                }
            }
            System.out.println("\"" + string + "\"" + " was an unknown attack style");
            return UNKNOWN;
        }
    }
}
