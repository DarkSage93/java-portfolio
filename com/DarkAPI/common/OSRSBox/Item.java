package com.DarkAPI.common.OSRSBox;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.framework.AbstractBot;

import javax.annotation.Nullable;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.*;

public class Item implements Cloneable, Serializable {
    
    private String name;
    private Integer itemId;
    private Integer notedId;
    private Boolean members;
    private Boolean tradeable;
    private Boolean tradeableGE;
    private Boolean stackable;
    private Boolean noteable;
    private Boolean equipable;
    private Integer cost;
    private Integer highAlch;
    private Integer lowAlch;
    private Integer weight;
    private Integer GeBuyLimit;
    private String icon;
    private Integer buyPrice;
    private final Set<Integer> linkedIds = new HashSet<>();
    private EquipmentDefinition equipmentDefinition;
    private WeaponDefinition weaponDefinition;
    private boolean isInitialized;

    private final List<Runnable> onUpdateRunnables = new ArrayList<>();


    public Item(){
    }

    public Item(String name){
        this.name = name;
        ItemFinder.getItem(this);
    }
    public Item(int id){
        this.itemId = id;
        ItemFinder.getItem(this);
    }
    @Override
    public Item clone(){
        try {
            return (Item) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int hashCode() {
        return itemId != null ? itemId : Objects.hashCode(name);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Item){
            return ((Item) obj).isSameAs(this);
        }
        return false;
    }

    public void setMembers(Boolean members) {
        this.members = members;
    }
    public void setTradeable(Boolean tradeable) {
        this.tradeable = tradeable;
    }
    public void setTradeableGE(Boolean tradeableGE) {
        this.tradeableGE = tradeableGE;
    }
    public void setStackable(Boolean stackable) {
        this.stackable = stackable;
    }
    public void setEquipable(Boolean equipable) {
        this.equipable = equipable;
    }
    public void setWeight(Integer weight) {
        this.weight = weight;
    }
    public void setGeBuyLimit(Integer geBuyLimit) {
        GeBuyLimit = geBuyLimit;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }
    public void setEquipmentDefinition(EquipmentDefinition equipmentDefinition) {
        this.equipmentDefinition = equipmentDefinition;
    }
    public void setWeaponDefinition(WeaponDefinition weaponDefinition) {
        this.weaponDefinition = weaponDefinition;
    }
    void setItemId(Integer itemId) {
        this.itemId = itemId;
    }
    void setLowAlch(Integer lowAlch) {
        synchronized (this) {
            this.lowAlch = lowAlch;
        }
    }
    void setHighAlch(Integer highAlch) {
        this.highAlch = highAlch;
    }
    void setCost(Integer cost) {
        this.cost = cost;
    }
    void setNotedId(Integer notedId) {
        this.notedId = notedId;
    }
    void setName(String name) {
        synchronized (this) {
            this.name = name;
        }
    }
    void setBuyPrice(Integer buyPrice) {
        this.buyPrice = buyPrice;
    }
    public void setNoteable(Boolean noteable) {
        this.noteable = noteable;
    }
    public void copy(Item other){
        if(other != null) {
            this.name = other.name;
            this.itemId = other.itemId;
            this.notedId = other.notedId;
            this.members = other.members;
            this.tradeable = other.tradeable;
            this.tradeableGE = other.tradeableGE;
            this.stackable = other.stackable;
            this.noteable = other.noteable;
            this.equipable = other.equipable;
            this.cost = other.cost;
            this.highAlch = other.highAlch;
            this.lowAlch = other.lowAlch;
            this.weight = other.weight;
            this.GeBuyLimit = other.GeBuyLimit;
            this.icon = other.icon;
            this.buyPrice = other.buyPrice;
            this.equipmentDefinition = other.equipmentDefinition;
            this.weaponDefinition = other.weaponDefinition;
        }
    }

    @Override
    public String toString() {
        String name = this.name;
        Integer id = getItemId();
        if(name != null) return name;
        if(id != null) return "item(id:" + id + ")";
        return super.toString();
    }

    public boolean equalsSpriteItem(SpriteItem item){
        if(item != null) {
            String thisName = this.getName();
            ItemDefinition itemDef = item.getDefinition();
            return thisName != null && itemDef != null && thisName.equals(itemDef.getName());
        }
        return false;
    }

    public boolean isSameAs(Item other){
        if(other != null){
            if(other.getItemId() != null)
                return Objects.equals(this.getItemId(), other.getItemId()) ||
                        this.getLinkedIds().stream().anyMatch(id -> Objects.equals(id, other.getItemId())) ||
                        other.getLinkedIds().stream().anyMatch(id -> Objects.equals(id, this.getItemId()));
        }
        return false;
    }

    public String getName(){
        return name;
    }


    public Integer getItemId() {
        return itemId;
    }

    public Integer getNotedId() {
        return notedId;
    }


    public Integer getBuyPrice() {
        return buyPrice;
    }

    public Boolean getMembers() {
        return members;
    }

    public Boolean getTradeable() {
        return tradeable;
    }

    public Boolean isTradeableGE() {
        return tradeableGE;
    }

    public Boolean getStackable() {
        return stackable;
    }

    public Boolean getNoteable() {
        return noteable;
    }

    public Boolean getEquipable() {
        return equipable;
    }

    public Integer getCost() {
        return cost;
    }

    public Integer getHighAlch() {
        return highAlch;
    }

    public Integer getLowAlch() {
        return lowAlch;
    }

    public Integer getWeight() {
        return weight;
    }

    public Integer getGeBuyLimit() {
        return GeBuyLimit;
    }

    public String getIcon() {
        return icon;
    }

    public Set<Integer> getLinkedIds(){
        return linkedIds;
    }

    public void addLinkedId(@Nullable Integer id){
        if(id != null) {
            this.linkedIds.add(id);
        }
    }

    public EquipmentDefinition getEquipmentDefinition() {
        return equipmentDefinition;
    }

    public WeaponDefinition getWeaponDefinition() {
        return weaponDefinition;
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    void triggerUpdate() {
        this.isInitialized = true;
        onUpdateRunnables.forEach(runnable -> {
            if(runnable != null) {
                try {
                    runnable.run();
                } catch (Exception e){
                    this.onUpdateRunnables.remove(runnable);
                    AbstractBot bot = Environment.getBot();
                    if(bot != null) {
                        bot.getLogger().warn("Runnable was removed from list on item " + this);
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        e.printStackTrace(pw);
                        bot.getLogger().fine(sw.toString());
                    } else {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void onUpdate(Runnable runnable){
        this.onUpdateRunnables.add(runnable);
    }
}
