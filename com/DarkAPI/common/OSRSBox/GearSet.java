package com.DarkAPI.common.OSRSBox;

import com.DarkAPI.common.net.OSRSBox.DamageSpells;
import com.DarkAPI.common.net.OSRSBox.EquipmentHelper;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GearSet implements Serializable {

    private Map<Equipment.Slot, Item> gear;
    private DamageSpells autocastSpell;
    private WeaponStance weaponStance;
    private String name;
    public final static String EQUIPPED_GEAR_SET_NAME = "Equipped";

    public GearSet(String name){
        this.name = name;
        gear = new HashMap<>();
        for (Equipment.Slot slot: Equipment.Slot.values()) {
            if (slot != Equipment.Slot.AURA && slot != Equipment.Slot.POCKET)
            gear.put(slot, null);
        }
    }

    public GearSet(GearSet other) {
        this.name = other.name;
        this.gear = new HashMap<>(other.gear);
        this.autocastSpell = other.autocastSpell;
        this.weaponStance = other.weaponStance;
    }

    public void copy(GearSet other){
        this.gear = new HashMap<>(other.gear);
        this.autocastSpell = other.autocastSpell;
        this.weaponStance = other.weaponStance;
    }

    public Item getItem(@Nullable Equipment.Slot slot){
        return gear.get(slot);
    }

    public void setItem(@Nullable EquipmentHelper.Slot slot, Item item){
        if(slot != null) {
            gear.replace(slot.getCorrespondingSlot(), item);
            if (slot == EquipmentHelper.Slot.TWO_HANDED) {
                gear.replace(Equipment.Slot.SHIELD, null);
            }
            if (slot == EquipmentHelper.Slot.SHIELD && weaponIsTwoHanded()) {
                gear.replace(Equipment.Slot.WEAPON, null);
            }
        }
    }
    public boolean weaponIsTwoHanded(){
        Item weapon = getItem(Equipment.Slot.WEAPON);
        EquipmentDefinition weaponEquipmentDefinition = weapon != null ? weapon.getEquipmentDefinition() : null;
        EquipmentHelper.Slot weaponSlot = weaponEquipmentDefinition != null ? weaponEquipmentDefinition.getSlot() : null;
        return weaponSlot == EquipmentHelper.Slot.TWO_HANDED;
    }
    public List<Item> getItems(){
        return new ArrayList<>(gear.values());
    }
    public static GearSet getEquippedGearSet(){
        GearSet set = new GearSet(EQUIPPED_GEAR_SET_NAME);
        for (Equipment.Slot slot : Equipment.Slot.values()) {
            SpriteItem spriteItem = Equipment.getItemIn(slot);
            if(spriteItem != null) {
                set.setItem(EquipmentHelper.Slot.fromCorrespondingSlot(slot), new Item(spriteItem.getId()));
            }
        }
        return set;
    }

    public boolean isEquipped(Item item){
        EquipmentDefinition def = item.getEquipmentDefinition();
        EquipmentHelper.Slot slot = def != null ? def.getSlot():null;
        if(slot == null) return false;
        return item.equals(getItem(slot.getCorrespondingSlot()));
    }

    public EquipmentBonuses getEquipmentBonuses(){
        EquipmentBonuses equipmentBonuses = new EquipmentBonuses();
        gear.values().forEach(item -> {
            if( item != null){
                EquipmentDefinition equipmentDefinition = item.getEquipmentDefinition();
                EquipmentBonuses itemBonuses = equipmentDefinition != null ? equipmentDefinition.getEquipmentBonuses() : null;
                if(itemBonuses != null) {
                    equipmentBonuses.addWith(itemBonuses);
                }
            }
        });
        return equipmentBonuses;
    }

    public DamageSpells getAutocastSpell() {
        return autocastSpell;
    }

    public void setAutocastSpell(@Nullable DamageSpells autocastSpell) {
        this.autocastSpell = autocastSpell;
    }

    public WeaponStance getWeaponStance() {
        return weaponStance;
    }

    public void setWeaponStance(@Nullable WeaponStance weaponStance) {
        this.weaponStance = weaponStance;
    }

    @Override
    public String toString() {
        return name;
    }

    public void rename(String name) {
        this.name = name;
    }
}
