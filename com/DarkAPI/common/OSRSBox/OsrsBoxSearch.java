package com.DarkAPI.common.OSRSBox;

import com.DarkAPI.common.net.OSRSBox.EquipmentHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.regal.utility.awesome_navigation.requirements.SkillRequirement;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.Skill;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class OsrsBoxSearch {

    private static final String BASE_URL = "https://www.osrsbox.com/osrsbox-db/";
    private static final String ITEM_FOLDER = "items-json/";
    private static final String ITEM_ICONS_FOLDER = "item_icons/";

    @Nullable
    public static ItemProperties searchItem(int id){
        Environment.getLogger().debug(String.format("Fetching item %d from OSRSBox", id));
        String json = HTTP.httpGET(BASE_URL + ITEM_FOLDER + id + ".json");
        Gson gson = new GsonBuilder().registerTypeAdapter(ItemProperties.class, ItemProperties.getAdapter()).create();
        try {
            return gson.fromJson(json, ItemProperties.class);
        } catch (JsonSyntaxException e){
            Environment.getLogger().debug(String.format("Gson parsing of item %d from OSRSBox failed", id));
            e.printStackTrace();
            return null;
        }
    }

    private static List<SkillRequirement> getRequirements(JSONObject obj){
        List<SkillRequirement> requirements = new ArrayList<>();
        JSONObject requirementsJSON = getAsJSONObject(obj, ItemAttributes.REQUIREMENTS);
        if(requirementsJSON != null){
            for (Skill skill : Skill.values()) {
                Integer level = getAsInteger(requirementsJSON, skill.toString().toLowerCase());
                if(level != null){
                    requirements.add(new SkillRequirement(skill, level));
                }
            }
        }
        return requirements;
    }

    private enum Slot {
        AMMUNITION(EquipmentHelper.Slot.AMMUNITION),
        AURA(EquipmentHelper.Slot.AURA),
        BODY(EquipmentHelper.Slot.BODY),
        CAPE(EquipmentHelper.Slot.CAPE),
        FEET(EquipmentHelper.Slot.FEET),
        HANDS(EquipmentHelper.Slot.HANDS),
        HEAD(EquipmentHelper.Slot.HEAD),
        LEGS(EquipmentHelper.Slot.LEGS),
        NECK(EquipmentHelper.Slot.NECK),
        POCKET(EquipmentHelper.Slot.POCKET),
        RING(EquipmentHelper.Slot.RING),
        SHIELD(EquipmentHelper.Slot.SHIELD),
        WEAPON(EquipmentHelper.Slot.WEAPON),
        TWOH(EquipmentHelper.Slot.TWO_HANDED);


        private final EquipmentHelper.Slot slot;

        Slot(EquipmentHelper.Slot slot){
            this.slot = slot;
        }
        @Override
        public String toString() {
            return super.toString().toLowerCase().replaceAll("two", "2");
        }
        public EquipmentHelper.Slot getSlot() {
            return slot;
        }
        public static EquipmentHelper.Slot getSlot(String slotString){
            for (Slot slot : values()) {
                if(slot.toString().equals(slotString)){
                    return slot.getSlot();
                }
            }
            Environment.getLogger().warn("Equipment slot \"" + slotString + "\" is unknown");
            return null;
        }
    }
    private static EquipmentDefinition getEquipmentDefinition(JSONObject obj) throws JSONException{
        JSONObject equipmentJSON = getAsJSONObject(obj, ItemAttributes.EQUIPMENT);
        if(equipmentJSON != null) {
            return new EquipmentDefinition(
                    getAsInt(equipmentJSON, ItemAttributes.ATTACK_STAB),
                    getAsInt(equipmentJSON, ItemAttributes.ATTACK_SLASH),
                    getAsInt(equipmentJSON, ItemAttributes.ATTACK_CRUSH),
                    getAsInt(equipmentJSON, ItemAttributes.ATTACK_MAGIC),
                    getAsInt(equipmentJSON, ItemAttributes.ATTACK_RANGED),
                    getAsInt(equipmentJSON, ItemAttributes.DEFENCE_STAB),
                    getAsInt(equipmentJSON, ItemAttributes.DEFENCE_SLASH),
                    getAsInt(equipmentJSON, ItemAttributes.DEFENCE_CRUSH),
                    getAsInt(equipmentJSON, ItemAttributes.DEFENCE_MAGIC),
                    getAsInt(equipmentJSON, ItemAttributes.DEFENCE_RANGED),
                    getAsInt(equipmentJSON, ItemAttributes.MELEE_STRENGTH),
                    getAsInt(equipmentJSON, ItemAttributes.RANGED_STRENGTH),
                    getAsInt(equipmentJSON, ItemAttributes.MAGIC_DAMAGE),
                    getAsInt(equipmentJSON, ItemAttributes.PRAYER),
                    getEquipmentSlot(equipmentJSON),
                    getRequirements(equipmentJSON));
        }
        return null;
    }
    private static EquipmentHelper.Slot getEquipmentSlot(JSONObject obj){
        String slotString = getAsString(obj, ItemAttributes.SLOT);
        if(slotString != null){
            return Slot.getSlot(slotString);
        }
        return null;
    }
    private static List<Enemy.AttackType> getEnemyAttackTypes(JSONObject obj){
        List<Enemy.AttackType> attackTypes = new ArrayList<>();
        JSONArray attackTypesJSON = getAsJSONArray(obj, EnemyAttributes.ATTACK_TYPE);
        if(attackTypesJSON != null){
            for (int i = 0; i < attackTypesJSON.length(); i++) {
                String typeString = attackTypesJSON.getString(i);
                attackTypes.add(Enemy.AttackType.getEnum(typeString));
            }
        }
        return attackTypes;
    }
    private static List<ItemDrop> getItemDrops(JSONObject obj){
        List<ItemDrop> itemDrops = new ArrayList<>();
        JSONArray itemDropsJSON = getAsJSONArray(obj, EnemyAttributes.DROPS);
        if(itemDropsJSON != null){
            for (int i = 0; i < itemDropsJSON.length(); i++) {
                if(itemDropsJSON.get(i) instanceof JSONObject) {
                    JSONObject itemDropJSON = itemDropsJSON.getJSONObject(i);
                    itemDrops.add(new ItemDrop(
                            getAsString(itemDropJSON, ItemDropAttributes.ID),
                            getAsInteger(itemDropJSON, ItemDropAttributes.QUANTITY),
                            getAsFloat(itemDropJSON, ItemDropAttributes.RARITY),
                            getAsBoolean(itemDropJSON, ItemDropAttributes.NOTED)));
                }
            }
        }
        return itemDrops;
    }

    private static int getAsInt(JSONObject obj, Attributes attribute) throws JSONException {
        return getAsInt(obj, attribute.toString());
    }
    private static Integer getAsInteger(JSONObject obj, Attributes attribute) {
        return getAsInteger(obj, attribute.toString());
    }
    private static Float getAsFloat(JSONObject obj, Attributes attribute){
        return getAsFloat(obj, attribute.toString());
    }
    private static String getAsString(JSONObject obj, Attributes attribute) {
        return getAsString(obj, attribute.toString());
    }
    private static Boolean getAsBoolean(JSONObject obj, Attributes attribute) {
        return getAsBoolean(obj, attribute.toString());
    }
    private static JSONObject getAsJSONObject(JSONObject obj, Attributes attribute) {
        return getAsJSONObject(obj, attribute.toString());
    }
    private static JSONArray getAsJSONArray(JSONObject obj, Attributes attribute){
        return getAsJSONArray(obj, attribute.toString());
    }
    private static List<String> getAsStringList(JSONObject obj, Attributes attribute){
        return getAsStringList(obj, attribute.toString());
    }
    private static JSONObject osrsBoxSearch(String name, String type){
        return HTTP.httpGETtoJSON(BASE_URL + type + name + ".json");
    }
    private static JSONObject osrsBoxEnemySearch(String name){
        return osrsBoxSearch(name, "monsters");
    }
    private static JSONObject osrsBoxItemSearch(String name){
        return osrsBoxSearch(name, "items");
    }
    private static JSONObject osrsBoxGetItem(int id){
        return HTTP.httpGETtoJSON(BASE_URL + ITEM_FOLDER + id +".json");
    }

    private static JSONObject osrsBoxGetMonster(int id){
        return HTTP.httpGETtoJSON(BASE_URL + "monsters/" + id);
    }

    private static int getAsInt(JSONObject obj, String attribute) throws JSONException {
        return obj.getInt(attribute);
    }
    private static Integer getAsInteger(JSONObject obj, String attribute){
        if(obj.has(attribute) && !obj.isNull(attribute)){
            return obj.getInt(attribute);
        }
        return null;
    }
    private static Float getAsFloat(JSONObject obj, String attribute){
        if(obj.has(attribute) && !obj.isNull(attribute)){
            return obj.getFloat(attribute);
        }
        return null;
    }
    private static String getAsString(JSONObject obj, String attribute){
        if(obj.has(attribute) && obj.get(attribute) instanceof String){
            return obj.getString(attribute);
        }
        return null;
    }
    private static Boolean getAsBoolean(JSONObject obj, String attribute){
        if(obj.has(attribute) && obj.get(attribute) instanceof Boolean){
            return obj.getBoolean(attribute);
        }
        return null;
    }
    private static JSONObject getAsJSONObject(JSONObject obj, String attribute){
        if(obj.has(attribute) && obj.get(attribute) instanceof JSONObject) {
            return obj.getJSONObject(attribute);
        }
        return null;
    }
    private static JSONArray getAsJSONArray(JSONObject obj, String attribute){
        if(obj.has(attribute) && obj.get(attribute) instanceof JSONArray) {
            return obj.getJSONArray(attribute);
        }
        return null;
    }
    private static List<String> getAsStringList(JSONObject obj, String attribute){
        List<String> list = new ArrayList<>();
        JSONArray listJSON = getAsJSONArray(obj, attribute);
        if(listJSON != null){
            for (int i = 0; i < listJSON.length(); i++) {
                list.add(listJSON.getString(i));
            }
        }
        return list;
    }

    interface Attributes{}

    private enum ItemAttributes implements Attributes {
        ID,
        NAME,
        INCOMPLETE,
        MEMBERS,
        TRADEABLE,
        TRADEABLE_ON_GE,
        STACKABLE,
        STACKED,
        NOTED,
        NOTEABLE,
        LINKED_ID_ITEM,
        LINKED_ID_NOTED,
        LINKED_ID_PLACEHOLDER,
        PLACEHOLDER,
        EQUIPABLE,
        EQUIPABLE_BY_PLAYER,
        EQUIPABLE_WEAPON,
        COST,
        LOWALCH,
        HIGHALCH,
        WEIGHT,
        BUY_LIMIT,
        QUEST_ITEM,
        RELEASE_DATE,
        DUPLICATE,
        EXAMINE,
        ICON,
        WIKI_NAME,
        WIKI_URL,
        WIKI_EXCHNAGE,

        EQUIPMENT,
        ATTACK_STAB,
        ATTACK_SLASH,
        ATTACK_CRUSH,
        ATTACK_MAGIC,
        ATTACK_RANGED,
        DEFENCE_STAB,
        DEFENCE_SLASH,
        DEFENCE_CRUSH,
        DEFENCE_MAGIC,
        DEFENCE_RANGED,
        MELEE_STRENGTH,
        RANGED_STRENGTH,
        MAGIC_DAMAGE,
        PRAYER,
        SLOT,
        REQUIREMENTS,
        WEAPON,
        ATTACK_SPEED,
        WEAPON_TYPE,
        STANCES,
        COMBAT_STYLE,
        ATTACK_TYPE,
        ATTACK_STYLE;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }
    private enum EnemyAttributes implements Attributes {
        ID,
        NAME,
        INCOMPLETE,
        MEMBERS,
        RELEASE_DATE,
        COMBAT_LEVEL,
        SIZE,
        HITPOINTS,
        MAX_HIT,
        ATTACK_TYPE,
        ATTACK_SPEED,
        AGGRESSIVE,
        POISONOUS,
        IMMUNE_POISON,
        IMMUNE_VENOM,
        ATTRIBUTES,
        CATEGORY,
        SLAYER_MONSTER,
        SLAYER_LEVEL,
        SLAYER_XP,
        SLAYER_MASTERS,
        DUPLICATE,
        EXAMINE,
        ICON,
        WIKI_NAME,
        WIKI_URL,
        ATTACK_LEVEL,
        STRENGTH_LEVEL,
        DEFENCE_LEVEL,
        MAGIC_LEVEL,
        RANGED_LEVEL,
        ATTACK_STAB,
        ATTACK_SLASH,
        ATTACK_CRUSH,
        ATTACK_MAGIC,
        ATTACK_RANGED,
        DEFENCE_STAB,
        DEFENCE_SLASH,
        DEFENCE_CRUSH,
        DEFENCE_MAGIC,
        DEFENCE_RANGED,
        ATTACK_ACCURACY,
        MELEE_STRENGTH,
        RANGED_STRENGTH,
        MAGIC_DAMAGE,
        DROPS;
        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    private enum ItemDropAttributes implements Attributes {
        ID,
        NAME,
        MEMBERS,
        QUANTITY,
        NOTED,
        RARITY,
        DROP_REQUIREMENTS;
        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    static void loadItem(Item item){
        if(item == null){
            return;
        }
        JSONObject itemJSON = null;
        if(item.getItemId() != null){
            itemJSON = osrsBoxGetItem(item.getItemId());
        } else if(item.getName() != null) {
            itemJSON = osrsBoxItemSearch(item.getName());
        }
        if(itemJSON != null){
            Boolean isNoted = getAsBoolean(itemJSON, ItemAttributes.NOTED);
            Boolean isPlaceHolder = getAsBoolean(itemJSON, ItemAttributes.PLACEHOLDER);
            isNoted = isNoted != null && isNoted;
            isPlaceHolder = isPlaceHolder != null && isPlaceHolder;

            item.setName(getAsString(itemJSON, ItemAttributes.NAME));
            item.setMembers(getAsBoolean(itemJSON, ItemAttributes.MEMBERS));
            item.setTradeable(getAsBoolean(itemJSON, ItemAttributes.TRADEABLE));
            item.setTradeableGE(getAsBoolean(itemJSON, ItemAttributes.TRADEABLE_ON_GE));
            item.setStackable(getAsBoolean(itemJSON, ItemAttributes.STACKABLE));
            item.setNoteable(getAsBoolean(itemJSON, ItemAttributes.NOTEABLE));
            item.setItemId(getAsInteger(itemJSON, !isNoted && !isPlaceHolder ? ItemAttributes.ID : ItemAttributes.LINKED_ID_ITEM));
            item.setNotedId(getAsInteger(itemJSON, isNoted ? ItemAttributes.ID : ItemAttributes.LINKED_ID_NOTED));
            item.setEquipable(getAsBoolean(itemJSON, ItemAttributes.EQUIPABLE_BY_PLAYER));
            item.setCost(getAsInteger(itemJSON, ItemAttributes.COST));
            item.setLowAlch(getAsInteger(itemJSON, ItemAttributes.LOWALCH));
            item.setHighAlch(getAsInteger(itemJSON, ItemAttributes.HIGHALCH));
            item.setWeight(getAsInteger(itemJSON, ItemAttributes.WEIGHT));
            item.setGeBuyLimit(getAsInteger(itemJSON, ItemAttributes.BUY_LIMIT));
            item.setIcon(getAsString(itemJSON, ItemAttributes.ICON));

        }
    }
    public static void loadEnemy(Enemy enemy){
        if(enemy == null){
            return;
        }
        JSONObject enemyJSON = null;
        if(enemy.getId() != null){
            enemyJSON = osrsBoxGetMonster(enemy.getId());
        } else if(enemy.getName() != null) {
            enemyJSON = osrsBoxEnemySearch(enemy.getName());
        }
        if(enemyJSON != null){
            enemy.setId(getAsInteger(enemyJSON, EnemyAttributes.ID));
            enemy.setName(getAsString(enemyJSON, EnemyAttributes.NAME));
            enemy.setMembers(getAsBoolean(enemyJSON, EnemyAttributes.MEMBERS));
            enemy.setCombatLevel(getAsInteger(enemyJSON, EnemyAttributes.COMBAT_LEVEL));
            enemy.setHitpoints(getAsInteger(enemyJSON, EnemyAttributes.HITPOINTS));
            enemy.setMaxHit(getAsInteger(enemyJSON, EnemyAttributes.MAX_HIT));
            enemy.setAttackTypes(getEnemyAttackTypes(enemyJSON));
            enemy.setAttackSpeed(getAsInteger(enemyJSON, EnemyAttributes.ATTACK_SPEED));
            enemy.setAggressive(getAsBoolean(enemyJSON, EnemyAttributes.AGGRESSIVE));
            enemy.setPoisonous(getAsBoolean(enemyJSON, EnemyAttributes.POISONOUS));
            enemy.setPoisonImmune(getAsBoolean(enemyJSON, EnemyAttributes.IMMUNE_POISON));
            enemy.setVenomImmune(getAsBoolean(enemyJSON, EnemyAttributes.IMMUNE_VENOM));
            enemy.setAttributes(getAsStringList(enemyJSON, EnemyAttributes.ATTRIBUTES));
            enemy.setCategories(getAsStringList(enemyJSON, EnemyAttributes.CATEGORY));
            enemy.setSlayerMonster(getAsBoolean(enemyJSON, EnemyAttributes.SLAYER_MONSTER));
            enemy.setSlayerLevel(getAsInteger(enemyJSON, EnemyAttributes.SLAYER_LEVEL));
            enemy.setSlayerXp(getAsInteger(enemyJSON, EnemyAttributes.SLAYER_XP));
            enemy.setSlayerMasters(getAsStringList(enemyJSON, EnemyAttributes.SLAYER_MASTERS));
            enemy.setHitpoints(getAsInteger(enemyJSON, EnemyAttributes.HITPOINTS));
            enemy.setAttackLevel(getAsInteger(enemyJSON, EnemyAttributes.ATTACK_LEVEL));
            enemy.setStrengthLevel(getAsInteger(enemyJSON, EnemyAttributes.STRENGTH_LEVEL));
            enemy.setDefenceLevel(getAsInteger(enemyJSON, EnemyAttributes.DEFENCE_LEVEL));
            enemy.setMagicLevel(getAsInteger(enemyJSON, EnemyAttributes.MAGIC_LEVEL));
            enemy.setRangedLevel(getAsInteger(enemyJSON, EnemyAttributes.RANGED_LEVEL));
            enemy.setAttackStab(getAsInteger(enemyJSON, EnemyAttributes.ATTACK_STAB));
            enemy.setAttackSlash(getAsInteger(enemyJSON, EnemyAttributes.ATTACK_SLASH));
            enemy.setAttackCrush(getAsInteger(enemyJSON, EnemyAttributes.ATTACK_CRUSH));
            enemy.setAttackMagic(getAsInteger(enemyJSON, EnemyAttributes.ATTACK_MAGIC));
            enemy.setAttackRanged(getAsInteger(enemyJSON, EnemyAttributes.ATTACK_RANGED));
            enemy.setDefenceStab(getAsInteger(enemyJSON, EnemyAttributes.DEFENCE_STAB));
            enemy.setDefenceSlash(getAsInteger(enemyJSON, EnemyAttributes.DEFENCE_SLASH));
            enemy.setDefenceCrush(getAsInteger(enemyJSON, EnemyAttributes.DEFENCE_CRUSH));
            enemy.setDefenceMagic(getAsInteger(enemyJSON, EnemyAttributes.DEFENCE_MAGIC));
            enemy.setDefenceRanged(getAsInteger(enemyJSON, EnemyAttributes.DEFENCE_RANGED));
            enemy.setAttackAccuracy(getAsInteger(enemyJSON, EnemyAttributes.ATTACK_ACCURACY));
            enemy.setMeleeStrength(getAsInteger(enemyJSON, EnemyAttributes.MELEE_STRENGTH));
            enemy.setRangedStrength(getAsInteger(enemyJSON, EnemyAttributes.RANGED_STRENGTH));
            enemy.setMagicDamage(getAsInteger(enemyJSON, EnemyAttributes.MAGIC_DAMAGE));
            enemy.setDrops(getItemDrops(enemyJSON));
        }
    }
}
