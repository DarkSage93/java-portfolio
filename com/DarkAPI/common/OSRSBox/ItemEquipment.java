package com.DarkAPI.common.OSRSBox;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

public class ItemEquipment {

    //The attack stab bonus of the item.
    public int attack_stab;

    //The attack slash bonus of the item.
    public int attack_slash;

    //The attack crush bonus of the item.
    public int attack_crush;

    //The attack magic bonus of the item.
    public int attack_magic;

    //The attack ranged bonus of the item.
    public int attack_ranged;

    //The defence stab bonus of the item.
    public int defence_stab;

    //The defence slash bonus of the item.
    public int defence_slash;

    //The defence crush bonus of the item.
    public int defence_crush;

    //The defence magic bonus of the item.
    public int defence_magic;

    //The defence ranged bonus of the item.
    public int defence_ranged;

    //The melee strength bonus of the item.
    public int melee_strength;

    //The ranged strength bonus of the item.
    public int ranged_strength;

    //The magic damage bonus of the item.
    public int magic_damage;

    //The prayer bonus of the item.
    public int prayer;

    @Nonnull
    //The equipment slot associated with the item (e.g., head).
    public String slot;

    @Nullable
    //An object of requirements {skill: level}.
    public Map<String, Integer> requirements;

}
