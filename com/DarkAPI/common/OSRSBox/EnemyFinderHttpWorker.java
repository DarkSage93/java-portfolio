package com.DarkAPI.common.OSRSBox;


import static com.DarkAPI.common.OSRSBox.OsrsBoxSearch.loadEnemy;

public class EnemyFinderHttpWorker implements Runnable {

    private final Enemy enemy;

    EnemyFinderHttpWorker(Enemy enemy) {
        this.enemy = enemy;
    }

    @Override
    public void run() {
        loadEnemy(enemy);
    }

    public Enemy getEnemy() {
        return enemy;
    }
}
