package com.DarkAPI.common.OSRSBox;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class EnemyFinder {

    private static List<Enemy> enemyCache;
    private static ThreadPoolExecutor executor;

    private static ThreadPoolExecutor getExecutor(){
        if(executor == null) {
            executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        }
        return executor;
    }
    private static List<Enemy> getCache() {
        if (enemyCache == null) {
            enemyCache = new ArrayList<>();
        }
        return enemyCache;
    }
    private static Enemy getFromCache(int id){
        return getCache().stream().filter(enemy -> enemy.getId() == id).findFirst().orElse(null);
    }
    private static Enemy getFromCache(String name){
        return getCache().stream().filter(enemy -> name.equals(enemy.getName())).findFirst().orElse(null);
    }
    static void getEnemy(Enemy enemy){
        Enemy cacheEnemy = null;
        if(enemy.getId() != null){
            cacheEnemy = getFromCache(enemy.getId());
        } else if(enemy.getName() != null){
            cacheEnemy = getFromCache(enemy.getName());
        }
        if(cacheEnemy != null) {
            enemy.copy(cacheEnemy);
        } else {
            EnemyFinderHttpWorker worker = new EnemyFinderHttpWorker(enemy);
            getExecutor().execute(worker);
            //getCache().add(enemy);
        }
    }
}
