package com.DarkAPI.common.OSRSBox;

import org.json.JSONObject;

public class RSBuddy {

    private static JSONObject cache;

    static void loadItemPrices(Item item){
        if(item.getItemId() != null) {
            if (cache == null) {
                loadAllItems();
            }
            if (cache != null) {
                if (cache.has(item.getItemId().toString())) {
                    JSONObject itemJSON = cache.getJSONObject(item.getItemId().toString());
                    if (itemJSON != null) {
                        if (itemJSON.has("buy_average")) {
                            item.setBuyPrice(itemJSON.getInt("buy_average"));
                        }
                    }
                }
            }
        }
    }
    private static void loadAllItems(){
        cache = HTTP.httpGETtoJSON("https://rsbuddy.com/exchange/summary.json");
    }

}
