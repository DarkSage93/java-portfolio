package com.DarkAPI.common.OSRSBox;

import java.util.List;

public class Enemy {

    private Integer id;
    private String name;
    private Boolean members;
    private Integer combatLevel;
    private Integer maxHit;
    private List<AttackType> attackTypes;
    private Integer attackSpeed;
    private Boolean aggressive;
    private Boolean poisonous;
    private Boolean poisonImmune;
    private Boolean venomImmune;
    private List<String> attributes;
    private List<String> categories;
    private Boolean slayerMonster;
    private Integer slayerLevel;
    private Integer slayerXp;
    private List<String> slayerMasters;
    private Integer hitpoints;
    private Integer attackLevel;
    private Integer strengthLevel;
    private Integer defenceLevel;
    private Integer magicLevel;
    private Integer rangedLevel;
    private Integer attackStab;
    private Integer attackSlash;
    private Integer attackCrush;
    private Integer attackMagic;
    private Integer attackRanged;
    private Integer defenceStab;
    private Integer defenceSlash;
    private Integer defenceCrush;
    private Integer defenceMagic;
    private Integer defenceRanged;
    private Integer attackAccuracy; //TODO:What is that?
    private Integer meleeStrength;
    private Integer rangedStrength;
    private Integer magicDamage;
    private List<ItemDrop> drops;

    public Enemy(String name){
        this.name = name;
        EnemyFinder.getEnemy(this);
    }
    public Enemy(int id){
        this.id = id;
        EnemyFinder.getEnemy(this);
    }

    public void copy(Enemy other) {
        if(other != null) {
            this.id = other.id;
            this.name = other.name;
            this.members = other.members;
            this.combatLevel = other.combatLevel;
            this.maxHit = other.maxHit;
            this.attackTypes = other.attackTypes;
            this.attackSpeed = other.attackSpeed;
            this.aggressive = other.aggressive;
            this.poisonous = other.poisonous;
            this.poisonImmune = other.poisonImmune;
            this.venomImmune = other.venomImmune;
            this.attributes = other.attributes;
            this.categories = other.categories;
            this.slayerMonster = other.slayerMonster;
            this.slayerLevel = other.slayerLevel;
            this.slayerXp = other.slayerXp;
            this.slayerMasters = other.slayerMasters;
            this.hitpoints = other.hitpoints;
            this.attackLevel = other.attackLevel;
            this.strengthLevel = other.strengthLevel;
            this.defenceLevel = other.defenceLevel;
            this.magicLevel = other.magicLevel;
            this.rangedLevel = other.rangedLevel;
            this.attackStab = other.attackStab;
            this.attackSlash = other.attackSlash;
            this.attackCrush = other.attackCrush;
            this.attackMagic = other.attackMagic;
            this.attackRanged = other.attackRanged;
            this.defenceStab = other.defenceStab;
            this.defenceSlash = other.defenceSlash;
            this.defenceCrush = other.defenceCrush;
            this.defenceMagic = other.defenceMagic;
            this.defenceRanged = other.defenceRanged;
            this.attackAccuracy = other.attackAccuracy;
            this.meleeStrength = other.meleeStrength;
            this.rangedStrength = other.rangedStrength;
            this.magicDamage = other.magicDamage;
            this.drops = other.drops;
        }
    }

    public enum AttackType{
        MELEE,
        MAGIC,
        RANGED;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
        public static AttackType getEnum(String string){
            for (AttackType val : values()) {
                if(val.toString().equals(string)){
                    return val;
                }
            }
            System.out.println(string + " was an unknown attack type");
            return null;
        }
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Boolean getMembers() {
        return members;
    }

    public Integer getCombatLevel() {
        return combatLevel;
    }

    public Integer getMaxHit() {
        return maxHit;
    }

    public List<AttackType> getAttackTypes() {
        return attackTypes;
    }

    public Integer getAttackSpeed() {
        return attackSpeed;
    }

    public Boolean getAggressive() {
        return aggressive;
    }

    public Boolean getPoisonous() {
        return poisonous;
    }

    public Boolean getPoisonImmune() {
        return poisonImmune;
    }

    public Boolean getVenomImmune() {
        return venomImmune;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public List<String> getCategories() {
        return categories;
    }

    public Boolean getSlayerMonster() {
        return slayerMonster;
    }

    public Integer getSlayerLevel() {
        return slayerLevel;
    }

    public Integer getSlayerXp() {
        return slayerXp;
    }

    public List<String> getSlayerMasters() {
        return slayerMasters;
    }


    public Integer getHitpoints() {
        return hitpoints;
    }

    public Integer getAttackLevel() {
        return attackLevel;
    }

    public Integer getStrengthLevel() {
        return strengthLevel;
    }

    public Integer getDefenceLevel() {
        return defenceLevel;
    }

    public Integer getMagicLevel() {
        return magicLevel;
    }

    public Integer getRangedLevel() {
        return rangedLevel;
    }

    public Integer getAttackStab() {
        return attackStab;
    }

    public Integer getAttackSlash() {
        return attackSlash;
    }

    public Integer getAttackCrush() {
        return attackCrush;
    }

    public Integer getAttackMagic() {
        return attackMagic;
    }

    public Integer getAttackRanged() {
        return attackRanged;
    }

    public Integer getDefenceStab() {
        return defenceStab;
    }

    public Integer getDefenceSlash() {
        return defenceSlash;
    }

    public Integer getDefenceCrush() {
        return defenceCrush;
    }

    public Integer getDefenceMagic() {
        return defenceMagic;
    }

    public Integer getDefenceRanged() {
        return defenceRanged;
    }

    public Integer getAttackAccuracy() {
        return attackAccuracy;
    }

    public Integer getMeleeStrength() {
        return meleeStrength;
    }

    public Integer getRangedStrength() {
        return rangedStrength;
    }

    public Integer getMagicDamage() {
        return magicDamage;
    }

    public List<ItemDrop> getDrops() {
        return drops;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMembers(Boolean members) {
        this.members = members;
    }

    public void setCombatLevel(Integer combatLevel) {
        this.combatLevel = combatLevel;
    }

    public void setMaxHit(Integer maxHit) {
        this.maxHit = maxHit;
    }

    public void setAttackTypes(List<AttackType> attackTypes) {
        this.attackTypes = attackTypes;
    }

    public void setAttackSpeed(Integer attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public void setAggressive(Boolean aggressive) {
        this.aggressive = aggressive;
    }

    public void setPoisonous(Boolean poisonous) {
        this.poisonous = poisonous;
    }

    public void setPoisonImmune(Boolean poisonImmune) {
        this.poisonImmune = poisonImmune;
    }

    public void setVenomImmune(Boolean venomImmune) {
        this.venomImmune = venomImmune;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void setSlayerMonster(Boolean slayerMonster) {
        this.slayerMonster = slayerMonster;
    }

    public void setSlayerLevel(Integer slayerLevel) {
        this.slayerLevel = slayerLevel;
    }

    public void setSlayerXp(Integer slayerXp) {
        this.slayerXp = slayerXp;
    }

    public void setSlayerMasters(List<String> slayerMasters) {
        this.slayerMasters = slayerMasters;
    }

    public void setHitpoints(Integer hitpoints) {
        this.hitpoints = hitpoints;
    }

    public void setAttackLevel(Integer attackLevel) {
        this.attackLevel = attackLevel;
    }

    public void setStrengthLevel(Integer strengthLevel) {
        this.strengthLevel = strengthLevel;
    }

    public void setDefenceLevel(Integer defenceLevel) {
        this.defenceLevel = defenceLevel;
    }

    public void setMagicLevel(Integer magicLevel) {
        this.magicLevel = magicLevel;
    }

    public void setRangedLevel(Integer rangedLevel) {
        this.rangedLevel = rangedLevel;
    }

    public void setAttackStab(Integer attackStab) {
        this.attackStab = attackStab;
    }

    public void setAttackSlash(Integer attackSlash) {
        this.attackSlash = attackSlash;
    }

    public void setAttackCrush(Integer attackCrush) {
        this.attackCrush = attackCrush;
    }

    public void setAttackMagic(Integer attackMagic) {
        this.attackMagic = attackMagic;
    }

    public void setAttackRanged(Integer attackRanged) {
        this.attackRanged = attackRanged;
    }

    public void setDefenceStab(Integer defenceStab) {
        this.defenceStab = defenceStab;
    }

    public void setDefenceSlash(Integer defenceSlash) {
        this.defenceSlash = defenceSlash;
    }

    public void setDefenceCrush(Integer defenceCrush) {
        this.defenceCrush = defenceCrush;
    }

    public void setDefenceMagic(Integer defenceMagic) {
        this.defenceMagic = defenceMagic;
    }

    public void setDefenceRanged(Integer defenceRanged) {
        this.defenceRanged = defenceRanged;
    }

    public void setAttackAccuracy(Integer attackAccuracy) {
        this.attackAccuracy = attackAccuracy;
    }

    public void setMeleeStrength(Integer meleeStrength) {
        this.meleeStrength = meleeStrength;
    }

    public void setRangedStrength(Integer rangedStrength) {
        this.rangedStrength = rangedStrength;
    }

    public void setMagicDamage(Integer magicDamage) {
        this.magicDamage = magicDamage;
    }

    public void setDrops(List<ItemDrop> drops) {
        this.drops = drops;
    }

    @Override
    public String toString() {
        return getName();
    }
}
