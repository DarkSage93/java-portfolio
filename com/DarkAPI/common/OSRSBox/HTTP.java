package com.DarkAPI.common.OSRSBox;

import com.runemate.game.api.hybrid.Environment;
import org.json.JSONObject;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HTTP {

    @Nullable
    public static JSONObject httpGETtoJSON(String stringUrl){
        try {
            URL url = new URL(stringUrl);

            String content = httpGET(url);
            if(content != null) {
                return new JSONObject(content);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    @Nullable
    public static String httpGET(String url){
        try {
            return httpGET(new URL(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static String httpGET(URL url){
        try {
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36");
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            return content.toString();
        } catch (Exception e) {
            Environment.getLogger().debug(String.format("HttpGET(%s) failed : %s", url, e.getMessage()));
            e.printStackTrace();
            return null;
        }
    }
}
