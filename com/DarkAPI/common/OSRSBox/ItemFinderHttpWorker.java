package com.DarkAPI.common.OSRSBox;

import static com.DarkAPI.common.OSRSBox.OsrsBoxSearch.loadItem;

public class ItemFinderHttpWorker implements Runnable {

    private final Item item;

    ItemFinderHttpWorker(Item item){
        this.item = item;
    }
    @Override
    public void run() {
        loadItem(item);
        if(item.isTradeableGE() == null || item.isTradeableGE()) {
            RSBuddy.loadItemPrices(item);
        }
        item.triggerUpdate();
    }
    public Item getItem() {
        return item;
    }
}
