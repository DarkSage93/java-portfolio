package com.DarkAPI.common.OSRSBox;

public class EquipmentBonuses {

    private int attackStab;
    private int attackSlash;
    private int attackCrush;
    private int attackMagic;
    private int attackRanged;
    private int defenceStab;
    private int defenceSlash;
    private int defenceCrush;
    private int defenceMagic;
    private int defenceRanged;
    private int meleeStrength;
    private int rangedStrength;
    private int magicDamage;
    private int prayer;


    public EquipmentBonuses(){}
    public EquipmentBonuses(int attackStab,
                            int attackSlash,
                            int attackCrush,
                            int attackMagic,
                            int attackRanged,
                            int defenceStab,
                            int defenceSlash,
                            int defenceCrush,
                            int defenceMagic,
                            int defenceRanged,
                            int meleeStrength,
                            int rangedStrength,
                            int magicDamage,
                            int prayer) {
        this.attackStab = attackStab;
        this.attackSlash = attackSlash;
        this.attackCrush = attackCrush;
        this.attackMagic = attackMagic;
        this.attackRanged = attackRanged;
        this.defenceStab = defenceStab;
        this.defenceSlash = defenceSlash;
        this.defenceCrush = defenceCrush;
        this.defenceMagic = defenceMagic;
        this.defenceRanged = defenceRanged;
        this.meleeStrength = meleeStrength;
        this.rangedStrength = rangedStrength;
        this.magicDamage = magicDamage;
        this.prayer = prayer;
    }
    public void addWith(EquipmentBonuses other){
        if(other != null) {
            this.attackStab = other.getAttackStab();
            this.attackSlash = other.getAttackSlash();
            this.attackCrush = other.getAttackCrush();
            this.attackMagic = other.getAttackMagic();
            this.attackRanged = other.getAttackRanged();
            this.defenceStab = other.getDefenceStab();
            this.defenceSlash = other.getDefenceSlash();
            this.defenceCrush = other.getDefenceCrush();
            this.defenceMagic = other.getDefenceMagic();
            this.defenceRanged = other.getDefenceRanged();
            this.meleeStrength = other.getMeleeStrength();
            this.rangedStrength = other.getRangedStrength();
            this.magicDamage = other.getMagicDamage();
            this.prayer = other.getPrayer();
        }
    }


    public enum Types {
        ATTACK_STAB,
        ATTACK_SLASH,
        ATTACK_CRUSH,
        ATTACK_MAGIC,
        ATTACK_RANGED,
        DEFENCE_STAB,
        DEFENCE_SLASH,
        DEFENCE_CRUSH,
        DEFENCE_MAGIC,
        DEFENCE_RANGED,
        MELEE_STRENGTH,
        RANGED_STRENGTH,
        MAGIC_DAMAGE,
        PRAYER;

        public int get(EquipmentBonuses equipmentBonuses){
            switch (this){
                case ATTACK_STAB:return equipmentBonuses.getAttackStab();
                case ATTACK_SLASH:return equipmentBonuses.getAttackSlash();
                case ATTACK_CRUSH:return equipmentBonuses.getAttackCrush();
                case ATTACK_MAGIC:return equipmentBonuses.getAttackMagic();
                case ATTACK_RANGED:return equipmentBonuses.getAttackRanged();
                case DEFENCE_STAB:return equipmentBonuses.getDefenceStab();
                case DEFENCE_SLASH:return equipmentBonuses.getDefenceSlash();
                case DEFENCE_CRUSH:return equipmentBonuses.getDefenceCrush();
                case DEFENCE_MAGIC:return equipmentBonuses.getDefenceMagic();
                case DEFENCE_RANGED:return equipmentBonuses.getDefenceRanged();
                case MELEE_STRENGTH:return equipmentBonuses.getMeleeStrength();
                case RANGED_STRENGTH:return equipmentBonuses.getRangedStrength();
                case MAGIC_DAMAGE:return equipmentBonuses.getMagicDamage();
                case PRAYER:return equipmentBonuses.getPrayer();
            }
            throw new IllegalArgumentException("Non-static enum function was accessed through invalid enum member");
        }
    }

    public int getAttackStab() {
        return attackStab;
    }

    public int getAttackSlash() {
        return attackSlash;
    }

    public int getAttackCrush() {
        return attackCrush;
    }

    public int getAttackMagic() {
        return attackMagic;
    }

    public int getAttackRanged() {
        return attackRanged;
    }

    public int getDefenceStab() {
        return defenceStab;
    }

    public int getDefenceSlash() {
        return defenceSlash;
    }

    public int getDefenceCrush() {
        return defenceCrush;
    }

    public int getDefenceMagic() {
        return defenceMagic;
    }

    public int getDefenceRanged() {
        return defenceRanged;
    }

    public int getMeleeStrength() {
        return meleeStrength;
    }

    public int getRangedStrength() {
        return rangedStrength;
    }

    public int getMagicDamage() {
        return magicDamage;
    }

    public int getPrayer() {
        return prayer;
    }
}
