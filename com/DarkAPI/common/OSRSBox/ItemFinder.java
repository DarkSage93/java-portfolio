package com.DarkAPI.common.OSRSBox;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ItemFinder {

    private static List<Item> itemCache;
    private static ThreadPoolExecutor executor;

    private static ThreadPoolExecutor getExecutor(){
        if(executor == null) {
            executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        }
        return executor;
    }
    private static List<Item> getCache() {
        if (itemCache == null) {
            itemCache = new ArrayList<>();
        }
        return itemCache;
    }
    private static Item getFromCache(int id){
        return getCache().stream().filter(Objects::nonNull).filter(item -> (item.getItemId() != null && item.getItemId() == id) || (item.getNotedId() != null && item.getNotedId() == id)).findFirst().orElse(null);
    }
    private static Item getFromCache(String name){
        if(name == null) return null;
        return getCache().stream().filter(item -> name.equals(item.getName())).findFirst().orElse(null);
    }
    static void getItem(Item item){
        Item cacheItem = null;
        if(item.getItemId() != null){
            cacheItem = getFromCache(item.getItemId());
        } else if(item.getName() != null){
            cacheItem = getFromCache(item.getName());
        }
        if(cacheItem != null) {
            item.copy(cacheItem);
            item.triggerUpdate();
        } else {
            getCache().add(item);
            ItemFinderHttpWorker worker = new ItemFinderHttpWorker(item);
            getExecutor().execute(worker);
        }
    }
    public static void clearCache(){
        getCache().clear();
    }
}
