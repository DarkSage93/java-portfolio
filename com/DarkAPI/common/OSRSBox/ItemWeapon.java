package com.DarkAPI.common.OSRSBox;

import javax.annotation.Nonnull;

public class ItemWeapon {

    //The attack speed of a weapon (in game ticks).
    public int attack_speed;

    @Nonnull
    //The weapon classification (e.g., axes)
    public String weapon_type;

    @Nonnull
    //An array of weapon stance information.
    public String[] stances;
}
