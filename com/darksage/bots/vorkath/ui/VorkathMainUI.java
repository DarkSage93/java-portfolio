package com.darksage.bots.vorkath.ui;

import com.DarkAPI.common.net.OSRSBox.EquipmentHelper;
import com.DarkAPI.common.net.OSRSBox.GearSet;
import com.DarkAPI.common.net.OSRSBox.ItemProperties;
import com.DarkAPI.common.net.OSRSBox.OsrsBoxSearch;
import com.DarkAPI.data.enums.Food;
import com.darksage.bots.vorkath.Vorkath;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.framework.listeners.EquipmentListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class VorkathMainUI extends VBox implements EquipmentListener {

    private final Vorkath bot;
    private StatusPane statusPane;
    private SettingsTab settingsTab;
    private KillTrackerTab killTrackerTab;
    private InventoryTab inventoryTab;
    private EquipmentTab equipmentTab;
    public VorkUIData uiData = new VorkUIData();

    private final AnchorPane anchorPane;

    public StatusPane getStatusPane() {
        if (statusPane == null)
            statusPane = new StatusPane(bot, this);
        return statusPane;
    }

    public SettingsTab getSettingsTab() {
        if (settingsTab == null)
            settingsTab = new SettingsTab(bot, this);
        return settingsTab;
    }

    public KillTrackerTab getKillTrackerTab() {
        if (killTrackerTab == null)
            killTrackerTab = new KillTrackerTab(bot, this);
        return killTrackerTab;
    }

    public InventoryTab getInventoryTab() {
        if (inventoryTab == null)
            inventoryTab = new InventoryTab(bot, this);
        return inventoryTab;
    }

    public EquipmentTab getEquipmentTab() {
        if (equipmentTab == null)
            equipmentTab = new EquipmentTab(bot, this);
        return equipmentTab;
    }


    private void initControllers() {
        getStatusPane();
        getEquipmentTab();
        loadEquipment();
        getInventoryTab();
        getSettingsTab();
        getKillTrackerTab();
    }

    public VorkathMainUI(Vorkath bot) {
        this.bot = bot;

        setSpacing(5);
        setMinSize(810, 600);
        setMaxSize(810, 600);

        initControllers();

        anchorPane = new AnchorPane();
        buildAnchorPane();
        this.setAlignment(Pos.CENTER);
        this.getChildren().add(anchorPane);
    }

    private void buildAnchorPane() {
        anchorPane.setPrefSize(800, 600);
        VBox boxStatus = new VBox();
        boxStatus.setAlignment(Pos.CENTER);
        boxStatus.setPrefSize(190, 590);
        boxStatus.setLayoutX(5);
        boxStatus.setLayoutY(5);
        boxStatus.getChildren().add(statusPane);
        TabPane tabPane = new TabPane();
        tabPane.setPrefSize(600, 590);
        tabPane.setSide(Side.TOP);
        tabPane.getTabs().addAll(settingsTab, equipmentTab, inventoryTab, killTrackerTab);
        tabPane.setLayoutY(5);
        tabPane.setLayoutX(200);
        anchorPane.getChildren().addAll(boxStatus, tabPane);
    }

    public EventHandler<ActionEvent> onStartPressed() {
        return (ActionEvent event) -> {
            bot.getPlatform().invokeLater(()->{
                bot.vars.isStart = true;
            });
        };
    }

    public EventHandler<ActionEvent> onLoadInventory() {
        return (ActionEvent event) -> {
            bot.getPlatform().invokeLater(()->{

            });
        };
    }

    public EventHandler<ActionEvent> onSaveInventory() {
        return (ActionEvent event) -> {
            bot.getPlatform().invokeLater(()->{

            });
        };
    }

    public EventHandler<ActionEvent> onMainGearSave() {
        return (ActionEvent e) -> {
            bot.getPlatform().invokeLater(()-> {
                for (SpriteItem item : Equipment.getItems()) {
                    ItemProperties props = OsrsBoxSearch.searchItem(item.getId());
                    if (props != null && props.equipment != null) {
                        bot.getLogger().debug(props.name);
                        if (bot.vars.mainGear == null)
                            bot.vars.mainGear = new GearSet("Main gear");
                        bot.vars.mainGear.setItem(EquipmentHelper.Slot.fromCorrespondingSlot(props.equipment.slot), props);
                    }

                }
            });
            getEquipmentTab().setToggleGreen();
        };
    }

    public EventHandler<ActionEvent> onSpecGearSave() {
        return (ActionEvent e) -> {
            bot.getPlatform().invokeLater(()-> {
                for (SpriteItem item : Equipment.getItems()) {
                    ItemProperties props = OsrsBoxSearch.searchItem(item.getId());
                    if (props != null && props.equipment != null) {
                        bot.getLogger().debug(props.name);
                        if (bot.vars.specGear == null)
                            bot.vars.specGear = new GearSet("Spec gear");
                        bot.vars.specGear.setItem(EquipmentHelper.Slot.fromCorrespondingSlot(props.equipment.slot), props);
                    }

                }
            });
            getEquipmentTab().setToggleGreen();
        };
    }

    public Food foodChoice;
    public EventHandler<ActionEvent> onFoodChosen() {
        return (ActionEvent event) -> {
            String choice = getSettingsTab().comboFood.getSelectionModel().getSelectedItem();
            if (choice != null)
                foodChoice = Food.getValues(choice);
            getSettingsTab().setRestoreValues();
        };
    }

    private void loadEquipment(){
        for (SpriteItem item : Equipment.getItems()) {
            getEquipmentTab().getEquipmentVisualizer().setEquipmentId(Equipment.Slot.resolve(item.getIndex()), item.getId());
        }
    }

    @Override
    public void onItemEquipped(ItemEvent event) {
        getEquipmentTab().getEquipmentVisualizer().setEquipmentId(Equipment.Slot.resolve(event.getItem().getIndex()), event.getItem().getId());
        getEquipmentTab().updateToggle();
    }

    @Override
    public void onItemUnequipped(ItemEvent event) {
        getEquipmentTab().getEquipmentVisualizer().setEquipmentId(Equipment.Slot.resolve(event.getItem().getIndex()), null);
        getEquipmentTab().updateToggle();
    }

}
