package com.darksage.bots.vorkath.ui;

import com.DarkAPI.common.util.ItemIcons;
import com.DarkAPI.data.enums.Food;
import com.darksage.bots.vorkath.Vorkath;
import com.runemate.game.api.script.framework.core.LoopingThread;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.awt.image.BufferedImage;

public class SettingsTab extends Tab {

    private final VorkathMainUI mainUI;
    private final Vorkath bot;
    private final AnchorPane anchorPane = new AnchorPane();

    public HBox hBoxCombat, hBoxAmmo, hBoxOptional, hBoxFood, hBoxSupplies;
    public ImageView imageViewCrossbow, imageViewBlowpipe, imageViewMelee;
    public RadioButton radioCrossbow, radioBlowpipe, radioMelee;
    public CheckBox checkAmmo1, checkAmmo2, checkAmmo3, checkAmmo4;
    public ImageView imageAmmo1, imageAmmo2, imageAmmo3, imageAmmo4;
    public ImageView imageRunePouch, imageSerpHelm, imageSlayerStaff;
    public CheckBox checkRunePouch, checkSerpHelm, checkSlayerStaff, checkFood;
    public ComboBox<String> comboFood;
    public ImageView imageSupply1, imageSupply2, imageSupply3, imageSupply4, imageSupply5, imageSupply6, imageSupply7, imageSupply8, imageSupply9, imageSupply10;
    public Spinner<Integer> spinner1, spinner2, spinner3, spinner4, spinner5, spinner6, spinner7, spinner8, spinner9, spinner10;

    public SettingsTab(Vorkath bot, VorkathMainUI mainUI) {
        this.bot = bot;
        this.mainUI = mainUI;

        setClosable(false);
        anchorPane.setPrefSize(600, 590);
        setText("General Settings");
        buildSettings();

        radioCrossbow.setOnAction((ActionEvent) -> {
            radioBlowpipe.setSelected(false);
            radioMelee.setSelected(false);
            hBoxAmmo.setVisible(true);
            buildAmmoCBow();
        });
        radioBlowpipe.setOnAction((ActionEvent) -> {
            radioCrossbow.setSelected(false);
            radioMelee.setSelected(false);
            hBoxAmmo.setVisible(true);
            buildAmmoBP();
        });
        radioMelee.setOnAction((ActionEvent) ->{
            radioCrossbow.setSelected(false);
            radioBlowpipe.setSelected(false);
            hBoxAmmo.setVisible(false);
        });
        checkFood.setOnAction((ActionEvent) -> {
            setComboFoodVisible(checkFood.isSelected());
            setRestoreValues();
        });

        comboFood.setOnAction(mainUI.onFoodChosen());

        loadImages();
        this.setContent(anchorPane);

        bot.getPlatform().invokeLater(()-> new LoopingThread(()-> Platform.runLater(this::imageUpdater), 1500).start());
    }


    private void imageUpdater() {
        loadImage(imageViewCrossbow, mainUI.uiData.crossbows[mainUI.uiData.rotationLoc]);
        loadImage(imageViewMelee, mainUI.uiData.meleeWeps[mainUI.uiData.rotationLoc]);
        incrementCycle();
    }

    private void loadImages() {
        loadImage(imageRunePouch, mainUI.uiData.runePouchID);
        addTooltip(imageRunePouch, "Rune pouch");
        loadImage(imageSerpHelm, mainUI.uiData.serpHelmID);
        addTooltip(imageSerpHelm, "Serpentine helm");
        loadImage(imageSlayerStaff, mainUI.uiData.slayerStaffID);
        addTooltip(imageSlayerStaff, "Slayer's staff");
        setRestoreValues();
        loadImage(imageSupply6, mainUI.uiData.anglerID);
        addTooltip(imageSupply6, "Anglerfish");
        loadImage(imageSupply5, mainUI.uiData.antivenomID);
        addTooltip(imageSupply5, "Anti-venom+");
        loadImage(imageSupply2, mainUI.uiData.superRestoreID);
        addTooltip(imageSupply2, "Super restores");
        loadImage(imageSupply7, mainUI.uiData.prayerPotID);
        addTooltip(imageSupply7, "Prayer pots");
        loadImage(imageSupply3, mainUI.uiData.divineRangeID);
        addTooltip(imageSupply3, "Divine ranging pots");
        loadImage(imageSupply8, mainUI.uiData.rangingPotID);
        addTooltip(imageSupply8, "Ranging pots");
        loadImage(imageSupply4, mainUI.uiData.divineCombatID);
        addTooltip(imageSupply4, "Divine super combats");
        loadImage(imageSupply9, mainUI.uiData.combatPotID);
        addTooltip(imageSupply9, "Super combats");
        loadImage(imageViewBlowpipe, mainUI.uiData.blowpipeID);
    }

    private void setRestoreSupply() {
        loadImage(imageSupply1, mainUI.uiData.hpRestoreID);
        addTooltip(imageSupply1, mainUI.uiData.hpRestore);
    }

    public void setRestoreValues() {
        if (checkFood.isSelected() && mainUI.foodChoice != null) {
            mainUI.uiData.hpRestore = mainUI.foodChoice.getFoodName();
            mainUI.uiData.hpRestoreID = mainUI.foodChoice.getFoodID();
        } else {
            mainUI.uiData.hpRestore = mainUI.uiData.saraBrew;
            mainUI.uiData.hpRestoreID = mainUI.uiData.saraBrewID;
        }
        setRestoreSupply();
    }

    private void addTooltip(ImageView view, String text) {
        Tooltip tooltip = new Tooltip();
        tooltip.setText(text);
        Tooltip.install(view, tooltip);
    }

    private void loadImage(ImageView view, int id) {
        BufferedImage image = ItemIcons.get(id);
        if (image != null)
            view.setImage(SwingFXUtils.toFXImage(image, null));
    }

    private void incrementCycle() {
        if (mainUI.uiData.rotationLoc < 3) {
            mainUI.uiData.rotationLoc++;
        } else {
            mainUI.uiData.rotationLoc = 0;
        }
    }

    private void buildSettings() {
        buildCombat();
        buildAmmoBase();
        buildOptionals();
        buildFood();
        buildSupplies();
    }

    private void buildSupplies() {
        hBoxSupplies = new HBox();
        hBoxSupplies.setAlignment(Pos.CENTER);
        hBoxSupplies.setPrefSize(580, 180);
        hBoxSupplies.setLayoutX(10);
        hBoxSupplies.setLayoutY(350);
        hBoxSupplies.setBackground(new Background(new BackgroundFill(Color.gray(.2, .2), new CornerRadii(15), new Insets(2))));
        hBoxSupplies.setBorder(new Border(new BorderStroke(Color.WHITESMOKE, BorderStrokeStyle.SOLID, new CornerRadii(15), BorderStroke.DEFAULT_WIDTHS)));
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPrefSize(475, 160);
        gridPane.setVgap(4);
        gridPane.setHgap(4);
        initSupplyNodes();
        gridPane.add(buildGridNode(imageSupply1, spinner1), 0, 0);
        gridPane.add(buildGridNode(imageSupply2, spinner2), 1, 0);
        gridPane.add(buildGridNode(imageSupply3, spinner3), 2, 0);
        gridPane.add(buildGridNode(imageSupply4, spinner4), 3, 0);
        gridPane.add(buildGridNode(imageSupply5, spinner5), 4, 0);
        gridPane.add(buildGridNode(imageSupply6, spinner6), 0, 1);
        gridPane.add(buildGridNode(imageSupply7, spinner7), 1, 1);
        gridPane.add(buildGridNode(imageSupply8, spinner8), 2, 1);
        gridPane.add(buildGridNode(imageSupply9, spinner9), 3, 1);
        hBoxSupplies.getChildren().add(gridPane);
        anchorPane.getChildren().add(hBoxSupplies);
    }

    private VBox buildGridNode(ImageView view, Spinner<Integer> spinner) {
        VBox vBox = new VBox();
        vBox.setPrefSize(90, 80);
        vBox.setAlignment(Pos.CENTER);
        vBox.setBackground(new Background(new BackgroundFill(Color.gray(.4, .2), new CornerRadii(15), new Insets(0))));
        HBox boxView = new HBox();
        boxView.setPrefHeight(55);
        boxView.setAlignment(Pos.CENTER);
        boxView.getChildren().add(view);
        HBox boxSpinner = new HBox();
        boxSpinner.setPrefHeight(25);
        boxSpinner.setAlignment(Pos.CENTER);
        boxSpinner.getChildren().add(spinner);
        vBox.getChildren().addAll(boxView, boxSpinner);
        return vBox;
    }

    private void initSupplyNodes() {
        imageSupply1 = initImageView();
        imageSupply2 = initImageView();
        imageSupply3 = initImageView();
        imageSupply4 = initImageView();
        imageSupply5 = initImageView();
        imageSupply6 = initImageView();
        imageSupply7 = initImageView();
        imageSupply8 = initImageView();
        imageSupply9 = initImageView();
        spinner1 = initSpinner(28, 0);
        spinner2 = initSpinner(20, 0);
        spinner3 = initSpinner(4, 1);
        spinner4 = initSpinner(4, 0);
        spinner5 = initSpinner(2, 0);
        spinner6 = initSpinner(5, 3);
        spinner7 = initSpinner(20, 3);
        spinner8 = initSpinner(4, 0);
        spinner9 = initSpinner(4, 0);
    }

    private Spinner<Integer> initSpinner(int max, int initVal) {
        Spinner<Integer> spinner = new Spinner<>(0, max, initVal);
        spinner.setEditable(false);
        spinner.setPrefWidth(60);
        return spinner;
    }

    private void setComboFoodVisible(boolean set) {
        comboFood.setVisible(set);
    }

    private void buildFood() {
        hBoxFood = new HBox();
        hBoxFood.setAlignment(Pos.CENTER);
        hBoxFood.setPrefSize(580, 40);
        hBoxFood.setLayoutX(10);
        hBoxFood.setLayoutY(300);
        hBoxFood.setBackground(new Background(new BackgroundFill(Color.gray(.2, .2), new CornerRadii(15), new Insets(2))));
        hBoxFood.setBorder(new Border(new BorderStroke(Color.WHITESMOKE, BorderStrokeStyle.SOLID, new CornerRadii(15), BorderStroke.DEFAULT_WIDTHS)));
        checkFood = new CheckBox("Use Food");
        Tooltip tooltip = new Tooltip();
        tooltip.setText("Use food or Sara Brews");
        Tooltip.install(checkFood, tooltip);
        checkFood.setPrefWidth(100);
        comboFood = new ComboBox<>();
        comboFood.getItems().addAll(FXCollections.observableArrayList(Food.getChoices()));
        comboFood.setPromptText("Food");
        comboFood.setPrefWidth(100);
        comboFood.setVisible(false);
        Separator separator = new Separator();
        separator.setPrefWidth(10);
        separator.setOrientation(Orientation.VERTICAL);
        separator.setVisible(false);
        hBoxFood.getChildren().addAll(checkFood, separator, comboFood);
        anchorPane.getChildren().add(hBoxFood);
    }

    private void buildOptionals() {
        hBoxOptional = new HBox();
        hBoxOptional.setAlignment(Pos.CENTER);
        hBoxOptional.setPrefSize(580, 90);
        hBoxOptional.setLayoutX(10);
        hBoxOptional.setLayoutY(200);
        hBoxOptional.setBackground(new Background(new BackgroundFill(Color.gray(.2, .2), new CornerRadii(15), new Insets(2))));
        hBoxOptional.setBorder(new Border(new BorderStroke(Color.WHITESMOKE, BorderStrokeStyle.SOLID, new CornerRadii(15), BorderStroke.DEFAULT_WIDTHS)));
        GridPane gridPane = new GridPane();
        gridPane.setHgap(4);
        gridPane.setVgap(4);
        gridPane.setPrefWidth(320);
        gridPane.setPrefHeight(85);
        gridPane.setAlignment(Pos.CENTER);
        initOptionalNodes();
        gridPane.add(createGridNode(imageRunePouch, checkRunePouch), 0, 0);
        gridPane.add(createGridNode(imageSerpHelm, checkSerpHelm), 1, 0);
        gridPane.add(createGridNode(imageSlayerStaff, checkSlayerStaff), 2, 0);
        hBoxOptional.getChildren().add(gridPane);
        anchorPane.getChildren().add(hBoxOptional);
    }

    private void buildAmmoCBow() {
        loadImage(imageAmmo1, mainUI.uiData.rubyBoltsID);
        loadImage(imageAmmo2, mainUI.uiData.rubyDragonID);
        loadImage(imageAmmo3, mainUI.uiData.diamondBoltsID);
        loadImage(imageAmmo4, mainUI.uiData.diamondDragonID);
        addTooltip(imageAmmo1, "Ruby bolts (e)");
        addTooltip(imageAmmo2, "Ruby dragon bolts (e)");
        addTooltip(imageAmmo3, "Diamond bolts (e)");
        addTooltip(imageAmmo4, "Diamond dragon bolts (e)");
        //allow 2 selections
    }

    private void buildAmmoBP() {
        loadImage(imageAmmo1, mainUI.uiData.mithDartID);
        loadImage(imageAmmo2, mainUI.uiData.addyDartID);
        loadImage(imageAmmo3, mainUI.uiData.runeDartID);
        loadImage(imageAmmo4, mainUI.uiData.dragDartID);
        addTooltip(imageAmmo1, "Mithril darts");
        addTooltip(imageAmmo2, "Adamant darts");
        addTooltip(imageAmmo3, "Rune darts");
        addTooltip(imageAmmo4, "Dragon darts");
        //allow 1 selection
    }

    private void buildAmmoBase() {
        hBoxAmmo = new HBox();
        hBoxAmmo.setPrefSize(580, 90);
        hBoxAmmo.setLayoutY(100);
        hBoxAmmo.setLayoutX(10);
        hBoxAmmo.setAlignment(Pos.CENTER);
        hBoxAmmo.setBackground(new Background(new BackgroundFill(Color.gray(.2, .2), new CornerRadii(15), new Insets(2))));
        hBoxAmmo.setBorder(new Border(new BorderStroke(Color.WHITESMOKE, BorderStrokeStyle.SOLID, new CornerRadii(15), BorderStroke.DEFAULT_WIDTHS)));
        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(4);
        gridPane.setPrefWidth(420);
        gridPane.setPrefHeight(80);
        gridPane.setAlignment(Pos.CENTER);
        initAmmoNodes();
        gridPane.add(createGridNode(imageAmmo1, checkAmmo1), 0, 0);
        gridPane.add(createGridNode(imageAmmo2, checkAmmo2), 1, 0);
        gridPane.add(createGridNode(imageAmmo3, checkAmmo3), 2, 0);
        gridPane.add(createGridNode(imageAmmo4, checkAmmo4), 3, 0);
        hBoxAmmo.getChildren().add(gridPane);
        hBoxAmmo.setVisible(true);
        buildAmmoCBow();
        anchorPane.getChildren().add(hBoxAmmo);
    }

    private VBox createGridNode(ImageView view, CheckBox check) {
        VBox vBox = new VBox();
        vBox.setPrefSize(100, 80);
        vBox.setAlignment(Pos.CENTER);
        vBox.setBackground(new Background(new BackgroundFill(Color.gray(.4, .2), new CornerRadii(15), new Insets(0))));
        HBox boxView = new HBox();
        boxView.setPrefHeight(55);
        boxView.setAlignment(Pos.CENTER);
        boxView.getChildren().add(view);
        HBox boxCheck = new HBox();
        boxCheck.setPrefHeight(25);
        boxCheck.setAlignment(Pos.CENTER);
        boxCheck.getChildren().add(check);
        vBox.getChildren().addAll(boxView, boxCheck);
        return vBox;
    }

    private void initOptionalNodes() {
        imageRunePouch = initImageView();
        imageSerpHelm = initImageView();
        imageSlayerStaff = initImageView();
        checkRunePouch = new CheckBox("Rune Pouch");
        checkRunePouch.setPrefWidth(80);
        checkSerpHelm = new CheckBox("Serp Helm");
        checkSerpHelm.setPrefWidth(80);
        checkSlayerStaff = new CheckBox("Slayer Staff");
        checkSlayerStaff.setPrefWidth(80);
    }

    private void initAmmoNodes() {
        imageAmmo1 = initImageView();
        imageAmmo2 = initImageView();
        imageAmmo3 = initImageView();
        imageAmmo4 = initImageView();
        checkAmmo1 = new CheckBox("Use");
        checkAmmo1.setPrefWidth(80);
        checkAmmo2 = new CheckBox("Use");
        checkAmmo2.setPrefWidth(80);
        checkAmmo3 = new CheckBox("Use");
        checkAmmo3.setPrefWidth(80);
        checkAmmo4 = new CheckBox("Use");
        checkAmmo4.setPrefWidth(80);
    }

    private ImageView initImageView() {
        ImageView view = new ImageView();
        view.setFitWidth(50);
        view.setFitHeight(50);
        return view;
    }

    private void buildCombat() {
        VBox vBox = new VBox();
        vBox.setPrefSize(580, 90);
        vBox.setLayoutX(10);
        vBox.setLayoutY(5);
        vBox.setAlignment(Pos.CENTER);
        vBox.setBackground(new Background(new BackgroundFill(Color.gray(.2, .2), new CornerRadii(15), new Insets(2))));
        vBox.setBorder(new Border(new BorderStroke(Color.WHITESMOKE, BorderStrokeStyle.SOLID, new CornerRadii(15), BorderStroke.DEFAULT_WIDTHS)));
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        GridPane gridPane = new GridPane();
        gridPane.setPrefSize(330, 85);
        gridPane.setHgap(4);
        gridPane.setVgap(4);
        initializeCombatNodes();
        gridPane.add(createRadioGridNode(imageViewCrossbow, radioCrossbow), 0, 0);
        gridPane.add(createRadioGridNode(imageViewBlowpipe, radioBlowpipe), 1, 0);
        gridPane.add(createRadioGridNode(imageViewMelee, radioMelee), 2, 0);
        hBox.getChildren().add(gridPane);
        vBox.getChildren().add(hBox);
        anchorPane.getChildren().add(vBox);
    }

    private VBox createRadioGridNode(ImageView view, RadioButton radio) {
        VBox vBox = new VBox();
        vBox.setPrefSize(100, 85);
        vBox.setAlignment(Pos.CENTER);
        vBox.setBackground(new Background(new BackgroundFill(Color.gray(.4, .2), new CornerRadii(15), new Insets(0))));
        HBox boxView = new HBox();
        boxView.setPrefSize(100, 55);
        boxView.setAlignment(Pos.CENTER);
        boxView.getChildren().add(view);
        HBox boxRadio = new HBox();
        boxRadio.setPrefSize(100, 25);
        boxRadio.setAlignment(Pos.CENTER);
        boxRadio.getChildren().add(radio);
        vBox.getChildren().addAll(boxView, boxRadio);
        return vBox;
    }

    private void initializeCombatNodes() {
        imageViewCrossbow = initImageView();
        imageViewBlowpipe = initImageView();
        imageViewMelee = initImageView();
        radioCrossbow = new RadioButton("Crossbow");
        radioCrossbow.setSelected(true);
        radioBlowpipe = new RadioButton("Blowpipe");
        radioMelee = new RadioButton("Melee");
    }
}
