package com.darksage.bots.vorkath.ui;

import com.darksage.bots.vorkath.Vorkath;
import javafx.scene.control.Tab;

public class KillTrackerTab extends Tab {

    private final VorkathMainUI mainUI;
    private final Vorkath bot;

    public KillTrackerTab(Vorkath bot, VorkathMainUI mainUI) {
        this.bot = bot;
        this.mainUI = mainUI;

    }
}
