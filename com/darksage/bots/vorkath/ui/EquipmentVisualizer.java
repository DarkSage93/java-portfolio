package com.darksage.bots.vorkath.ui;

import com.DarkAPI.common.net.OSRSBox.ItemProperties;
import com.DarkAPI.common.net.OSRSBox.OsrsBoxSearch;
import com.DarkAPI.common.util.IconLoader;
import com.DarkAPI.common.util.ImageUtils;
import com.DarkAPI.common.util.ItemIcons;
import com.DarkAPI.common.util.ResourcePaths;
import com.darksage.bots.vorkath.Vorkath;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

public class EquipmentVisualizer extends GridPane {

    private Vorkath bot;
    private final double scale;
    private final ObservableMap<Equipment.Slot, Integer> equipmentIds = FXCollections.observableHashMap();
    private final ObjectProperty<Equipment.Slot> selectedSlot = new SimpleObjectProperty<>();


    public EquipmentVisualizer(Vorkath bot, double scale) {
        this.bot = bot;
        this.scale = scale;

        setVgap(4);
        setHgap(4);

        equipmentIds.addListener((InvalidationListener) observable -> update());
        selectedSlot.addListener(observable -> update());

        setFocusTraversable(true);

        setOnMouseClicked(this::onMouseClicked);

        update();
    }

    private void update(){
        this.getChildren().clear();
        for (Map.Entry<Equipment.Slot, EquipmentSlotUIDefinition> entry : EQUIPMENT_SLOT_TO_GRIDPANE.entrySet()) {
            Equipment.Slot slot = entry.getKey();
            EquipmentSlotUIDefinition slotDefinition = entry.getValue();
            Integer itemId = equipmentIds.get(slot);
            String background = slotDefinition.imageName;
            BufferedImage itemIcon = null;
            if(itemId != null){
                background = ResourcePaths.IMG_OCCUPIED_SLOT;
                itemIcon = ItemIcons.get(itemId);
            }
            StackPane slotPane = drawSlot(background, itemIcon, selectedSlot.get() == slot);
            if (itemId != null)
                addTooltip(slotPane, itemId);
            this.add(slotPane, slotDefinition.col, slotDefinition.row);
        }
    }

    private void addTooltip(StackPane pane, int itemID) {
        Tooltip tooltip = new Tooltip();
        ItemProperties props = OsrsBoxSearch.searchItem(itemID);
        if (props != null) {
            tooltip.setText(props.name);
        } else {
            tooltip.setText("Empty Slot");
        }
        Tooltip.install(pane, tooltip);
    }

    SpriteItem item;
    public ItemProperties getWeapon() {
        bot.getPlatform().invokeLater(()->{
            item = Equipment.getItemIn(Equipment.Slot.WEAPON);
        });
        if (item != null)
            return OsrsBoxSearch.searchItem(item.getId());
        return null;
    }

    private void onMouseClicked(MouseEvent event){
        Node clickedNode = event.getPickResult().getIntersectedNode();
        if (clickedNode instanceof ImageView) {
            clickedNode = clickedNode.getParent();
        }
        Integer colIndex = GridPane.getColumnIndex(clickedNode);
        Integer rowIndex = GridPane.getRowIndex(clickedNode);
        if(colIndex == null || rowIndex == null) {
            selectedSlot.set(null);
        } else {
            selectedSlot.set(getSlot(colIndex, rowIndex));
        }
    }

    public void setEquipmentId(@Nonnull Equipment.Slot slot, @Nullable Integer id){
        Platform.runLater(() -> equipmentIds.put(slot, id));
    }

    private StackPane drawSlot(String background, BufferedImage itemIcon, boolean selected){
        return drawSlot(getSlotImage(background), itemIcon, selected ? getSlotImage(ResourcePaths.IMG_SELECTED_SLOT) : null);
    }

    private StackPane drawSlot(BufferedImage... images){
        StackPane pane = new StackPane();
        for (BufferedImage image : images) {
            if(image != null){
                image = ImageUtils.getScaledImage(image, scale);
                pane.getChildren().add(new ImageView(SwingFXUtils.toFXImage(image, null)));
            }
        }
        return pane;
    }

    @Nullable
    private BufferedImage getSlotImage(String name){
        return IconLoader.loadIcon(bot, ResourcePaths.IMG_BASE_EQUIPMENT_SLOT + name);
    }

    @Nullable
    private static Equipment.Slot getSlot(int col, int row){
        for (Map.Entry<Equipment.Slot, EquipmentSlotUIDefinition> entry : EQUIPMENT_SLOT_TO_GRIDPANE.entrySet()) {
            Equipment.Slot slot = entry.getKey();
            EquipmentSlotUIDefinition def = entry.getValue();
            if(def.col == col && def.row == row){
                return slot;
            }
        }
        return null;
    }

    private final static Map<Equipment.Slot, EquipmentSlotUIDefinition> EQUIPMENT_SLOT_TO_GRIDPANE = new HashMap<>();
    static {
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.HEAD, new EquipmentSlotUIDefinition(1,0, ResourcePaths.IMG_HEAD_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.CAPE, new EquipmentSlotUIDefinition(0,1, ResourcePaths.IMG_CAPE_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.NECK, new EquipmentSlotUIDefinition(1,1, ResourcePaths.IMG_NECK_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.AMMUNITION, new EquipmentSlotUIDefinition(2,1, ResourcePaths.IMG_AMMO_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.WEAPON, new EquipmentSlotUIDefinition(0,2, ResourcePaths.IMG_WEAPON_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.BODY, new EquipmentSlotUIDefinition(1,2, ResourcePaths.IMG_BODY_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.SHIELD, new EquipmentSlotUIDefinition(2,2, ResourcePaths.IMG_SHIELD_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.LEGS, new EquipmentSlotUIDefinition(1,3, ResourcePaths.IMG_LEGS_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.HANDS, new EquipmentSlotUIDefinition(0,4, ResourcePaths.IMG_HANDS_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.FEET, new EquipmentSlotUIDefinition(1,4, ResourcePaths.IMG_FEET_SLOT));
        EQUIPMENT_SLOT_TO_GRIDPANE.put(Equipment.Slot.RING, new EquipmentSlotUIDefinition(2,4, ResourcePaths.IMG_RING_SLOT));
    }

    private static class EquipmentSlotUIDefinition {
        public int col;
        public int row;
        public String imageName;

        public EquipmentSlotUIDefinition(int col, int row, String imageName) {
            this.col = col;
            this.row = row;
            this.imageName = imageName;
        }
    }
}


