package com.darksage.bots.vorkath.ui;

import com.DarkAPI.common.net.OSRSBox.GearSet;

import java.util.HashMap;

public class VorkUIData {

    public Integer[] crossbows = new Integer[] { 21012, 11785, 21902, 9185 };
    public Integer[] meleeWeps = new Integer[] { 22978, 22324, 11889, 13265 };
    public int rotationLoc = 0;
    public int blowpipeID = 12926;

    public int runePouchID = 12791;
    public int serpHelmID = 12931;
    public int slayerStaffID = 4170;

    public String saraBrew = "Saradomin brew";
    public int saraBrewID = 6685;

    public String hpRestore = null;
    public int hpRestoreID = -1;
    public int antivenomID = 12913;
    public int rangingPotID = 2444;
    public int divineRangeID = 23733;
    public int combatPotID = 12695;
    public int divineCombatID = 23685;
    public int prayerPotID = 2434;
    public int superRestoreID = 3024;
    public int anglerID = 13441;

    public int rubyDragonID = 8777;
    public int rubyBoltsID = 9273;
    public int diamondDragonID = 1690;
    public int diamondBoltsID = 9277;
    public int mithDartID = 809;
    public int addyDartID = 810;
    public int runeDartID = 811;
    public int dragDartID = 11230;

    public GearSet mainGear;
    public GearSet specGear;

}
