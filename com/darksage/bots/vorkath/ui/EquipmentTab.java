package com.darksage.bots.vorkath.ui;

import com.darksage.bots.vorkath.Vorkath;
import com.runemate.game.api.client.ClientUI;
import com.runemate.game.api.script.framework.logger.BotLogger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class EquipmentTab extends Tab {

    private final Vorkath bot;
    private final VorkathMainUI mainUI;

    private VBox vBox = new VBox();
    public Button buttonSave;
    public ToggleButton toggleMain, toggleSpec;
    public CheckBox checkSpecUsed;

    private EquipmentVisualizer equipmentVisualizer;

    public EquipmentVisualizer getEquipmentVisualizer() {
        if (equipmentVisualizer == null)
            equipmentVisualizer = new EquipmentVisualizer(bot, 1.5);
        return equipmentVisualizer;
    }

    public EquipmentTab(Vorkath bot, VorkathMainUI mainUI) {
        this.bot = bot;
        this.mainUI = mainUI;

        setClosable(false);
        vBox.setPrefSize(600, 590);
        vBox.setAlignment(Pos.CENTER);
        setText("Equipment Settings");

        HBox visual = new HBox();
        visual.setPrefSize(600, 590);
        visual.setAlignment(Pos.CENTER);
        getEquipmentVisualizer();
        buildEquipVisual(visual);
        buildEquipControls(visual);
        vBox.getChildren().add(visual);

        toggleSpec.setVisible(false);
        toggleMain.setOnAction((ActionEvent e) -> {
            if (toggleMain.isSelected())
                toggleSpec.setSelected(false);
        });

        checkSpecUsed.setOnAction((ActionEvent e)->{
            toggleSpec.setVisible(checkSpecUsed.isSelected());
        });

        toggleSpec.setOnAction((ActionEvent e)->{
            if (toggleSpec.isSelected())
                toggleMain.setSelected(false);
        });

        buttonSave.setOnAction(toggleMain.isSelected() ? mainUI.onMainGearSave() : toggleSpec.isSelected() ? mainUI.onSpecGearSave() : sendAlert());
        setContent(vBox);
    }

    private EventHandler<ActionEvent> sendAlert() {
        return (ActionEvent e) -> {
            if (checkSpecUsed.isSelected()) {
                ClientUI.showAlert(BotLogger.Level.WARN, "Please Select either Main gear or Spec gear");
            } else {
                toggleMain.setSelected(true);
                ClientUI.showAlert("Failed to save gear, Please try again");
            }
        } ;
    }

    public void updateToggle() {
        if (toggleMain.isSelected()) {
            if (toggleMain.getTextFill() == Color.GREEN)
                toggleMain.setTextFill(Color.YELLOW);
        }
        if (toggleSpec.isSelected()) {
            if (toggleSpec.getTextFill() == Color.GREEN)
                toggleSpec.setTextFill(Color.YELLOW);
        }
    }

    public void setToggleGreen() {
        if (toggleMain.isSelected()) {
            if (toggleMain.getTextFill() != Color.GREEN)
                toggleMain.setTextFill(Color.GREEN);
        }
        if (toggleSpec.isSelected()) {
            if (toggleSpec.getTextFill() != Color.GREEN)
                toggleSpec.setTextFill(Color.GREEN);
        }
    }

    private void buildEquipVisual(HBox visual) {
        VBox hBox = new VBox();
        hBox.setPrefSize(300, 590);
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(getEquipmentVisualizer());
        visual.getChildren().add(hBox);
    }

    private void buildEquipControls(HBox visual) {
        VBox vBox = new VBox();
        vBox.setPrefSize(300, 590);
        vBox.setAlignment(Pos.CENTER);
        HBox hBoxTop = new HBox();
        hBoxTop.setPrefSize(300, 100);
        HBox main = buildToggle(toggleMain, "Main Gear", true);
        HBox spec = buildToggle(toggleSpec, "Spec Gear", false);
        HBox check = buildCheck();
        HBox hBoxSpacer = new HBox();
        hBoxSpacer.setPrefSize(300, 30);
        HBox save = buildButtonSave();
        HBox tips = buildToggleTips();
        vBox.getChildren().addAll(hBoxTop, main, check, spec, hBoxSpacer, save, tips);
        visual.getChildren().add(vBox);
    }

    private HBox buildToggleTips() {
        HBox hBox = new HBox();
        hBox.setPrefSize(300, 40);
        hBox.setAlignment(Pos.CENTER);
        Label label1 = new Label("Not Saved");
        label1.setTextFill(Color.RED);
        label1.setPrefWidth(90);
        Label label2 = new Label("Modified");
        label2.setTextFill(Color.YELLOW);
        label2.setPrefWidth(90);
        Label label3 = new Label("Saved");
        label3.setTextFill(Color.GREEN);
        label3.setPrefWidth(90);
        hBox.getChildren().addAll(label1, label2, label3);
        return hBox;
    }

    private HBox buildButtonSave() {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setPrefSize(300, 50);
        buttonSave = new Button();
        buttonSave.setText("Save");
        buttonSave.setPrefWidth(120);
        hBox.getChildren().add(buttonSave);
        return hBox;
    }

    private HBox buildCheck() {
        HBox hBox = new HBox();
        hBox.setPrefSize(300, 50);
        hBox.setAlignment(Pos.CENTER);
        checkSpecUsed = new CheckBox();
        checkSpecUsed.setSelected(false);
        checkSpecUsed.setText("Spec Weapon used");
        hBox.getChildren().add(checkSpecUsed);
        return hBox;
    }

    private HBox buildToggle(ToggleButton button, String text, boolean selected) {
        HBox hBox = new HBox();
        hBox.setPrefSize(300, 60);
        hBox.setAlignment(Pos.CENTER);
        button = new ToggleButton();
        button.setSelected(selected);
        button.setText(text);
        button.setTextFill(Color.RED);
        hBox.getChildren().add(button);
        return hBox;
    }
}
