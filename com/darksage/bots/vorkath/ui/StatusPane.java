package com.darksage.bots.vorkath.ui;

import com.darksage.bots.vorkath.Vorkath;
import com.runemate.game.api.script.framework.core.LoopingThread;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class StatusPane extends VBox {

    private final Vorkath bot;
    private final VorkathMainUI mainUI;
    private Label totalKC, currentKill, totalTrips, currentTrip, killsHour, killsTrip, avgKillValue, avgCostKill, avgProfitKill, killsLeft;
    private Button buttonStart;

    public StatusPane(Vorkath bot, VorkathMainUI mainUI) {
        this.bot = bot;
        this.mainUI = mainUI;

        setPrefSize(190, 590);
        setAlignment(Pos.CENTER);

        initializeLabels();
        buildStatusPane();

        bot.getPlatform().invokeLater(()-> new LoopingThread(()-> Platform.runLater(this::updateLabels), 1500).start());
    }

    private void buildStatusPane() {
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setPrefSize(185, 440);
        vBox.setBackground(new Background(new BackgroundFill(new Color(0.2,0.2,0.2,0.3), new CornerRadii(15), Insets.EMPTY)));
        vBox.setBorder(new Border(new BorderStroke(Color.gray(.6, .2), BorderStrokeStyle.SOLID, new CornerRadii(15), BorderStroke.DEFAULT_WIDTHS)));
        HBox spacer = new HBox();
        spacer.setPrefHeight(20);
        HBox killsLeftBox = new HBox();
        killsLeftBox.setAlignment(Pos.CENTER);
        killsLeftBox.setPrefHeight(30);
        killsLeftBox.getChildren().add(killsLeft);
        HBox spacer2 = new HBox();
        spacer2.setPrefHeight(20);
        vBox.getChildren().addAll(
                createLabelHBox("Session Status", 40), createLabelHBox("Total Session KC : ", 30, totalKC),
                createLabelHBox("Current Kill : ", 30, currentKill), createLabelHBox("Total Trip Count : ", 30, totalTrips),
                createLabelHBox("Current Trip : ", 30, currentTrip), createLabelHBox("Kills Per Hour : ", 30, killsHour),
                createLabelHBox("Kills Per Trip : ", 30, killsTrip), spacer, createLabelHBox("Estimated kills remaining ", 20),
                createLabelHBox("based on supplies left : ", 20), killsLeftBox, spacer2, createLabelHBox("Avg. Kill Value : ", 30, avgKillValue),
                createLabelHBox("Avg. Cost Per Kill : ", 30, avgCostKill), createLabelHBox("Avg. Profit per Kill : ", 30, avgProfitKill)
        );
        HBox start = new HBox();
        start.setAlignment(Pos.CENTER);
        start.setPrefHeight(150);
        buttonStart = new Button("Start");
        buttonStart.setPrefWidth(100);
        buttonStart.setOnAction(mainUI.onStartPressed());
        start.getChildren().add(buttonStart);
        getChildren().addAll(vBox, start);
    }

    private HBox createLabelHBox(String text, int height, Label label) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setPrefHeight(height);
        hBox.getChildren().addAll(new Label(text), label);
        return hBox;
    }

    private HBox createLabelHBox(String text, int height) {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setPrefHeight(height);
        hBox.getChildren().add(new Label(text));
        return hBox;
    }

    private void initializeLabels() {
        totalKC = new Label();
        totalKC.prefWidth(30);
        currentKill = new Label();
        currentKill.prefWidth(30);
        totalTrips = new Label();
        totalTrips.prefWidth(30);
        currentTrip = new Label();
        currentTrip.prefWidth(30);
        killsHour = new Label();
        killsHour.prefWidth(30);
        killsTrip = new Label();
        killsTrip.prefWidth(30);
        avgKillValue = new Label();
        avgKillValue.prefWidth(30);
        avgCostKill = new Label();
        avgCostKill.prefWidth(30);
        avgProfitKill = new Label();
        avgProfitKill.prefWidth(30);
        killsLeft = new Label();
        killsLeft.setPrefWidth(40);
        killsLeft.setAlignment(Pos.CENTER);
    }

    private void updateLabels() {
        totalKC.setText(String.valueOf(bot.vars.killCount));
        currentKill.setText(String.valueOf(bot.vars.currKill));
        totalTrips.setText(String.valueOf(bot.vars.tripCount));
        currentTrip.setText(String.valueOf(bot.vars.currTrip));
        killsHour.setText(String.valueOf(bot.vars.killsHour));
        killsTrip.setText(String.valueOf(bot.vars.killsTrip));
        avgKillValue.setText(String.valueOf(bot.vars.avgLootKill));
        avgCostKill.setText(String.valueOf(bot.vars.avgCostKill));
        avgProfitKill.setText(String.valueOf(bot.vars.avgProfitKill));
        killsLeft.setText(String.valueOf(bot.vars.killsLeft));
    }
}
