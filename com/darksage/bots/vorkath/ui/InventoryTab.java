package com.darksage.bots.vorkath.ui;

import com.DarkAPI.common.util.IconLoader;
import com.DarkAPI.common.util.ItemIcons;
import com.DarkAPI.common.util.ResourcePaths;
import com.DarkAPI.common.util.TextFormatters;
import com.darksage.bots.vorkath.Vorkath;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.sun.deploy.util.StringUtils;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.awt.image.BufferedImage;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class InventoryTab extends Tab {

    private final Vorkath bot;
    private final VorkathMainUI mainUI;

    private GridPane invGrid;
    private Button buttonLoad, buttonSave;

    private final ObservableList<SpriteItem> inventory = FXCollections.observableList(IntStream.range(0, 28).mapToObj(i -> (SpriteItem)null).collect(Collectors.toList()));
    private final ObservableList<Pattern> inventorySetup = FXCollections.observableArrayList();

    public InventoryTab(Vorkath bot, VorkathMainUI mainUI) {
        this.bot = bot;
        this.mainUI = mainUI;

        setText("Inventory Settings");
        setClosable(false);
        HBox root = new HBox();
        root.setAlignment(Pos.CENTER);
        root.setPrefSize(600, 590);
        root.getChildren().add(buildVisualizer());

        buttonLoad.setOnAction(mainUI.onLoadInventory());
        buttonSave.setOnAction(mainUI.onSaveInventory());

        inventory.addListener((InvalidationListener) observable -> updateInv());

        setContent(root);

    }

    private void updateInv() {
        invGrid.getChildren().clear();
        for (int i = 0; i < 28; i++) {
            SpriteItem item = inventory.get(i);
            if (item != null) {
                StackPane pane = new StackPane();
                BufferedImage itemIcon = ItemIcons.get(item.getId());
                if(itemIcon != null){
                    pane.getChildren().add(new ImageView(SwingFXUtils.toFXImage(itemIcon, null)));
                } else {
                    pane.getChildren().add(new Label(String.valueOf(item.getId())));
                }
                if(item.getQuantity() > 1){
                    Text text = new Text(String.valueOf(item.getQuantity()));
                    text.setFill(Color.YELLOW);
                    text.setFont(new Font(8));
                    pane.getChildren().add(text);
                    StackPane.setAlignment(text, Pos.TOP_LEFT);
                }
                invGrid.add(pane, i % 4, i / 4);
            }
        }
    }

    private VBox buildVisualizer() {
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setPrefSize(320, 590);
        HBox visual = new HBox();
        visual.setPrefSize(320, 350);
        visual.setAlignment(Pos.CENTER);
        StackPane stackPane = new StackPane();
        stackPane.setAlignment(Pos.CENTER);
        stackPane.setPrefSize(210, 310);
        stackPane.getChildren().add(loadBackground());
        initGridPane();
        stackPane.getChildren().add(invGrid);
        visual.getChildren().add(stackPane);
        vBox.getChildren().add(visual);
        vBox.getChildren().add(buildLoadButton());
        return vBox;
    }

    private HBox buildLoadButton() {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setPrefSize(310, 120);
        buttonLoad = new Button("Load Inventory");
        hBox.getChildren().add(buttonLoad);
        return hBox;
    }

    private void initGridPane() {
        invGrid = new GridPane();
        invGrid.setPrefSize(200, 300);
        invGrid.setHgap(10);
        invGrid.setVgap(5);
        invGrid.setPadding(new Insets(5));
        for (int i = 0; i < 4; i++) {
            invGrid.getColumnConstraints().add(new ColumnConstraints(40));
        }
        for (int i = 0; i < 7; i++) {
            invGrid.getRowConstraints().add(new RowConstraints(40));
        }
        invGrid.setAlignment(Pos.CENTER);
    }

    /*
    210x310 (stackPane size)
    200x300 (gridPane size)
    40x40 (image view size)
     */
    private ImageView loadBackground() {
        ImageView view = new ImageView();
        view.setFitHeight(310);
        view.setFitWidth(210);
        BufferedImage image = IconLoader.loadIcon(bot, ResourcePaths.IMG_CONTROLPANEL_TAB_BACKGROUND);
        if (image != null)
            view.setImage(SwingFXUtils.toFXImage(image, null));
        return view;
    }

    public void setItem(SpriteItem item) {
        setItem(item.getIndex(), item);
    }

    public void setItem(int index, SpriteItem item){
        Platform.runLater(() -> inventory.set(index, item));
    }

    public int getRow(int slot){
        return slot / 4;
    }

    public int getCol(int slot){
        return slot % 4;
    }
}
