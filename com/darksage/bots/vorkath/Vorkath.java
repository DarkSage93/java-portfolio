package com.darksage.bots.vorkath;

import com.DarkAPI.common.net.OSRSBox.ItemProperties;
import com.DarkAPI.common.net.OSRSBox.OsrsBoxSearch;
import com.DarkAPI.data.general.DarkPlayerSense;
import com.DarkAPI.util.VorkathArea;
import com.darksage.bots.vorkath.data.BaseStates;
import com.darksage.bots.vorkath.data.VorkathKillTracker;
import com.darksage.bots.vorkath.data.VorkathVars;
import com.darksage.bots.vorkath.logical.BotStatusUpdater;
import com.darksage.bots.vorkath.logical.VorkRootSetter;
import com.darksage.bots.vorkath.ui.VorkathMainUI;
import com.regal.regal_bot.RegalSkipTreeBot;
import com.regal.regal_bot.tree_tasks.IsPlayerLoaded;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.core.LoopingThread;
import com.runemate.game.api.script.framework.listeners.ChatboxListener;
import com.runemate.game.api.script.framework.listeners.DeathListener;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.MenuInteractionListener;
import com.runemate.game.api.script.framework.listeners.events.DeathEvent;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.listeners.events.MenuInteractionEvent;
import com.runemate.game.api.script.framework.listeners.events.MessageEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import javafx.scene.layout.VBox;

public class Vorkath extends RegalSkipTreeBot implements ChatboxListener, InventoryListener, DeathListener, MenuInteractionListener {

    public VorkathVars vars = new VorkathVars();
    public VorkathArea area;

    @Override
    public Skill[] setSkillsToTrack() {
        return new Skill[]{Skill.RANGED, Skill.MAGIC};
    }

    @Override
    public VBox setSettingsVBox() {
        return new VorkathMainUI(this);
    }

    @Override
    public void onStart(String... args) {
        super.onStart(args);
        getEventDispatcher().addListener(this);
        DarkPlayerSense.initializeKeys();
        setPrayHp();

        new LoopingThread(new BotStatusUpdater().statusUpdater(), 200).start();
    }

    @Override
    public TreeTask taskToSkipTo() {
        return null;
    }

    @Override
    public TreeTask setRootTask() {
        Mouse.setPathGenerator(Mouse.MLP_PATH_GENERATOR);
        return new IsPlayerLoaded(this, new VorkRootSetter());
    }

    @Override
    public boolean isSafeToLogout() {
        return vars.edgeville.contains(getPlayer());
    }

    private void setPrayHp() {
        vars.panicPray = 10;
        vars.panicHp = 32;
        vars.triggerHp = 24;
        /*
        vars.panicPray = Random.nextInt(DarkPlayerSense.Key.VORKATH_PANIC_HP_MIN.getAsInteger(), DarkPlayerSense.Key.VORKATH_PANIC_PRAY_MAX.getAsInteger());
        vars.panicHp = Random.nextInt(DarkPlayerSense.Key.VORKATH_PANIC_HP_MIN.getAsInteger(), DarkPlayerSense.Key.VORKATH_PANIC_HP_MAX.getAsInteger());
        vars.triggerHp = Random.nextInt(DarkPlayerSense.Key.VORKATH_TRIGGER_HP_MIN.getAsInteger(), DarkPlayerSense.Key.VORKATH_TRIGGER_HP_MAX.getAsInteger());
         */
    }

    @Override
    public void onMessageReceived(MessageEvent m) {
        if (m.getMessage().equals("You have been frozen!"))
            vars.isFrozen = true;
        if (m.getMessage().equals("The dragon is currently immune to your attacks."))
            vars.isFrozen = true;
        if (m.getMessage().equals("You become unfrozen as you kill the spawn."))
            vars.isFrozen = false;
        if (m.getMessage().equals("The spawn violently explodes, unfreezing you as it does so."))
            vars.isFrozen = false;

    }

    @Override
    public void onItemAdded(ItemEvent event) {
        InventoryListener.super.onItemAdded(event);
        if (!Bank.isOpen() && vars.vorkathState.equals(BaseStates.VORKATH_LOOT))
            addLoot(event.getItem().getId(), event.getQuantityChange());
    }

    private void addLoot(int id, int quantity) {
        ItemProperties itemProps = OsrsBoxSearch.searchItem(id);
        if (vars.currKillTracker == null || vars.currKillTracker.getSessionKC() != vars.currKill) {
            if (vars.currKill > 0) {
                vars.currKillTracker = new VorkathKillTracker(vars.currKill);
                vars.currKillTracker.getLootGained().compute(itemProps, (key, val) -> (val != null ? val : 0) + quantity);
            }
        } else {
            vars.currKillTracker.getLootGained().compute(itemProps, (key, val) -> (val != null ? val : 0) + quantity);
        }
    }

    @Override
    public void onItemRemoved(ItemEvent event) {
        InventoryListener.super.onItemRemoved(event);
    }

    @Override
    public void onDeath(DeathEvent deathEvent) {
        vars.baseState = BaseStates.DEATH;
        vars.deathState = vars.deathsOffice.contains(getPlayer()) ? BaseStates.DEATH_OFFICE : BaseStates.DEATH_BANK_PRE_COLLECT;
    }

    @Override
    public void onInteraction(MenuInteractionEvent e) {
        if (e.getAction().equals("Drink")) {
            if (e.getTarget().contains("Divine ") || e.getTarget().contains("Ranging ") || e.getTarget().contains("Super combat")) {
                vars.statBoostWatch.start();
                vars.isStatBoostNeeded = false;
            }
            if (e.getTarget().contains("antifire")) {
                vars.antifireWatch.start();
                vars.isAntifireNeeded = false;
            }
        }
        if (e.getTarget().contains("Zombified spawn")) {
            vars.isSpawnAttacked = true;
        }
    }
}
