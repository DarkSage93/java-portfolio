package com.darksage.bots.vorkath.logical;

import com.darksage.bots.vorkath.Vorkath;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

public class DeathStatus extends BranchTask {

    Vorkath bot = (Vorkath) Environment.getBot();

    @Override
    public boolean validate() {
        return false;
    }

    @Override
    public TreeTask successTask() {
        return null;
    }

    @Override
    public TreeTask failureTask() {
        return null;
    }
}
