package com.darksage.bots.vorkath.logical;

import com.DarkAPI.common.net.OSRSBox.ItemProperties;
import com.DarkAPI.data.enums.Spells;
import com.DarkAPI.data.enums.Staff;
import com.DarkAPI.framework.GameObjectInteract;
import com.DarkAPI.framework.treeTasks.executable.DarkBankDepositing;
import com.DarkAPI.framework.treeTasks.executable.DarkCastSpell;
import com.DarkAPI.framework.treeTasks.executable.DarkEmptyLeaf;
import com.DarkAPI.framework.treeTasks.executable.DarkInventoryItemAction;
import com.DarkAPI.util.Delay;
import com.darksage.bots.vorkath.Vorkath;
import com.darksage.bots.vorkath.data.BaseStates;
import com.darksage.bots.vorkath.data.VorkathKillTracker;
import com.regal.regal_bot.tree_tasks.bank.GetItemsFromBank;
import com.regal.regal_bot.tree_tasks.bank.IsBankOpen;
import com.regal.utility.awesome_navigation.requirements.CombinedRequirement;
import com.regal.utility.awesome_navigation.requirements.InventoryRequirement;
import com.regal.utility.awesome_navigation.requirements.Requirement;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.House;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Health;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.osrs.local.hud.interfaces.Prayer;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;

public class BankingStatus extends BranchTask {

    Vorkath bot = (Vorkath) Environment.getBot();

    @Override
    public boolean validate() {
        return bot.vars.edgeville.contains(bot.getPlayer());
    }

    @Override
    public TreeTask successTask() {
        if (Inventory.containsAnyOf(getLootList())) {
            return new IsBankOpen(new DarkBankDepositing(buildDepositMap(), false, false));
        }
        if (Bank.isOpen()) {
            //add the kills left
        }
        if (Health.getCurrent() < Health.getMaximum()) {
            if (Inventory.contains(bot.vars.hpRestore)) {
                if (bot.vars.hpRestore.pattern().contains("Sara"))
                    return new DarkInventoryItemAction(bot.vars.hpRestore, "Drink");
                return new DarkInventoryItemAction(bot.vars.hpRestore, "Eat");
            } else {
                return new GetItemsFromBank(Collections.singletonList(new InventoryRequirement(bot.vars.hpRestore, 1)), new DarkEmptyLeaf("we have item for healing"));
            }
        }
        if (Prayer.getPoints() < Prayer.getMaximumPoints()) {
            if (Inventory.contains(bot.vars.prayRestore)) {
                return new DarkInventoryItemAction(bot.vars.prayRestore, "Drink");
            } else {
                return new GetItemsFromBank(Collections.singletonList(new InventoryRequirement(bot.vars.prayRestore, 1)), new DarkEmptyLeaf("we have item for prayer restore"));
            }
        }
        //add gear related checks (scales/darts) maybe at later time

        return new GetItemsFromBank(getWithdrawList(), new StateChange());
    }

    @Override
    public TreeTask failureTask() {
        if (bot.vars.tripCount != bot.vars.currTrip) {
            bot.vars.tripCount++;
        }
        if (House.isInside()) {
            GameObject altar = GameObjects.newQuery().names("Altar").results().first();
            if (altar != null && Prayer.getPoints() < Prayer.getMaximumPoints())
                return new GameObjectInteract(praying(), "Pray", new String[]{"Altar"});
            GameObject pool = GameObjects.newQuery().names(bot.vars.pools).results().first();
            if (pool != null && Health.getCurrent() < Health.getMaximum()) {
                return new GameObjectInteract(pool(), bot.vars.pools);
            }
            return new GameObjectInteract(Delay.untilArea(bot.vars.edgeville), "Edgeville", new String[]{"Amulet of Glory"});
        } else {
            return new DarkCastSpell(Spells.HOUSE_PORT.getSpell(), tilHouse());
        }
    }

    private static Callable<Boolean> pool() {
        return ()-> Execution.delayUntil(()-> Health.getCurrent() >= Health.getMaximum(), 2400, 3000);
    }

    private static Callable<Boolean> praying() {
        return ()-> Execution.delayUntil(()-> Prayer.getPoints() == Prayer.getMaximumPoints(), 2400, 3000);
    }

    private static Callable<Boolean> tilHouse() {
        return ()-> Execution.delayUntil(House::isInside, 1800, 2400);
    }

    private List<Requirement> getWithdrawList() {
        List<Requirement> reqs = new ArrayList<>(0);
        if (bot.vars.tripCount > 0) {
            reqs.addAll(buildAdaptiveList());
        } else {
            reqs.addAll(buildDefaultList());
        }
        return reqs;
    }

    private List<Requirement> buildAdaptiveList() {
        List<Requirement> reqs = new ArrayList<>(0);
        reqs.addAll(buildConstants());
        setAverages();
        for (Map.Entry<String, Integer> set : bot.vars.perTripAvg.entrySet()) {
            if (set.getKey().contains("Anti-v")) {
                if (set.getValue() < 4) {
                    reqs.add(new CombinedRequirement(false, new InventoryRequirement("Anti-venom(" + set.getValue() + ")", 1),
                            new InventoryRequirement("Anti-venom(" + (set.getValue() + 1) + ")", 1)));
                } else {
                    if (set.getValue() > 4) {
                        reqs.add(new InventoryRequirement("Anti-venom(4)", set.getValue() / 4));
                    }
                }
            }
            if (set.getKey().contains("antifire")) {
                if (set.getValue() < 4) {
                    reqs.add(new CombinedRequirement(false, new InventoryRequirement("Anti-venom(" + set.getValue() + ")", 1),
                            new InventoryRequirement("Extended super antifire(" + (set.getValue() + 1) + ")", 1)));
                } else {
                    if (set.getValue() > 4) {
                        reqs.add(new InventoryRequirement("Extended super antifire(4)", set.getValue() / 4));
                    }
                }
            }
            if (set.getKey().contains("Divine ra")) {
                if (set.getValue() < 4) {
                    reqs.add(new CombinedRequirement(false, new InventoryRequirement("Anti-venom(" + set.getValue() + ")", 1),
                            new InventoryRequirement("Divine ranging potion(" + (set.getValue() + 1) + ")", 1)));
                } else {
                    if (set.getValue() > 4) {
                        reqs.add(new InventoryRequirement("Divine ranging potion(4)", set.getValue() / 4));
                    }
                }
            }
            if (set.getKey().contains("Ranging")) {
                if (set.getValue() < 4) {
                    reqs.add(new CombinedRequirement(false, new InventoryRequirement("Anti-venom(" + set.getValue() + ")", 1),
                            new InventoryRequirement("Ranging potion(" + (set.getValue() + 1) + ")", 1)));
                } else {
                    if (set.getValue() > 4) {
                        reqs.add(new InventoryRequirement("Ranging potion(4)", set.getValue() / 4));
                    }
                }
            }
            if (set.getKey().contains("Divine su")) {
                if (set.getValue() < 4) {
                    reqs.add(new CombinedRequirement(false, new InventoryRequirement("Anti-venom(" + set.getValue() + ")", 1),
                            new InventoryRequirement("Divine super combat potion(" + (set.getValue() + 1) + ")", 1)));
                } else {
                    if (set.getValue() > 4) {
                        reqs.add(new InventoryRequirement("Divine super combat  potion(4)", set.getValue() / 4));
                    }
                }
            }
            if (set.getKey().contains("Super com")) {
                if (set.getValue() < 4) {
                    reqs.add(new CombinedRequirement(false, new InventoryRequirement("Anti-venom(" + set.getValue() + ")", 1),
                            new InventoryRequirement("Super combat potion(" + (set.getValue() + 1) + ")", 1)));
                } else {
                    if (set.getValue() > 4) {
                        reqs.add(new InventoryRequirement("Super combat potion(4)", set.getValue() / 4));
                    }
                }
            }
            if (set.getKey().contains("Sara"))
                reqs.add(new InventoryRequirement("Saradomin brew(4)", set.getValue() / 4));
            if (set.getKey().contains("Angler"))
                reqs.add(new InventoryRequirement("Anglerfish", set.getValue()));
            if (set.getKey().equals(bot.vars.hpRestoreString))
                reqs.add(new InventoryRequirement(bot.vars.hpRestoreString, set.getValue()));
        }
        return reqs;
    }

    private void setAverages() {
        List<HashMap<String, Integer>> allKills = getAllKills();
        HashMap<String, Integer> perKillAvg = getAvgKill(allKills);
        HashMap<String, Integer> perTripAvg = getAvgTrip(allKills);
        if (!bot.vars.perKillAvg.isEmpty()) {
            bot.vars.perKillAvg.clear();
            bot.vars.perKillAvg.putAll(perKillAvg);
        }
        if (!bot.vars.perTripAvg.isEmpty()) {
            bot.vars.perTripAvg.clear();
            bot.vars.perTripAvg.putAll(perTripAvg);
        }
    }

    private HashMap<String, Integer> getAvgTrip(List<HashMap<String, Integer>> allKills) {
        HashMap<String, Integer> holder = new HashMap<>(0);
        HashMap<String, Integer> temp = new HashMap<>(0);
        List<HashMap<String, Integer>> tripTotals = new ArrayList<>(0);
        List<HashMap<String, Integer>> tripAverages = new ArrayList<>(0);
        Integer[] killsTrip = getKillsTrip();
        setAvgKillsTrip(killsTrip);
        int x = 0; int y = 0;
        for (int i = 0; i < allKills.size(); i++) {
            for (Map.Entry<String, Integer> set : allKills.get(i).entrySet()) {
                temp.compute(set.getKey(), (key, val) -> (val != null ? val : 0) + set.getValue());
            }
            if (x == i && i != 0) {
                tripTotals.add(temp);
                temp.clear();
            }
            if (x == 0) {
                x = x + killsTrip[y];
                y++;
            }
        }
        for (int i = 0; i < tripTotals.size(); i++) {
            temp.clear();
            for (Map.Entry<String, Integer> set : tripTotals.get(i).entrySet()) {
                temp.put(set.getKey(), set.getValue() / killsTrip[i]);
            }
            tripAverages.add(temp);
        }
        temp.clear();
        for (HashMap<String, Integer> tripAverage : tripAverages) {
            for (Map.Entry<String, Integer> set : tripAverage.entrySet()) {
                temp.compute(set.getKey(), (key, val) -> (val != null ? val : 0) + set.getValue());
            }
        }
        for (Map.Entry<String, Integer> set : temp.entrySet()) {
            holder.put(set.getKey(), set.getValue() / temp.size());
        }
        return holder;
    }

    private void setAvgKillsTrip(Integer[] killsTrip) {
        int x = 0; int y = killsTrip.length;
        for (Integer integer : killsTrip) {
            x = x + integer;
        }
        bot.vars.killsTrip = (x / y);
    }

    private Integer[] getKillsTrip() {
        HashMap<Integer, Integer> temp = new HashMap<>(0);
        for (Map.Entry<Integer, VorkathKillTracker> set : bot.vars.killTracker.entrySet()) {
            temp.compute(set.getValue().getTrip(), (key, val) -> (val != null ? val : 0) + 1);
        }
        return temp.values().toArray(new Integer[0]);
    }

    private HashMap<String, Integer> getAvgKill(List<HashMap<String, Integer>> allKills) {
        HashMap<String, Integer> temp = new HashMap<>(0);
        HashMap<String, Integer> holder = new HashMap<>(0);
        for (int i = 0; i < allKills.size(); i++) {
            temp.clear();
            temp.putAll(allKills.get(i));
            for (Map.Entry<String, Integer> set : temp.entrySet()) {
                holder.compute(set.getKey(), (key, val) -> (val != null ? val : 0) + set.getValue());
            }
        }
        for (Map.Entry<String, Integer> set : holder.entrySet()) {
            holder.replace(set.getKey(), set.getValue() / allKills.size());
        }
        return holder;
    }

    private List<HashMap<String, Integer>> getAllKills() {
        List<HashMap<String, Integer>> temp = new ArrayList<>();
        for (Map.Entry<Integer, VorkathKillTracker> set : bot.vars.killTracker.entrySet()) {
            temp.add(set.getValue().getUsedDisplay());
        }
        return temp;
    }

    private List<Requirement> buildDefaultList() {
        List<Requirement> reqs = new ArrayList<>(0);
        reqs.addAll(buildConstants());
        if (bot.vars.isStatBoostUsed) {
            if (bot.vars.isRanged) {
                if (bot.vars.statBooster.pattern().contains("Divine")) {
                    reqs.add(new InventoryRequirement("Divine ranging potion(4)", bot.vars.statBoostQty));
                } else {
                    reqs.add(new InventoryRequirement("Ranging potion(4)", bot.vars.statBoostQty));
                }
            } else {
                if (bot.vars.statBooster.pattern().contains("Divine")) {
                    reqs.add(new InventoryRequirement("Divine super combat potion(4)", bot.vars.statBoostQty));
                } else {
                    reqs.add(new InventoryRequirement("Super combat potion(4)", bot.vars.statBoostQty));
                }
            }
        }
        if (!bot.vars.isSerpHelm)
            reqs.add(new InventoryRequirement("Anti-venom+(4)", bot.vars.antiVenomQty));
        if (bot.vars.prayRestore.pattern().contains("Prayer "))
            reqs.add(new InventoryRequirement("Prayer potion(4)", bot.vars.prayRestoreQty));
        if (bot.vars.prayRestore.pattern().contains("Super "))
            reqs.add(new InventoryRequirement("Super restore potion(4)", bot.vars.prayRestoreQty));
        reqs.add(new InventoryRequirement("Extended super antifire(4)", bot.vars.antifireQty));
        if (bot.vars.isAngler)
            reqs.add(new InventoryRequirement(bot.vars.angler, bot.vars.anglerQty));
        if (bot.vars.hpRestore.pattern().contains("Sara")) {
            reqs.add(new InventoryRequirement("Saradomin brew(4)", bot.vars.hpRestoreQty));
        } else {
            reqs.add(new InventoryRequirement(bot.vars.hpRestoreString, bot.vars.hpRestoreQty));
        }
        return reqs;
    }

    private List<Requirement> buildConstants() {
        List<Requirement> reqs = new ArrayList<>(0);
        if (bot.vars.isSlayerStaff)
            reqs.add(new CombinedRequirement(false,
                    new InventoryRequirement(Staff.SLAYER_STAFF_E.getStaffName(), 1),
                    new InventoryRequirement(Staff.SLAYER_STAFF.getStaffName(), 1)));
        if (bot.vars.isRunePouch)
            reqs.add(new InventoryRequirement("Rune pouch", 1));
        return reqs;
    }

    private HashMap<Pattern, Integer> buildDepositMap() {
        HashMap<Pattern, Integer> holder = new HashMap<>();
        if (bot.vars.tripCount > 0 ) {
            for (Map.Entry<Integer, VorkathKillTracker> set : bot.vars.killTracker.entrySet()) {
                if (set.getValue().getTrip() == bot.vars.tripCount + 1) {
                    HashMap<ItemProperties, Integer> loot = set.getValue().getLootGained();
                    for (ItemProperties props : loot.keySet()) {
                        holder.putIfAbsent(Regex.getPatternForExactString(props.name), 0);
                    }
                }
            }
        }
        return holder;
    }

    private List<Pattern> getLootList() {
        HashMap<Pattern, Integer> temp = buildDepositMap();
        return Arrays.asList(temp.keySet().toArray(new Pattern[0]));
    }

    private class StateChange extends LeafTask {
        Vorkath bot = (Vorkath) Environment.getBot();
        @Override
        public void execute() {
            bot.getLogger().info("Exiting Banking state");
            if (bot.vars.currTrip == bot.vars.tripCount)
                bot.vars.currTrip++;
            bot.vars.baseState = BaseStates.VORKATH;
        }
    }
}
