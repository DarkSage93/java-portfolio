package com.darksage.bots.vorkath.logical;

import com.DarkAPI.common.net.OSRSBox.GearSet;
import com.DarkAPI.common.net.OSRSBox.ItemProperties;
import com.DarkAPI.common.net.OSRSBox.OsrsBoxSearch;
import com.DarkAPI.data.enums.Spells;
import com.DarkAPI.data.enums.Staff;
import com.DarkAPI.framework.treeTasks.executable.*;
import com.DarkAPI.util.Delay;
import com.DarkAPI.util.VorkathArea;
import com.darksage.bots.vorkath.Vorkath;
import com.darksage.bots.vorkath.data.BaseStates;
import com.darksage.bots.vorkath.data.VorkathKillTracker;
import com.regal.utility.SpecialAttack;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.Actor;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.House;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Health;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.osrs.local.hud.interfaces.Prayer;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

import java.util.*;
import java.util.concurrent.Callable;

public class VorkathStatus extends BranchTask {

    Vorkath bot = (Vorkath) Environment.getBot();
    Npc vorkath;

    @Override
    public boolean validate() {
        if (bot.vars.vorkathState != null) {
            if (bot.vars.vorkathState.equals(BaseStates.VORKATH_ISLE)) {
                vorkath = Npcs.newQuery().names("Vorkath").results().first();
                if (vorkath != null)
                    bot.vars.vorkathState = BaseStates.VORKATH_PRE_FIGHT;
            }
            return bot.vars.vorkathState.equals(BaseStates.VORKATH_FIGHT);
        } else {
            bot.vars.vorkathState = BaseStates.VORKATH_ISLE;
        }
        return false;
    }

    @Override
    public TreeTask successTask() {
        if (bot.vars.isEmergencyTeleport) {
            return new DarkCastSpell(Spells.HOUSE_PORT.getSpell(), teleToHouse());
        }
        if (bot.vars.fightState != null) {
            vorkath = Npcs.newQuery().names("Vorkath").results().first();
            switch (bot.vars.fightState) {
                case POKEY_POKE:
                    return handlePokeyPoke();
                case VORK_SPEC:
                    switch (bot.vars.specAttack) {
                        case FREEZE_ATT:
                            return handleFreezeSpec();
                        case ACID_POOLS:
                            return handleAcidSpec();
                        case FIREBALL:
                            return handleFireBomb();
                    }
                case VORK_STANDARD:
                    if (isResetSpecNeeded()) resetSpecVars();
                    return handleStandard();
            }
        }
        return new DarkEmptyLeaf("#VorkathStatus : failssafe");
    }

    @Override
    public TreeTask failureTask() {
        if (bot.vars.vorkathState != null)
            switch (bot.vars.vorkathState) {
                case VORKATH_ISLE:
                    return traverseToUngael();
                case VORKATH_PRE_FIGHT:
                    return handlePreFight();
                case VORKATH_LOOT:
                    return handleLoot();
                case VORKATH_POST_FIGHT:
                    return handlePostFight();
            }
        return new DarkEmptyLeaf("#FailTask = failsafe : VorkStatus");
    }

    private TreeTask handleFreezeSpec() {
        if (isSpawnAttacked()) {
            if (bot.vars.isSpecRestore) {
                if (isPanicRestore() || isTriggerRestore())
                    return statRestore();
                if (isStaffEquipped())
                    return equipGear(bot.vars.mainGear);
                return new DarkEmptyLeaf("#HandleFreeze : We are ready to attack... waiting...");
            }
        }
        if (bot.vars.isSlayerStaff) {
            if (isStaffEquipped()) {
                if (bot.vars.isSpecRestore)
                    return new DarkNpcInteract("Zombified spawn", Delay.ticks1());
                return new DarkNpcInteract("Zombified spawn", Delay.ticks3());
            }
            return new DarkEquipItems(Staff.SLAYER_STAFF.getStaffName(), Staff.SLAYER_STAFF_E.getStaffName());
        }
        if (Spells.CRUMBLE_UNDEAD.getSpell().isSelected()) {
            if (bot.vars.isSpecRestore)
                return new DarkNpcInteract(("Zombified spawn"), Delay.ticks1());
            return new DarkNpcInteract("Zombified spawn", Delay.ticks3());
        }
        return new DarkEmptyLeaf("#HandleFreeze : We are at a failsafe point...");
    }

    private boolean isStaffEquipped() {
        return Equipment.containsAnyOf(Staff.SLAYER_STAFF.getStaffName(), Staff.SLAYER_STAFF_E.getStaffName());
    }

    private TreeTask handleStandard() {
        if (bot.vars.isSpecUsed && !isMeleeSpecDone()) return handleSpecAttack();
        if (bot.vars.isRanged && isNeedToMove()) return moveOutOfMelee();
        if (!isGearEquipped(bot.vars.mainGear)) return equipGear(bot.vars.mainGear);
        if (isEnvenomed()) return new DarkInventoryItemAction(bot.vars.antiVenom);
        if (!Prayer.isQuickPraying()) return new DarkQuickPrayer(true);
        if (isPanicRestore()) return statRestore();
        return attackOrWait();
    }

    private boolean isSpawnAttacked() {
        return bot.vars.isSpawnAttacked;
    }

    private TreeTask handlePokeyPoke() {
        if (!isVorkathPoked()) return new DarkNpcInteract("Vorkath", "Poke");
        if (!Prayer.isQuickPraying()) return new DarkQuickPrayer(true);
        bot.vars.isFightStarted = true;
        if (isVorkathWaking()) return new DarkEmptyLeaf("Waiting for Vorkath to wake...");
        if (bot.vars.isSpecUsed && !isMeleeSpecDone()) return handleSpecAttack();
        if (isNeedToMove()) return moveOutOfMelee();
        return new DarkEmptyLeaf("#HandlePokeyPoke : failsafe");
    }

    private void resetSpecVars() {
        bot.vars.acidGoal = null;
        bot.vars.fireBombTarget = null;
        bot.vars.acidPath = new ArrayList<>(0);
        bot.vars.isSpawnAttacked = false;
        bot.vars.isSpecRestore = false;
        bot.vars.isFrozen = false;
    }

    private boolean isResetSpecNeeded() {
        return bot.vars.acidGoal != null || bot.vars.fireBombTarget != null || bot.vars.acidPath.size() > 0 || bot.vars.isSpawnAttacked || bot.vars.isSpecRestore || bot.vars.isFrozen;
    }

    private TreeTask handleAcidSpec() {
        if (!bot.getPlayer().isMoving()) {
            Coordinate pos = bot.getPlayer().getPosition();
            if (pos != null) return randomWalk(pos);
        }
        if (isPathGenerated()) {
            return acidWalk();
        } else {
            generatePath();
        }
        return new DarkEmptyLeaf("#handleAcidSpecs : we have reached a failsafe point");
    }

    private TreeTask acidWalk() {
        if (Traversal.isRunEnabled())
            return new DarkRunToggle(false);
        Coordinate coord1 = bot.vars.acidPath.get(0);
        Coordinate coord2 = bot.vars.acidPath.get(bot.vars.acidPath.size() - 1);
        if (bot.vars.acidGoal == null) {
            bot.vars.acidGoal = determineStart(coord1, coord2);
            return new DarkTileWalk(bot.vars.acidGoal, Delay.reaction());
        }
        int dist = getGoalDist(bot.vars.acidPath.size());
        if ((int) bot.vars.acidGoal.distanceTo(bot.getPlayer()) > dist) {
            return Prayer.isQuickPraying() ? new DarkQuickPrayer(false) : isTriggerRestore() ? statRestore() : new DarkEmptyLeaf("We are in acid walk no action needed...");
        } else {
            bot.vars.acidGoal = swapGoal(coord1, coord2);
            return new DarkTileWalk(bot.vars.acidGoal, Delay.ticks1());
        }
    }

    private Coordinate swapGoal(Coordinate coord1, Coordinate coord2) {
        return bot.vars.acidGoal.equals(coord1) ? coord2 : coord1;
    }

    private Coordinate determineStart(Coordinate coord1, Coordinate coord2) {
        int x = (int) coord1.distanceTo(bot.getPlayer());
        int y = (int) coord2.distanceTo(bot.getPlayer());
        return x < y ? coord1 : coord2;
    }

    private int getGoalDist(int size) {
        return size == 3 ? 1 : size == 4 || size == 5 ? 2 : 3;
    }

    private void generatePath() {
        Map.Entry<List<Coordinate>, Integer> map = VorkathArea.getAcidPath(vorkath.getPosition(), bot.getPlayer());
        bot.vars.acidPath.addAll(map.getKey());
    }

    private boolean isPathGenerated() {
        return bot.vars.acidPath.size() > 0;
    }

    private TreeTask moveOutOfMelee() {
        Coordinate curr = bot.getPlayer().getPosition();
        if (curr != null)
            return new DarkTileWalk(curr.derive(0, -2), Delay.ticks3());
        return new DarkEmptyLeaf("#moveOutOfMelee : failsafe");
    }

    private boolean isNeedToMove() {
        if (vorkath != null && vorkath.getPosition() != null) {
            Area.Rectangular vorkMelee = new Area.Rectangular(vorkath.getPosition().derive(-1, -1), vorkath.getPosition().derive(+6, +6));
            return vorkMelee.contains(bot.getPlayer());
        }
        return false;
    }

    private TreeTask randomWalk(Coordinate coordinate) {
        Coordinate goal = VorkathArea.isWalkRight(vorkath.getPosition(), coordinate, true) ? coordinate.derive(+5, 0) : coordinate.derive(-5, 0);
        return new DarkTileWalk(goal, Delay.ticks1());
    }

    private TreeTask handleFireBomb() {
        if (bot.vars.fireBombTarget == null)
            bot.vars.fireBombTarget = bot.getPlayer().getPosition();
        Coordinate coord = bot.getPlayer().getPosition();
        if (coord != null && vorkath != null) {
            Coordinate goal = VorkathArea.isWalkRight(vorkath.getPosition(), coord, false) ? bot.vars.fireBombTarget.derive(+2, 0) : bot.vars.fireBombTarget.derive(-2, 0);
            return goal.equals(bot.getPlayer().getPosition()) ? new DarkNpcInteract("Vorkath") : new DarkTileWalk(goal, true, Delay.ticks1());
        }
        String reason = vorkath != null ? "Fire Bomb Spec Error : Vorkath is null" : "Fire Bomb Spec Error : Local position is null";
        return new DarkEmptyLeaf(reason);
    }

    private TreeTask statRestore() {
        if (Health.getCurrent() <= bot.vars.panicHp || Health.getCurrent() <= bot.vars.triggerHp) {
            bot.vars.isEmergencyTeleport = !Inventory.contains(bot.vars.hpRestore) && Health.getCurrent() <= bot.vars.panicHp;
            if (bot.vars.hpRestore.pattern().contains("Saradomin brew"))
                return new DarkInventoryItemAction(bot.vars.hpRestore, "Drink", Delay.reaction());
            return new DarkInventoryItemAction(bot.vars.hpRestore, "Eat", Delay.reaction());
        }
        if (Prayer.getPoints() <= bot.vars.panicPray || Prayer.getPoints() <= bot.vars.triggerPray) {
            bot.vars.isEmergencyTeleport = !Inventory.contains(bot.vars.prayRestore) && Prayer.getPoints() <= 5;
            return new DarkInventoryItemAction(bot.vars.prayRestore, "Drink", Delay.reaction());
        }
        return new DarkEmptyLeaf("#statRestore() : we don't need to restore any stats");
    }

    private TreeTask handleSpecAttack() {
        if (!isMeleeSpecDone())
            if (bot.vars.specEnergyStart == -1) bot.vars.specEnergyStart = SpecialAttack.getEnergy();
        if (SpecialAttack.isActivated()) {
            return new DarkNpcInteract("Vorkath", "Attack", Delay.ticks2());
        } else if (!SpecialAttack.isActivated() && SpecialAttack.getEnergy() > 60) {
            return new DarkSpecialAttack(true);
        }
        bot.vars.isSpecUsed = true;
        return new DarkEmptyLeaf("#HandleSpecAttack : We can't activate special attack");
    }

    private TreeTask attackOrWait() {
        Actor target = bot.getPlayer().getTarget();
        if (target != null) return target.equals(vorkath) ? new DarkEmptyLeaf("We are actively fighting Vorkath", Delay.reaction()) : new DarkNpcInteract("Vorkath", "Attack");
        return new DarkNpcInteract("Vorkath", "Attack");
    }

    private boolean isVorkathWaking() {
        Npc npc = Npcs.newQuery().names("Vorkath").actions("Poke").results().first();
        Npc npc1 = Npcs.newQuery().names("Vorkath").actions("Attack").results().first();
        return npc == null && npc1 == null;
    }

    private boolean isVorkathPoked() {
        return (Npcs.newQuery().names("Vorkath").actions("Poke").results().first()) == null;
    }

    private boolean isMeleeSpecDone() {
        return bot.vars.isMeleeSpecUsed = bot.vars.specEnergyStart > SpecialAttack.getEnergy();
    }

    private boolean isEnvenomed() {
        return Health.isVenomous();
    }

    //FailTask Methods

    private TreeTask handlePostFight() {
        if (isTriggerRestore()) {
            if (bot.vars.isTriggerHP)
                return new DarkInventoryItemAction(bot.vars.hpRestore);
            return new DarkInventoryItemAction(bot.vars.prayRestore);
        } else {
            if (bot.vars.currKillTracker.getItemsUsed().isEmpty())
                setItemsUsed();
            setPostFightKillTrack();
        }

        return new DarkEmptyLeaf("#PostFight failsafe");
    }

    private void setPostFightKillTrack() {
        bot.vars.isFightStarted = false;
        bot.vars.killTracker.put(bot.vars.currKill, bot.vars.currKillTracker);
        bot.vars.killCount++;
    }

    private TreeTask handleLoot() {
        Area.Rectangular lootArea = new Area.Rectangular(bot.vars.vorkathPos.derive(-1, -1), bot.vars.vorkathPos.derive(+6, +6));
        LocatableEntityQueryResults<GroundItem> items = GroundItems.newQuery().within(lootArea).results();
        if (items.size() > 0) {
            if (Inventory.getEmptySlots() < items.size()) {
                if (bot.vars.isTriggerHP) {
                    return new DarkInventoryItemAction(bot.vars.hpRestore);
                } else {
                    if (lootArea.contains(bot.getPlayer())) {
                        Coordinate lootSpot = items.get(0).getPosition();
                        if (lootSpot != null)
                            return new DarkTileWalk(lootSpot.derive(0, -1), Delay.ticks1());
                    } else {
                        return new DarkInventoryItemAction(bot.vars.hpRestore, "Drop");
                    }
                }
            }
            return new DarkGroundItemTake(lootArea);
        }
        return new StateChange(BaseStates.VORKATH_POST_FIGHT);
    }

    private TreeTask handlePreFight() {
        setItemsStart();
        if (Traversal.isRunEnabled())
            return new DarkRunToggle(false);

        if (bot.vars.isSpecUsed) {
            GearSet main = new GearSet("copy of spec");
            main.copy(bot.vars.specGear);
            if (bot.vars.isSpecUsed && !isGearEquipped(main))
                return equipGear(main);
        }
        GearSet main = new GearSet("copy of main");
        main.copy(bot.vars.mainGear);
        if (!bot.vars.isSpecUsed && !isGearEquipped(main)) {
            return equipGear(main);
        }

        if (!isQuickPrayersSet())
            return new DarkQuickPrayer(getQuickPray());
        if (bot.vars.isTriggerPray)
            return new DarkInventoryItemAction(bot.vars.prayRestore);
        if (bot.vars.isTriggerHP)
            return new DarkInventoryItemAction(bot.vars.hpRestore);
        if (bot.vars.isAngler && Health.getCurrent() <= Health.getMaximum() && Inventory.contains(bot.vars.angler))
            return new DarkInventoryItemAction(bot.vars.angler);
        if (bot.vars.isStatBoostUsed && bot.vars.isStatBoostNeeded && Inventory.contains(bot.vars.statBooster))
            return new DarkInventoryItemAction(bot.vars.statBooster);
        if (bot.vars.isAntifireNeeded)
            return new DarkInventoryItemAction(bot.vars.antifire);
        if (bot.vars.isSpecUsed && !SpecialAttack.isActivated())
            return new DarkSpecialAttack(true);

        return new StateChange(BaseStates.VORKATH_FIGHT);
    }

    private boolean isQuickPrayersSet() {
        return compareQuickPrayers(getQuickPray());
    }

    private Prayer[] getQuickPray() {
        Prayer[] prayers;
        if (!bot.vars.isRanged) {
            if (Prayer.PIETY.isActivatable()) {
                prayers = new Prayer[] {Prayer.PIETY, Prayer.PROTECT_FROM_MAGIC};
            } else {
                prayers = new Prayer[] { Prayer.PROTECT_FROM_MAGIC};
            }
        } else {
            if (bot.vars.isCrossbow) {
                if (Prayer.RIGOUR.isActivatable()) {
                    prayers = new Prayer[] {Prayer.RIGOUR, Prayer.PROTECT_FROM_MISSILES};
                } else {
                    prayers = new Prayer[] {Prayer.EAGLE_EYE, Prayer.PROTECT_FROM_MISSILES};
                }
            } else {
                if (Prayer.RIGOUR.isActivatable()) {
                    prayers = new Prayer[] {Prayer.RIGOUR, Prayer.PROTECT_FROM_MAGIC};
                } else {
                    prayers = new Prayer[] {Prayer.EAGLE_EYE, Prayer.PROTECT_FROM_MAGIC};
                }
            }
        }
        return prayers;
    }

    private boolean compareQuickPrayers(Prayer[] prayers) {
        List<Prayer> quickPrayers = Prayer.getSelectedQuickPrayers();
        return quickPrayers.size() == prayers.length && quickPrayers.containsAll(Arrays.asList(prayers));
    }

    private TreeTask equipGear(GearSet set) {
        return new DarkEquipItems(set.getItems().stream().filter(Objects::nonNull).map((val)->val.name).toArray(String[]::new));
    }

    private boolean isGearEquipped(GearSet set) {
        List<ItemProperties> gear = set.getItems();
        int[] ids = gear.stream().filter(Objects::nonNull).mapToInt((val) -> val.id).toArray();
        return Equipment.containsAllOf(ids);
    }

    private TreeTask traverseToUngael() {
        if (bot.vars.isInVorkLair)
            return new StateChange(BaseStates.VORKATH_PRE_FIGHT);
        if (bot.vars.edgeville.contains(bot.getPlayer()))
            return new DarkCastSpell(Spells.HOUSE_PORT.getSpell(), teleToHouse());
        if (House.isInside())
            return new DarkGameObject(leaveHouse(), new String[]{"Portal"});
        if (bot.vars.vorkEntrance.contains(bot.getPlayer()))
            return new DarkGameObject(Delay.ticks3(), "Climb-over", new String[] {"Ice chunks"});
        if (bot.getPlayer().isMoving() && !bot.vars.ungael.contains(bot.getPlayer()))
            return new DarkEmptyLeaf("waiting to cross ice");
        boolean web = !bot.vars.ungael.contains(bot.getPlayer());
        Area area = bot.vars.ungael.contains(bot.getPlayer()) ? bot.vars.vorkEntrance : bot.vars.ungael;
        return new DarkTraversal(area, "Ungael - Vorkath Isle", web, bot);
    }

    private Callable<Boolean> teleToHouse() {
        return ()-> Execution.delayUntil(House::isInside, 2400, 3000);
    }

    private Callable<Boolean> leaveHouse() {
        return () -> Execution.delayUntil(()->!House.isInside(), 2400, 3000);
    }

    private boolean isTriggerRestore() {
        return bot.vars.isTriggerHP || bot.vars.isTriggerPray;
    }

    private boolean isPanicRestore() {
        return bot.vars.isPanicHP || bot.vars.isPanicPray;
    }

    private void setItemsStart() {
        if (isKillTrackerInit()) {
            initKillTracking();
        }
        if (bot.vars.killCount == bot.vars.currKill) {
            incrementKillTracking();
        }
        if (bot.vars.currKillTracker.getItemsStart() == null) {
            bot.vars.currKillTracker.setItemsStart(getCurrentSupplies());
        }
    }

    private boolean isKillTrackerInit() {
        return bot.vars.currKillTracker == null;
    }

    private void initKillTracking() {
        if (bot.vars.currKill <= 0) {
            bot.vars.currKill = 1;
            bot.vars.killCount = 0;
            bot.vars.tripCount = 0;
            bot.vars.currTrip = 1;
            bot.vars.currKillTracker = new VorkathKillTracker(bot.vars.currKill);
            bot.vars.currKillTracker.setSessionKC(0);
            bot.vars.currKillTracker.setTrip(bot.vars.currTrip);
        }
    }

    private void incrementKillTracking() {
        bot.vars.currKill++;
        if (bot.vars.currTrip == bot.vars.tripCount)
            bot.vars.currTrip++;
        bot.vars.currKillTracker = new VorkathKillTracker(bot.vars.currKill);
        bot.vars.currKillTracker.setSessionKC(bot.vars.killCount);
        bot.vars.currKillTracker.setTrip(bot.vars.currTrip);
    }

    private void setItemsUsed() {
        HashMap<ItemProperties, Integer> start = bot.vars.currKillTracker.getItemsStart();
        HashMap<ItemProperties, Integer> end = getCurrentSupplies();
        HashMap<String, Integer> display = new HashMap<>();
        HashMap<ItemProperties, Integer> holder = new HashMap<>();
        String name; int potDose = 0;
        for (Map.Entry<ItemProperties, Integer> set : start.entrySet()) {
            for (Map.Entry<ItemProperties, Integer> endSet : end.entrySet()) {
                if (set.getKey().equals(endSet.getKey())) {
                    name = set.getKey().name; String s = name;
                    if (s.matches("[\\d]+[A-Za-z]?")) {
                        potDose = Integer.parseInt(s.replaceAll("[\\D]", ""));
                        name = name.substring(0, name.length() - 3);
                        name = name + "doses ";
                    }
                    int difference = set.getValue() - endSet.getValue();
                    holder.compute(set.getKey(), (key, val) -> (val != null ? val : 0) + difference);
                    if (potDose != 0) {
                        int dif = getDosesUsed(potDose, set.getValue(), endSet.getValue());
                        display.compute(name, (key, val) -> (val != null ? val : 0) + dif);
                    }
                    display.compute(name, (key, val) -> (val != null ? val : 0) + difference);
                }
            }
        }
        bot.vars.currKillTracker.setItemsUsed(holder);
        bot.vars.currKillTracker.setUsedDisplay(display);
    }

    private int getDosesUsed(int potDose, int start, int end) {
        return ((start * potDose) - (end * potDose));
    }

    private HashMap<ItemProperties, Integer> getCurrentSupplies() {
        HashMap<ItemProperties, Integer> holder = new HashMap<>();
        int[] ids = Inventory.getItems().stream().mapToInt(SpriteItem::getId).toArray();
        List<ItemProperties> propsList = new ArrayList<>();
        for (int id : ids) {
            ItemProperties temp = OsrsBoxSearch.searchItem(id);
            if (temp != null) {
                if (!temp.equipable && !temp.name.contains("Rune pouch"))
                    propsList.add(temp);
            }
        }
        for (ItemProperties itemProps : propsList) {
            holder.compute(itemProps, (key, val) -> (val != null ? val : 0) + 1);
        }
        return holder;
    }

    private class StateChange extends LeafTask {

        private final BaseStates state;

        public StateChange(BaseStates states) {
            this.state = states;
        }

        @Override
        public void execute() {
            bot.vars.vorkathState = state;
        }
    }
}
