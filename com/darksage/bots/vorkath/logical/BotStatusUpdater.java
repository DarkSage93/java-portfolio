package com.darksage.bots.vorkath.logical;

import com.DarkAPI.common.net.OSRSBox.EquipmentHelper;
import com.DarkAPI.common.net.OSRSBox.OsrsBoxSearch;
import com.DarkAPI.util.VorkathArea;
import com.darksage.bots.vorkath.Vorkath;
import com.darksage.bots.vorkath.data.BaseStates;
import com.darksage.bots.vorkath.data.FightState;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Health;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.osrs.local.hud.interfaces.Prayer;

public class BotStatusUpdater {

    Vorkath bot = (Vorkath) Environment.getBot();

    public Runnable statusUpdater() {
        return new Runnable() {
            Npc vorkath;
            Coordinate vorkPos;
            @Override
            public void run() {
                if (bot.vars.isStart && RuneScape.isLoggedIn()) {
                    setVorkath();
                    if (vorkath != null && (vorkPos = vorkath.getPosition()) != null) {
                        if ((bot.vars.isInVorkLair = isInArea())) {
                            setGearAmmoSlot();
                            if (bot.vars.vorkathState == null) {
                                bot.vars.vorkathState = BaseStates.VORKATH_PRE_FIGHT;
                            } else {
                                if (!bot.vars.vorkathState.equals(BaseStates.VORKATH_FIGHT) &&
                                        !bot.vars.vorkathState.equals(BaseStates.VORKATH_POST_FIGHT) &&
                                        !bot.vars.vorkathState.equals(BaseStates.VORKATH_LOOT))
                                    bot.vars.vorkathState = BaseStates.VORKATH_PRE_FIGHT;
                            }
                        } else {
                            bot.vars.vorkathState = BaseStates.VORKATH_ISLE;
                        }

                    }
                    if (bot.vars.vorkathState != null && bot.vars.vorkathState != BaseStates.VORKATH_ISLE)
                        vorkathStatus();
                    if (bot.vars.triggerPray < 1)
                        setPrayTrigger();
                    if (bot.vars.baseState == BaseStates.RESET) {
                        if (canFight()) {
                            bot.vars.baseState = BaseStates.VORKATH;
                        } else {
                            bot.vars.baseState = BaseStates.BANK;
                        }
                    }
                } else {
                    bot.getLogger().fine("Waiting on GUI to perform status updates...");
                }
                bot.getLogger().debug(bot.vars.vorkathState);
            }

            private boolean isInArea() {
                setVorkathInstanceArea(vorkPos);
                return bot.vars.vorkLair.contains(bot.getPlayer());
            }

            private void setVorkathInstanceArea(Coordinate pos) {
                bot.vars.vorkLair = VorkathArea.buildFightArea(pos);
            }

            private boolean isLairValid() {
                return bot.vars.vorkLair != null;
            }


            private void vorkathStatus() {
                if (bot.vars.vorkathState != null && bot.vars.vorkathState.equals(BaseStates.VORKATH_FIGHT))
                    bot.vars.isFightStarted = true;
                if (bot.vars.isFightStarted) {
                    setFightState();
                    bot.vars.isPanicHP = Health.getCurrent() <= bot.vars.panicHp;
                    bot.vars.isTriggerHP = Health.getCurrent() <= Health.getMaximum() - bot.vars.triggerHp;
                    bot.vars.isPanicPray = Prayer.getPoints() <= bot.vars.panicPray;
                    bot.vars.isTriggerPray = Prayer.getPoints() <= Prayer.getMaximumPoints() - bot.vars.triggerPray;
                } else {
                    if (!canFight()) {
                        bot.vars.baseState = BaseStates.BANK;
                    } else {
                        if (!isInArea()) {
                            bot.vars.vorkathState = BaseStates.VORKATH_ISLE;
                        } else {
                            if (bot.vars.vorkathState == null) {
                                bot.vars.vorkathState = BaseStates.VORKATH_PRE_FIGHT;
                            } else if (!bot.vars.vorkathState.equals(BaseStates.VORKATH_FIGHT)
                                    && !bot.vars.vorkathState.equals(BaseStates.VORKATH_LOOT)
                                    && !bot.vars.vorkathState.equals(BaseStates.VORKATH_POST_FIGHT))
                                bot.vars.vorkathState = BaseStates.VORKATH_PRE_FIGHT;
                        }
                    }
                }
                if (bot.vars.vorkathState == null)
                    bot.vars.vorkathState = BaseStates.VORKATH_ISLE;
            }

            private boolean canFight() {
                boolean canFight = Inventory.getQuantity(bot.vars.prayRestore) >= 1;
                if (bot.vars.hpRestore.pattern().contains("Sara") && Inventory.getQuantity(bot.vars.hpRestore) < 2)
                    canFight = false;
                if (!bot.vars.hpRestore.pattern().contains("Sara") && Inventory.getQuantity(bot.vars.hpRestore) < 4)
                    canFight = false;
                if (!bot.vars.statBoostWatch.isRunning() && bot.vars.isStatBoostUsed && !Inventory.contains(bot.vars.statBooster))
                    canFight = false;
                if (!bot.vars.antifireWatch.isRunning() && !Inventory.contains(bot.vars.antifire))
                    canFight = false;
                return canFight;
            }

            private void setFightState() {
                if (vorkath != null) {
                    int animationID = vorkath.getAnimationId();
                    System.out.println(animationID + " animation ID");
                    switch (animationID) {
                        case 7949:
                            bot.vars.vorkathState = BaseStates.VORKATH_LOOT;
                            bot.vars.fightState = null;
                            break;
                        case 7950:
                            bot.vars.fightState = FightState.POKEY_POKE;
                            break;
                        case 7951:
                        case 7952:
                            bot.vars.fightState = FightState.VORK_STANDARD;
                            break;
                        case 7960:
                            bot.vars.fightState = FightState.SPEC_ATT;
                            bot.vars.specAttack = specAttackType();
                        case 7957:
                            bot.vars.fightState = FightState.SPEC_ATT;
                            bot.vars.specAttack = FightState.ACID_POOLS;
                            break;
                        case -1:
                            if (isLootOnGround()) {
                                bot.vars.vorkathState = BaseStates.VORKATH_LOOT;
                                break;
                            }
                            if (!bot.vars.isFightStarted) {
                                if (!isVorkathPoked() && isReadyToFight()) {
                                    bot.vars.vorkathState = BaseStates.VORKATH_FIGHT;
                                    bot.vars.fightState = FightState.POKEY_POKE;
                                    break;
                                }
                                if (!isReadyToFight())
                                    bot.vars.vorkathState = BaseStates.VORKATH_PRE_FIGHT;
                                break;
                            }
                    }
                } else {
                    setVorkath();
                }
            }

            private boolean isReadyToFight() {
                boolean ready = Health.getCurrent() > Health.getMaximum() - bot.vars.triggerHp;
                if (Prayer.getPoints() <= bot.vars.triggerPray)
                    ready = false;
                if (bot.vars.antifireWatch.isRunning()) {
                    if (antifireExpired(bot.vars.antifireWatch.getRuntime())) {
                        bot.vars.antifireWatch.stop();
                        bot.vars.antifireWatch.reset();
                        ready = false;
                        bot.vars.isAntifireNeeded = true;
                    }
                } else {
                    bot.vars.isAntifireNeeded = true;
                }
                if (bot.vars.statBoostWatch.isRunning()) {
                    if (boostExpired(bot.vars.statBoostWatch.getRuntime())) {
                        bot.vars.statBoostWatch.stop();
                        bot.vars.statBoostWatch.reset();
                        ready = false;
                        bot.vars.isStatBoostNeeded = true;
                    }
                } else {
                    bot.vars.isStatBoostNeeded = true;
                }
                return ready;
            }

            private void setPrayTrigger() {
                int temp = (int)(Prayer.getMaximumPoints() * .25);
                bot.vars.triggerPray = temp + 7;
            }

            private boolean antifireExpired(long runTime) {
                return runTime >= 355000;
            }

            private boolean boostExpired(long runTime) {
                return runTime >= 295000;
            }

            private void setVorkath() {
                vorkath = Npcs.newQuery().names("Vorkath").results().first();
            }

            private FightState specAttackType() {
                return bot.vars.isFrozen ? FightState.FREEZE_ATT : FightState.FIREBALL;
            }

            private boolean isLootOnGround() {
                if (bot.vars.vorkathPos != null)
                    return !GroundItems.newQuery().within(new Area.Rectangular(bot.vars.vorkathPos.derive(-1, -1), bot.vars.vorkathPos.derive(+6, +6))).results().isEmpty();
                return false;
            }

            private boolean isVorkathPoked() {
                return (Npcs.newQuery().names("Vorkath").actions("Poke").results().first()) == null;
            }

            private boolean isBelow30() {
                if (vorkath != null)
                    if (vorkath.getHealthGauge() != null) {
                        int curr = vorkath.getHealthGauge().getPercent();
                        return curr <= 30;
                    }
                return false;
            }

            private boolean isMultiAmmo() {
                return bot.vars.diamondBolts != null && bot.vars.rubyBolts != null;
            }

            private void setGearAmmoSlot() {
                if (isMultiAmmo()) {
                    if (isBelow30()) {
                        if (bot.vars.mainGear.getItem(Equipment.Slot.AMMUNITION).id != bot.vars.diamondBoltsID)
                            bot.vars.mainGear.setItem(EquipmentHelper.Slot.AMMUNITION, OsrsBoxSearch.searchItem(bot.vars.diamondBoltsID));
                        if (bot.vars.specGear != null && !bot.vars.specGear.getItems().isEmpty())
                            bot.vars.specGear.setItem(EquipmentHelper.Slot.AMMUNITION, OsrsBoxSearch.searchItem(bot.vars.diamondBoltsID));
                    } else {
                        if (bot.vars.mainGear.getItem(Equipment.Slot.AMMUNITION).id != bot.vars.rubyBoltsID)
                            bot.vars.mainGear.setItem(EquipmentHelper.Slot.AMMUNITION, OsrsBoxSearch.searchItem(bot.vars.rubyBoltsID));
                        if (bot.vars.specGear != null && !bot.vars.specGear.getItems().isEmpty())
                            bot.vars.specGear.setItem(EquipmentHelper.Slot.AMMUNITION, OsrsBoxSearch.searchItem(bot.vars.rubyBoltsID));
                    }
                }
            }
        };
    }
}
