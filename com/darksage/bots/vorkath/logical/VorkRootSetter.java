package com.darksage.bots.vorkath.logical;

import com.DarkAPI.framework.treeTasks.executable.DarkEmptyLeaf;
import com.DarkAPI.util.Delay;
import com.darksage.bots.vorkath.Vorkath;
import com.darksage.bots.vorkath.data.BaseStates;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

public class VorkRootSetter extends BranchTask {

    Vorkath bot = (Vorkath) Environment.getBot();

    @Override
    public boolean validate() {
        return isStart();
    }

    @Override
    public TreeTask successTask() {
        switch (bot.vars.baseState) {
            case RESET:
                bot.vars.baseState = BaseStates.VORKATH;
                return new DarkEmptyLeaf("we are Initialising...", Delay.ticks1());
            case VORKATH:
                return new VorkathStatus();
            case BANK:
                return new BankingStatus();
        }
        return new DarkEmptyLeaf("Failsafe point : #RootSetter");
    }

    @Override
    public TreeTask failureTask() {
        return new DarkEmptyLeaf("Please choose your desired settings and press start.");
    }

    private boolean isStart() {
        if (!bot.vars.isStart) {
            Execution.delayUntil(()->bot.vars.isStart, 30000);
            if (isTimeout()) {
                bot.stopBot("Settings were not chosen and set... Failsafe Stop");
            }
        }
        return bot.vars.isStart;
    }

    private boolean isTimeout() {
        return bot.getUI().getRunTimer().getRuntime() >= 600000;
    }
}
