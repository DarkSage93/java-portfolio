package com.darksage.bots.vorkath.data;

public enum FightState {
    VORK_SPEC,
    VORK_STANDARD,

    POKEY_POKE,
    SPEC_ATT,
    FREEZE_ATT,
    ACID_POOLS,
    FIREBALL,
    MELEE,

}
