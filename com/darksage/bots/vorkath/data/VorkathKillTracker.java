package com.darksage.bots.vorkath.data;

import com.DarkAPI.common.net.OSRSBox.ItemProperties;

import java.util.HashMap;

public class VorkathKillTracker {

    private int sessionKC;
    private int acctKC;
    private int trip;
    private int killLootValue;
    private int deathCount = 0;

    private boolean isDeathKC;

    private long killTime;

    private HashMap<ItemProperties, Integer> deathCost = new HashMap<>(0);

    private HashMap<ItemProperties, Integer> lootGained = new HashMap<>(0); // id, quantity
    private HashMap<ItemProperties, Integer> itemsUsed = new HashMap<>(0);
    private HashMap<String, Integer> usedDisplay = new HashMap<>(0);
    private HashMap<ItemProperties, Integer> itemsStart = new HashMap<>(0);

    public VorkathKillTracker(int kc) {
        this.setSessionKC(kc);
    }

    public boolean isDeathKC() {
        return isDeathKC;
    }

    public HashMap<ItemProperties, Integer> getDeathCost() {
        return deathCost;
    }

    public int getDeathCount() {
        return deathCount;
    }

    public void setDeathCost(HashMap<ItemProperties, Integer> deathCost) {
        this.deathCost = deathCost;
    }

    public void setDeathCount(int deathCount) {
        this.deathCount = deathCount;
    }

    public void setDeathKC(boolean deathKC) {
        isDeathKC = deathKC;
    }

    public HashMap<ItemProperties, Integer> getItemsStart() {
        return itemsStart;
    }

    public void setItemsStart(HashMap<ItemProperties, Integer> itemsStart) {
        this.itemsStart = itemsStart;
    }

    public int getSessionKC() {
        return sessionKC;
    }

    public void setSessionKC(int sessionKC) {
        this.sessionKC = sessionKC;
    }

    public int getAcctKC() {
        return acctKC;
    }

    public void setAcctKC(int totalKc) {
        this.acctKC = totalKc;
    }

    public int getTrip() {
        return trip;
    }

    public void setTrip(int trip) {
        this.trip = trip;
    }

    public int getKillLootValue() {
        return killLootValue;
    }

    public void setKillLootValue(int killLootValue) {
        this.killLootValue = killLootValue;
    }

    public long getKillTime() {
        return killTime;
    }

    public void setKillTime(long killTime) {
        this.killTime = killTime;
    }

    public HashMap<ItemProperties, Integer> getLootGained() {
        return lootGained;
    }

    public void setLootGained(HashMap<ItemProperties, Integer> lootGained) {
        this.lootGained = lootGained;
    }

    public HashMap<ItemProperties, Integer> getItemsUsed() {
        return itemsUsed;
    }

    public void setItemsUsed(HashMap<ItemProperties, Integer> itemsUsed) {
        this.itemsUsed = itemsUsed;
    }

    public HashMap<String, Integer> getUsedDisplay() {
        return usedDisplay;
    }

    public void setUsedDisplay(HashMap<String, Integer> usedDisplay) {
        this.usedDisplay = usedDisplay;
    }
}
