package com.darksage.bots.vorkath.data;

import com.DarkAPI.common.net.OSRSBox.GearSet;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.util.StopWatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class VorkathVars {

    public final Area.Polygonal ungael = new Area.Polygonal(new Coordinate(2270, 4052, 0), new Coordinate(2274, 4052, 0), new Coordinate(2274, 4044, 0),
            new Coordinate(2279, 4038, 0), new Coordinate(2283, 4038, 0), new Coordinate(2283, 4037, 0),
            new Coordinate(2278, 4033, 0), new Coordinate(2269, 4037, 0));
    public final Area.Rectangular edgeville = new Area.Rectangular(new Coordinate(3083, 3501, 0), new Coordinate(3099, 3486, 0));
    public final Area.Rectangular deathsOffice = new Area.Rectangular(new Coordinate(3181, 5723, 0), new Coordinate(3169, 5730, 0));
    public final Area.Rectangular vorkEntrance = new Area.Rectangular(new Coordinate(2269, 4052, 0), new Coordinate(2275, 4048, 0));
    /**
     * BaseStates is to determine the current state the bot is in
     */
    public BaseStates baseState = BaseStates.RESET;
    public BaseStates vorkathState;
    public FightState fightState;
    public FightState specAttack;
    public BaseStates deathState;

    /**
     * Prayer and HitPoint vars
     */
    public int panicHp;
    public int triggerHp;
    public int panicPray;
    public int triggerPray = -1;

    /**
     * Item Tracking and intuitive changing
     * This stores data inputs...
     */
    // utility file for tracking and adjusting ... maybe probably
    // VorkathKillTracker - data class for all kill based data
    public VorkathKillTracker currKillTracker;
    public HashMap<Integer, VorkathKillTracker> killTracker = new HashMap<>(0);

    public int killCount = 0;
    public int tripCount = 0;
    public int currKill = 0;
    public int currTrip = 0;
    public double killsTrip = 0;
    public double killsHour = 0;
    public int avgLootKill = 0;
    public int avgCostKill = 0;
    public int avgProfitKill = 0;
    public int killsLeft = 0;


    /**
     * General Variables
     */
    //reset per trip
    public Coordinate vorkathPos;
    public Area.Rectangular vorkLair;

    public boolean isEmergencyTeleport;
    public boolean isTripStarted = false;
    public boolean isAbleToContinue;

    //Spec specific must be reset when out of spec
    public Coordinate fireBombTarget;
    public boolean isFrozen;
    public boolean isSpecRestore;
    public boolean isSpawnAttacked = false;
    public List<Coordinate> acidPath = new ArrayList<>(0);
    public Coordinate acidGoal;

    //once per fight - reset on loot phase
    public boolean isMeleeSpecUsed = false;
    public int specEnergyStart = -1;

    public boolean isAntifireNeeded = true;
    public boolean isStatBoostNeeded;
    public boolean isTriggerHP;
    public boolean isTriggerPray;
    public boolean isPanicHP;
    public boolean isPanicPray;

    public boolean isInVorkLair;

    public boolean isFightStarted = false;
    public boolean isPreFight = false;
    public boolean isPostFight = false;

    public int houseAltar;
    public String housePool;

    public String[] pools = new String[] { "Restoration pool", "Revitalisation pool", "Rejuvenation pool", "Fancy rejuvenation pool", "Ornate rejuvenation pool" };

    /*
    PPots - 7 + 25% of level rounded down
     */

    /**
     * GUI based variables
     */
    public boolean isStart = false;
    public boolean isAngler = true;
    public boolean isRunePouch = true;
    public boolean isSlayerStaff = true;
    public boolean isRanged = true;
    public boolean isCrossbow = true;
    public boolean isSerpHelm = true;
    public boolean isSpecUsed = false;
    public boolean isStatBoostUsed = true;

    public Pattern hpRestore = Pattern.compile("Manta ray");
    public String hpRestoreString = "Manta ray";
    public Pattern prayRestore = Pattern.compile("Prayer potion");
    public Pattern statBooster = Pattern.compile("Divine ranging potion");
    public String angler = "Anglerfish";
    public String diamondBolts = "Diamond dragon bolts(e)";
    public String rubyBolts = "Ruby dragon bolts(e)";
    public String darts = null;
    public String scales = "Zulrah's scales";
    public String blowpipe = "Toxic blowpipe";

    public Pattern antifire = Pattern.compile("Extended super antifire");
    public Pattern antiVenom = Pattern.compile("Anti-venom+");

    public int antiVenomQty = 0;
    public int hpRestoreQty = 16;
    public int prayRestoreQty = 3;
    public int anglerQty = 3;
    public int statBoostQty = 1;
    public int antifireQty = 1;

    public int rubyBoltsID = 21944;
    public int diamondBoltsID = 21946;

    public GearSet mainGear;
    public GearSet specGear;
    public StopWatch statBoostWatch = new StopWatch();
    public StopWatch antifireWatch = new StopWatch();
    public StopWatch checkGearWatch = new StopWatch();

    public HashMap<String, Integer> perKillAvg = new HashMap<>(0);
    public HashMap<String, Integer> perTripAvg = new HashMap<>(0);
    /*
        Sara/Food
        Antivenom
        Divine Ranging Potion
        Divine Super Combat
        Super Combat
        Ranging potion
        Prayer potion
        Super restore
        Anglerfish

        ruby bolts (e)            9242
        ruby dragon bolts (e)     21944
        diamond bolts (e)         9243
        diamond dragon bolts (e)  21946
     */
}
