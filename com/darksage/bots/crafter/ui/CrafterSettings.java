package com.darksage.bots.crafter.ui;

import com.DarkAPI.common.util.ItemIcons;
import com.DarkAPI.common.util.TextFormatters;
import com.darksage.bots.crafter.Crafter;
import com.darksage.bots.crafter.data.CraftOptions;
import com.darksage.bots.crafter.data.CraftTask;
import com.darksage.bots.crafter.data.CraftType;
import com.darksage.bots.crafter.data.Locations;
import com.runemate.game.api.hybrid.util.Resources;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class CrafterSettings extends VBox implements Initializable {

    Crafter bot;

    @FXML
    private ComboBox<String> ComboBoxCraftType, ComboBoxFilter, ComboBoxLocation;
    @FXML
    private ListView<String> ListViewOptions;
    @FXML
    private ListView<CraftTask> ListViewTasks;
    @FXML
    private ImageView ImageViewItem;
    @FXML
    private Label LabelInfoItem, LabelItemMade;
    @FXML
    private ToggleButton ToggleGoal;
    @FXML
    private CheckBox CheckBoxAmount, CheckBoxLevel;
    @FXML
    private TextField TextFieldAmount, TextFieldLevel;
    @FXML
    private Button ButtonAdd, ButtonEdit, ButtonRemove, ButtonStart;

    private final ObservableList<String> displayOptions = FXCollections.observableArrayList();
    private final ObservableMap<Integer, CraftTask> tasks = FXCollections.observableHashMap();
    private final ObservableList<CraftTask> displayTasks = FXCollections.observableArrayList();

    public CrafterSettings(Crafter bot) {
        this.bot = bot;
        FXMLLoader loader = new FXMLLoader();
        Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream("com/darksage/bots/crafter/ui/Crafter.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load(stream.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    String optionSelected;
    int taskCount = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Top HBox ComboBoxes
        ComboBoxCraftType.getItems().addAll(FXCollections.observableArrayList(CraftType.getChoices()));
        ComboBoxCraftType.setOnAction(onCraftType());
        ComboBoxFilter.setVisible(false);
        ComboBoxFilter.getItems().addAll(FXCollections.observableArrayList(Arrays.asList("ring", "necklace", "amulet", "bracelet", "Sapphire", "Ruby", "Emerald", "Diamond", "Dragonstone", "Onyx", "Zenyte", "Opal", "Jade", "Topaz", "Gold")));
        ComboBoxFilter.setOnAction(onFilter());
        ComboBoxLocation.setVisible(false);
        ComboBoxLocation.getItems().addAll(FXCollections.observableArrayList(Locations.getChoices()));
        ComboBoxLocation.setOnAction(onLocation());
        // Item to make stuff
        toggleGoalNodes(false);

        TextFieldAmount.setTextFormatter(TextFormatters.getIntegerFormatter());
        TextFieldLevel.setTextFormatter(TextFormatters.getIntegerFormatter());
        ToggleGoal.setOnAction(onToggleGoal());

        displayTasks.addListener((ListChangeListener<? super CraftTask>) c -> {
            ListViewTasks.getItems().setAll(displayTasks);
        });

        ListViewOptions.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.RIGHT) {
                optionSelected = event.getText();
                updateItemSelected();
            }
        });

        ListViewOptions.setOnMouseClicked(event -> {
            if (event.getClickCount() >= 2) {
                optionSelected = ListViewOptions.getSelectionModel().getSelectedItem();
                updateItemSelected();
            }
        });

        TextFieldLevel.setEditable(false);
        TextFieldAmount.setEditable(false);
        CheckBoxAmount.setOnAction(onAmountAction());
        CheckBoxLevel.setOnAction(onLevelAction());

        ButtonAdd.setOnAction(onButtonAdd());
        ButtonStart.setOnAction(onButtonStart());
        ButtonEdit.setOnAction(onButtonEdit());

        ButtonAdd.setVisible(false);

        //Temp
        ButtonEdit.setVisible(false);
        ButtonRemove.setVisible(false);

        ListViewTasks.setCellFactory(ListView -> new CrafterTaskListViewCell());
    }

    private EventHandler<ActionEvent> onButtonEdit() {
        return (ActionEvent e) -> {
            // edit function(s)
            //set task data to selected task from edit?
        };
    }

    private EventHandler<ActionEvent> onButtonStart() {
        return (ActionEvent e) -> {
            bot.getPlatform().invokeLater(()-> {
                bot.vars.isStart = true;
                bot.vars.taskQueue.putAll(tasks);
            });
        };
    }

    private void updateTasks() {
        Collection<CraftTask> tasks1 = tasks.values();
        if(tasks.size() > 0){
            displayTasks.setAll(tasks1);
        }
    }

    boolean isEdit = false;
    int editTask;

    private EventHandler<ActionEvent> onButtonAdd() {
        return (ActionEvent e) -> {
            CraftTask task = new CraftTask();
            task.setCraftType(option.getType());
            task.setActionType(option.getActionType());
            task.setHasGoal(ToggleGoal.isSelected());
            task.setItemBank(option.getItemBank());
            task.setItemBank2(option.getItemBank2());
            task.setItemInterface(option.getItemInterface());
            task.setMat1(option.getMat1());
            task.setMat2(option.getMat2());
            task.setMat3(option.getMat3());
            task.setMinAmountRequired(option.getMinAmountRequired());
            task.setMaxAmountMade(option.getMaxAmountMade());
            task.setIconId(option.getId());
            if (ToggleGoal.isSelected()) {
                if (CheckBoxLevel.isSelected())
                    task.setGoalLevel(Integer.parseInt(TextFieldLevel.getText()));
                if (CheckBoxAmount.isSelected())
                    task.setGoalAmount(Integer.parseInt(TextFieldAmount.getText()));
            }
            if (option.getActionType() != null && option.getActionType().equals(CraftOptions.ActionType.FURNACE)) {
                task.setBankArea(location.getBank());
                task.setFurnaceArea(location.getFurnace());
                task.setLocation(location.getChoice());
            }
            taskCount = tasks.size() + 1;
            tasks.put(taskCount, task);
            updateTasks();
        };
    }

    private EventHandler<ActionEvent> onLevelAction() {
        return (ActionEvent e) -> {
            TextFieldLevel.setEditable(CheckBoxLevel.isSelected());
        };
    }

    private EventHandler<ActionEvent> onAmountAction() {
        return (ActionEvent e) -> {
            TextFieldAmount.setEditable(CheckBoxAmount.isSelected());
        };
    }

    CraftOptions option;
    private void updateItemSelected() {
        option = CraftOptions.getValues(optionSelected);
        LabelItemMade.setText(optionSelected);
        BufferedImage image = ItemIcons.get(option.getId());
        if (image != null)
            ImageViewItem.setImage(SwingFXUtils.toFXImage(image, null));
        ButtonAdd.setVisible(true);

    }

    Locations location;
    private EventHandler<ActionEvent> onLocation() {
        return (ActionEvent event) -> {
            String locale = ComboBoxLocation.getSelectionModel().getSelectedItem();
            if (locale != null)
                location = Locations.getValues(locale);
        };
    }

    String filter = null;
    private EventHandler<ActionEvent> onFilter() {
        return (ActionEvent event) -> {
            filter = ComboBoxFilter.getSelectionModel().getSelectedItem();
            updateOptions();
        };
    }

    private EventHandler<ActionEvent> onToggleGoal() {
        return (ActionEvent event) -> {
            toggleGoalNodes(ToggleGoal.isSelected());
            if (ToggleGoal.isSelected()) {
                ToggleGoal.setTextFill(Color.GREEN);
            } else {
                resetToggleNodes();
            }
        };
    }

    private void toggleGoalNodes(boolean value) {
        CheckBoxAmount.setVisible(value);
        CheckBoxLevel.setVisible(value);
        TextFieldAmount.setVisible(value);
        TextFieldLevel.setVisible(value);
    }

    private void resetToggleNodes() {
        CheckBoxLevel.setSelected(false);
        CheckBoxAmount.setSelected(false);
        TextFieldLevel.setText("");
        TextFieldAmount.setText("");
        TextFieldAmount.setEditable(false);
        TextFieldAmount.setEditable(false);
        ToggleGoal.setSelected(false);
        ToggleGoal.setTextFill(Color.RED);
    }

    CraftType craftType;
    private EventHandler<ActionEvent> onCraftType() {
        return (ActionEvent event) -> {
            String type = ComboBoxCraftType.getSelectionModel().getSelectedItem();
            if (type != null) {
                craftType = CraftType.getValues(type);
                if (craftType.isJewelry()) {
                    ComboBoxFilter.setVisible(true);
                    ComboBoxLocation.setVisible(true);
                } else {
                    ComboBoxFilter.setVisible(false);
                    ComboBoxLocation.setVisible(false);
                }
                updateOptions();
            }
        };
    }

    private void updateOptions() {
        displayOptions.clear();
        if (craftType != null) {
            if (filter != null && craftType.isJewelry()) {
                displayOptions.addAll(Arrays.asList(CraftOptions.getChoices(craftType, filter)));
            } else {
                displayOptions.addAll(Arrays.asList(CraftOptions.getChoices(craftType)));
            }
            updateListView();
        }
    }

    private void updateListView() {
        ListViewOptions.getItems().clear();
        ListViewOptions.getItems().addAll(displayOptions);
    }
}
