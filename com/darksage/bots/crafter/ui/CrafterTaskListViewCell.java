package com.darksage.bots.crafter.ui;

import com.DarkAPI.common.util.ItemIcons;
import com.darksage.bots.crafter.data.CraftTask;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;

import java.awt.image.BufferedImage;

public class CrafterTaskListViewCell extends ListCell<CraftTask> {
    public void updateItem(CraftTask task, boolean isEmpty) {
        super.updateItem(task, isEmpty);
        if (isEmpty || task == null) {
            setText(null);
            setGraphic(null);
        } else {
            if (task.isHasGoal()) {
                if (task.getGoalAmount() > 0) {
                    setText(task.getItemBank() + " x " + task.getGoalAmount());
                } else if (task.getGoalLevel() > 0) {
                    setText(task.getItemBank() + " until lvl " + task.getGoalLevel());
                } else {
                    setText(task.getItemBank());
                }
            } else {
                setText(task.getItemBank());
            }
            setGraphicTextGap(3.0);
            BufferedImage image = ItemIcons.get(task.getIconId());
            ImageView imageView = new ImageView();
            if (image != null)
                imageView.setImage(SwingFXUtils.toFXImage(image, null));
            setGraphic(imageView);
        }
    }
}
