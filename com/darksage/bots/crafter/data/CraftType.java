package com.darksage.bots.crafter.data;

import java.util.Arrays;

public enum CraftType {

    JEWELRY("Jewelry", 1, true, true, true, false, false, false, false),
    GEMS("Gem Cutting", 2, false, false, false, true, false, false, false),
    LEATHER("Leather", 3, false, false, false, false, true, false, false),
    GLASS("Glass", 4, false, false, false, false, false, true, false),
    STAFF("Battlestaff", 5, false, false, false, false, false, false, true);

    private final String choice;
    private final int craftType;
    private final boolean isJewelry;
    private final boolean isJewelryType;
    private final boolean isLocation;
    private final boolean isGems;
    private final boolean isLeather;
    private final boolean isGlass;
    private final boolean isStaff;

    CraftType(String choice, int craftType, boolean isJewelry,boolean isJewelryType, boolean isLocation,boolean isGems,boolean isLeather, boolean isGlass, boolean isStaff){
        this.choice=choice;
        this.craftType=craftType;
        this.isJewelry=isJewelry;
        this.isJewelryType=isJewelryType;
        this.isLocation=isLocation;
        this.isGems=isGems;
        this.isLeather=isLeather;
        this.isGlass=isGlass;
        this.isStaff=isStaff;
    }

    public String getChoice() {
        return choice;
    }

    public int getCraftType() {
        return craftType;
    }

    public boolean isJewelry() {
        return isJewelry;
    }

    public boolean isJewelryType() {
        return isJewelryType;
    }

    public boolean isLocation() {
        return isLocation;
    }

    public boolean isGems() {
        return isGems;
    }

    public boolean isLeather() {
        return isLeather;
    }

    public boolean isGlass() {
        return isGlass;
    }

    public boolean isStaff() {
        return isStaff;
    }

    public static String[] getChoices(){
        return Arrays.stream(values()).map(CraftType::getChoice).toArray(String[]::new);
    }

    public static CraftType getValues(String choice){
        return Arrays.stream(values()).filter((val) -> val.getChoice().equals(choice)).findFirst().orElse(null);
    }
}
