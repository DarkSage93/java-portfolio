package com.darksage.bots.crafter.data;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

import java.util.Arrays;

public enum Locations {

    AL_KHARID("Al-Kharid", new Area.Circular(new Coordinate(3275, 3186, 0), 5),
            new Area.Circular(new Coordinate(3270, 3167, 0),5)),
    PORT_PHASMY("Port Phasmy", new Area.Circular(new Coordinate(3685, 3480, 0), 5),
            new Area.Circular(new Coordinate(3689, 3468, 0),5)),
    EDGEVILLE("Edgeville", new Area.Circular(new Coordinate(3108, 3499, 0), 5),
            new Area.Circular(new Coordinate(3096,3494,0),5));

    private final String choice;
    private final Area.Circular furnace;
    private final Area.Circular bank;


    Locations(String choice, Area.Circular furnace, Area.Circular bank) {
        this.choice=choice;
        this.furnace=furnace;
        this.bank=bank;
    }

    public String getChoice() {
        return choice;
    }

    public Area.Circular getBank() {
        return bank;
    }

    public Area.Circular getFurnace() {
        return furnace;
    }

    public static String[] getChoices(){
        return Arrays.stream(values()).map(Locations::getChoice).toArray(String[]::new);
    }

    public static Locations getValues(String local){
        return Arrays.stream(values()).filter((val)->val.getChoice().equals(local)).findFirst().orElse(null);
    }
}
