package com.darksage.bots.crafter.data;

import com.runemate.game.api.hybrid.location.Area;

import java.util.HashMap;

public class CrafterVars {

    public HashMap<Integer, CraftTask> taskQueue = new HashMap<>(0);

    public int taskCounter = 0;
    public int taskBank = -1;
    public int itemID = -1;
    public int itemCounter = 0;

    public boolean isStart = false;

    // everything below is old

    public final String BUCKET = "bucket";
    public final String CHISEL = "Chisel";
    public final String CRUSHED = "Crushed gem";
    public final String STAFF = "Battlestaff";
    public final String NEEDLE = "Needle";
    public final String THREAD = "Thread";

    public int craftType = 0;
    public int quantity = 0;
    public int minAmount = 0;
    public int maxMakable = 0;

    public String itemMade;
    public String itemMadeType;
    public String itemBank;
    public String mould;
    public String matOne;
    public String matTwo;
    public String location;

    public boolean isFurnaceUsed = false;
    public boolean isNoted = false;

    public Area.Circular bankArea;
    public Area.Circular furnaceArea;

}
