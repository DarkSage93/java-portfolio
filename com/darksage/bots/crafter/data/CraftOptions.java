package com.darksage.bots.crafter.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum CraftOptions {

    LEATHER_GLOVES("Leather gloves", 1059, CraftType.LEATHER, "Leather gloves", "Leather glove", "Leather", "Needle", "Thread", 1),
    LEATHER_BOOTS("Leather boots", 1061, CraftType.LEATHER, "Leather boots", "Leather boots", "Leather", "Needle", "Thread", 1),
    LEATHER_VAMB("Leather vambraces", 1063, CraftType.LEATHER, "Leather vambraces", "Leather vambraces", "Leather", "Needle", "Thread", 1),
    LEATHER_CHAPS("Leather chaps", 1095, CraftType.LEATHER, "Leather chaps", "Leather chaps", "Leather", "Needle", "Thread", 1),
    LEATHER_BODY("Leather body", 1129, CraftType.LEATHER, "Leather body", "Leather body", "Leather", "Needle", "Thread", 1),
    LEATHER_COWL("Leather cowl", 1167, CraftType.LEATHER, "Leather cowl", "Leather cowl", "Leather", "Needle", "Thread", 1),
    LEATHER_COIF("Leather coif", 1169, CraftType.LEATHER, "Coif", "Leather coif", "Leather", "Needle", "Thread", 1),
    HARDLEATHER_BODY("Hardleather body", 1131, CraftType.LEATHER, "Hardleather body", "Hardleather body", "Hard leather", "Needle", "Thread", 1),
    GREEN_VAMB("Green vambraces", 1065, CraftType.LEATHER, "Green dragonhide vambraces", "Green d'hide vamb", "Green dragon leather", "Needle", "Thread", 1),
    GREEN_CHAPS("Green chaps", 1099, CraftType.LEATHER, "Green dragonhide chaps", "Green d'hide chaps", "Green dragon leather", "Needle", "Thread", 2),
    GREEN_BODY("Green body", 1135, CraftType.LEATHER, "Green dragonhide body", "Green d'hide body", "Green dragon leather", "Needle", "Thread", 3),
    BLUE_VAMB("Blue vambraces", 2487, CraftType.LEATHER, "Blue dragonhide vambraces", "Blue d'hide vamb", "Blue dragon leather", "Needle", "Thread", 1),
    BLUE_CHAPS("Blue chaps", 2493, CraftType.LEATHER, "Blue dragonhide chaps", "Blue d'hide chaps", "Blue dragon leather", "Needle", "Thread", 2),
    BLUE_BODY("Blue body", 2499, CraftType.LEATHER, "Blue dragonhide body", "Blue d'hide body", "Blue dragon leather", "Needle", "Thread", 3),
    RED_VAMB("Red vambraces", 2489, CraftType.LEATHER, "Red dragonhide vambraces", "Red d'hide vamb", "Red dragon leather", "Needle", "Thread", 1),
    RED_CHAPS("Red chaps", 2495, CraftType.LEATHER, "Red dragonhide chaps", "Red d'hide chaps", "Red dragon leather", "Needle", "Thread", 2),
    RED_BODY("Red body", 2501, CraftType.LEATHER, "Red dragonhide body", "Red d'hide body", "Red dragon leather", "Needle", "Thread", 3),
    BLACK_VAMB("Black vambraces", 2491, CraftType.LEATHER, "Black dragonhide vambraces", "Black d'hide vamb", "Black dragon leather", "Needle", "Thread", 1),
    BLACK_CHAPS("Black chaps", 2497, CraftType.LEATHER, "Black dragonhide chaps", "Black d'hide chaps", "Black dragon leather", "Needle", "thread", 2),
    BLACK_BODY("Black body", 2503, CraftType.LEATHER, "Black dragonhide body", "black d'hide body", "Black dragon leather", "Needle", "Thread", 3),
    SNAKE_BODY("Snakeskin body", 6322, CraftType.LEATHER, "Snakeskin body", "Snakeskin body", "Snakeskin", "Needle", "Thread", 15, 11),
    SNAKE_CHAPS("Snakeskin chaps", 6324, CraftType.LEATHER, "Snakeskin chaps", "Snakeskin chaps", "Snakeskin", "Needle", "Thread", 12, 14),
    SNAKE_VAMBS("Snakeskin vambraces", 6330, CraftType.LEATHER, "Snakeskin vambraces", "Snakeskin vambraces", "Snakeskin", "Needle", "Thread", 8, 18),
    SNAKE_BANDANA("Snakeskin bandana", 6326, CraftType.LEATHER, "Snakeskin bandana", "Snakeskin bandana", "Snakeskin", "Needle", "Thread", 5, 21),
    SNAKE_BOOTS("Snakeskin boots", 6328, CraftType.LEATHER, "Snakeskin boots", "Snakeskin boots", "Snakeskin", "Needle", "Thread", 6, 20),
    BEER_GLASS("Beer glass", 1919, CraftType.GLASS, "Beer glass", "Beer glass", "Molten glass", "Glassblowing pipe"),
    CANDLE_LANTERN("Candle lantern", 4527, CraftType.GLASS, "Candle lantern", "Empty candle lantern", "Molten glass", "Glassblowing pipe"),
    OIL_LAMP("Oil lamp", 4525, CraftType.GLASS, "Oil lamp", "Empty oil lamp", "Molten glass", "Glassblowing pipe"),
    VIAL("Vials", 229, CraftType.GLASS, "Vial", "Vial", "Molten glass", "Glassblowing pipe"),
    FISH_BOWL("Fish bowls", 6667, CraftType.GLASS, "Fish bowl", "Empty fish bowl", "Molten glass", "Glassblowing pipe"),
    ORB("Orbs", 567, CraftType.GLASS, "Unpowered staff orb", "Unpowered orb", "Molten glass", "Glassblowing pipe"),
    LANTERN_LENS("Lantern lens", 4542, CraftType.GLASS, "Lantern lens", "Lantern lens", "Molten glass", "Glassblowing pipe"),
    LIGHT_ORBS("Light orbs", 10980, CraftType.GLASS, "Light orb", "Empty light orb", "Molten glass", "Glassblowing pipe"),
    MOLTEN_GLASS("Molten glass", 1775, CraftType.GLASS, ActionType.FURNACE, "Molten glass", "Molten glass", "Bucket", "Bucket of sand", "Soda ash"),
    SAPPHIRE("Sapphires", 1607, CraftType.GEMS, "Uncut sapphire", "Sapphire", "Uncut sapphire", "Chisel"),
    EMERALD("Emeralds", 1605, CraftType.GEMS, "Uncut emerald", "Emerald", "Uncut emerald", "Chisel"),
    RUBY("Rubies", 1603, CraftType.GEMS, "Uncut ruby", "Ruby", "Uncut ruby", "Chisel"),
    DIAMOND("Diamonds", 1601, CraftType.GEMS, "Uncut diamond", "Diamond", "Uncut diamond", "Chisel"),
    DRAGONSTONE("Dragonstones", 1615, CraftType.GEMS, "Uncut dragonstone", "Dragonstone", "Uncut dragonstone", "Chisel"),
    ONYX("Onyxes", 6573, CraftType.GEMS, "Uncut onyx", "Onyx", "Uncut onyx", "Chisel"),
    ZENYTE("Zenytes", 19493, CraftType.GEMS, "Uncut zenyte", "Zenyte", "Uncut zenyte", "Chisel"),
    OPAL("Opals", 1609, CraftType.GEMS, "Uncut opal", "Opal", "Uncut opal", "Chisel"),
    JADE("Jades", 1611, CraftType.GEMS, "Uncut jade", "Jade", "Uncut jade", "Chisel"),
    TOPAZ("Topaz", 1613, CraftType.GEMS, "Uncut red topaz", "Red topaz", "Uncut red topaz", "Chisel"),
    AIR_STAFF("Air staff", 1397, CraftType.STAFF, "Air battlestaff", "Air battlestaff", "Air orb", "Battlestaff"),
    WATER_STAFF("Water staff", 1395, CraftType.STAFF, "Water battlestaff", "Water battlestaff", "Water orb", "Battlestaff"),
    EARTH_STAFF("Earth staff", 1399, CraftType.STAFF, "Earth battlestaff", "Earth battlestaff", "Earth orb", "Battlestaff"),
    FIRE_STAFF("Fire staff", 1393, CraftType.STAFF, "Fire battlestaff", "Fire battlestaff", "Fire orb", "Battlestaff"),
    GOLD_R("Gold rings", 1635, CraftType.JEWELRY, ActionType.FURNACE, "Gold ring", "Gold ring", "Gold bar", "Ring mould"),
    GOLD_N("Gold necklaces", 1654, CraftType.JEWELRY, ActionType.FURNACE, "Gold necklace", "Gold necklace", "Gold bar", "Necklace mould"),
    GOLD_A("Gold amulets", 1673, CraftType.JEWELRY, ActionType.FURNACE, "Gold amulet (u)", "Gold amulet (u)", "Gold bar", "Amulet mould"),
    GOLD_B("Gold bracelets", 11069, CraftType.JEWELRY, ActionType.FURNACE, "Gold bracelet", "Gold bracelet", "Gold bar", "Bracelet mould"),
    SAPPHIRE_R("Sapphire rings", 1637, CraftType.JEWELRY, ActionType.FURNACE, "Sapphire ring", "Sapphire ring", null, "Gold bar", "Sapphire", "Ring mould"),
    SAPPHIRE_N("Sapphire necklaces", 1656, CraftType.JEWELRY, ActionType.FURNACE, "Sapphire necklace", "Sapphire necklace", null, "Gold bar", "Sapphire", "Necklace mould"),
    SAPPHIRE_A("Sapphire amulets", 1675, CraftType.JEWELRY, ActionType.FURNACE, "Sapphire amulet (u)", "Sapphire amulet (u)", null, "Gold bar", "Sapphire", "Amulet mould"),
    SAPPHIRE_B("Sapphire bracelets", 11072, CraftType.JEWELRY, ActionType.FURNACE, "Sapphire bracelet", "Sapphire bracelet", null, "Gold bar", "Sapphire", "Bracelet mould"),
    EMERALD_R("Emerald rings", 1639, CraftType.JEWELRY, ActionType.FURNACE, "Emerald ring", "Emerald ring", null, "Gold bar", "Emerald", "Ring mould"),
    EMERALD_N("Emerald necklaces", 1658, CraftType.JEWELRY, ActionType.FURNACE, "Emerald necklace", "Emerald necklace", null, "Gold bar", "Emerald", "Necklace mould"),
    EMERALD_A("Emerald amulets", 1677, CraftType.JEWELRY, ActionType.FURNACE, "Emerald amulet (u)", "Emerald amulet (u)", null, "Gold bar", "Emerald", "Amulet mould"),
    EMERALD_B("Emerald bracelets", 11076, CraftType.JEWELRY, ActionType.FURNACE, "Emerald bracelet", "Emerald bracelet", null, "Gold bar", "Emerald", "Bracelet mould"),
    RUBY_R("Ruby rings", 1641, CraftType.JEWELRY, ActionType.FURNACE, "Ruby ring", "Ruby ring", null, "Gold bar", "Ruby", "Ring mould"),
    RUBY_N("Ruby necklaces", 1660, CraftType.JEWELRY, ActionType.FURNACE, "Ruby necklace", "Ruby necklace", null, "Gold bar", "Ruby", "Necklace mould"),
    RUBY_A("Ruby amulets", 1679, CraftType.JEWELRY, ActionType.FURNACE, "Ruby amulet (u)", "Ruby amulet (u)", null, "Gold bar", "Ruby", "Amulet mould"),
    RUBY_B("Ruby bracelets", 11085, CraftType.JEWELRY, ActionType.FURNACE, "Ruby bracelet", "Ruby bracelet", null, "Gold bar", "Ruby", "Bracelet mould"),
    DIAMOND_R("Diamond rings", 1643, CraftType.JEWELRY, ActionType.FURNACE, "Diamond ring", "Diamond ring", null, "Gold bar", "Diamond", "Ring mould"),
    DIAMOND_N("Diamond necklaces", 1662, CraftType.JEWELRY, ActionType.FURNACE, "Diamond necklace", "Diamond necklace", null, "Gold bar", "Diamond", "Necklace mould"),
    DIAMOND_A("Diamond amulets", 1681, CraftType.JEWELRY, ActionType.FURNACE, "Diamond amulet (u)", "Diamond amulet (u)", null, "Gold bar", "Diamond", "Amulet mould"),
    DIAMOND_B("Diamond bracelets", 11092, CraftType.JEWELRY, ActionType.FURNACE, "Diamond bracelet", "Diamond bracelet", null, "Gold bar", "Diamond", "Bracelet mould"),
    DRAGONSTONE_R("Dragonstone rings", 1645, CraftType.JEWELRY, ActionType.FURNACE, "Dragonstone ring", "Dragonstone ring", null, "Gold bar", "Dragonstone", "Ring mould"),
    DRAGONSTONE_A("Dragonstone amulets", 1683, CraftType.JEWELRY, ActionType.FURNACE, "Dragonstone amulet (u)", "Dragonstone amulet (u)", null, "Gold bar", "Dragonstone", "Amulet mould"),
    DRAGONSTONE_B("Dragonstone bracelets", 11115, CraftType.JEWELRY, ActionType.FURNACE, "Dragonstone bracelet", "Dragonstone bracelet", null, "Gold bar", "Dragonstone", "Bracelet mould"),
    ONYX_R("Onyx rings", 6575, CraftType.JEWELRY, ActionType.FURNACE, "Onyx ring", "Onyx ring", null, "Gold bar", "Onyx", "Ring mould"),
    ONYX_N("Onyx necklaces", 6577, CraftType.JEWELRY, ActionType.FURNACE, "Onyx necklace", "Onyx necklace", null, "Gold bar", "Onyx", "Necklace mould"),
    ONYX_A("Onyx amulets", 6579, CraftType.JEWELRY, ActionType.FURNACE, "Onyx amulet (u)", "Onyx amulet (u)", null, "Gold bar", "Onyx", "Amulet mould"),
    ONYX_B("Onyx bracelets", 11130, CraftType.JEWELRY, ActionType.FURNACE, "Onyx bracelet", "Onyx bracelet", null, "Gold bar", "Onyx", "Bracelet mould"),
    ZENYTE_R("Zenyte rings", 19538, CraftType.JEWELRY, ActionType.FURNACE, "Zenyte ring", "Zenyte ring", null, "Gold bar", "Zenyte", "Ring mould"),
    ZENYTE_N("Zenyte necklaces", 19535, CraftType.JEWELRY, ActionType.FURNACE, "Zenyte necklace", "Zenyte necklace", null, "Gold bar", "Zenyte", "Necklace mould"),
    ZENYTE_A("Zenyte amulets", 19501, CraftType.JEWELRY, ActionType.FURNACE,  "Zenyte amulet (u)", "Zenyte amulet (u)", null, "Gold bar", "Zenyte", "Amulet mould"),
    ZENYTE_B("Zenyte bracelets", 19532, CraftType.JEWELRY, ActionType.FURNACE, "Zenyte bracelet", "Zenyte bracelet", null, "Gold bar", "Zenyte", "Bracelet mould"),
    OPAL_R("Opal rings", 21081, CraftType.JEWELRY, ActionType.FURNACE, "Opal ring", "Opal ring", null, "Silver bar", "Opal", "Ring mould"),
    OPAL_N("Opal necklaces", 21090, CraftType.JEWELRY, ActionType.FURNACE, "Opal necklace", "Opal necklace", null, "Silver bar", "Opal", "Necklace mould"),
    OPAL_A("Opal amulets", 21099, CraftType.JEWELRY, ActionType.FURNACE, "Opal amulet (u)", "Opal amulet (u)", null, "Silver bar", "Opal", "Amulet mould"),
    OPAL_B("Opal bracelets", 21117, CraftType.JEWELRY, ActionType.FURNACE, "Opal bracelet", "Opal bracelet", null, "Silver bar", "Opal", "Bracelet mould"),
    JADE_R("Jade rings", 21084, CraftType.JEWELRY, ActionType.FURNACE, "Jade ring", "Jade ring", null, "Silver bar", "Jade", "Ring mould"),
    JADE_N("Jade necklaces", 21093, CraftType.JEWELRY, ActionType.FURNACE, "Jade necklace", "Jade necklace", null, "Silver bar", "Jade", "Necklace mould"),
    JADE_A("Jade amulets", 21102, CraftType.JEWELRY, ActionType.FURNACE, "Jade amulet (u)", "Jade amulet (u)", null, "Silver bar", "Jade", "Amulet mould"),
    JADE_B("Jade bracelets", 21120, CraftType.JEWELRY, ActionType.FURNACE, "Jade bracelet", "Jade bracelet", null, "Silver bar", "Jade", "Bracelet mould"),
    TOPAZ_R("Topaz rings", 21087, CraftType.JEWELRY, ActionType.FURNACE, "Topaz ring", "Topaz ring", null, "Silver bar", "Red topaz", "Ring mould"),
    TOPAZ_N("Topaz necklaces", 21096, CraftType.JEWELRY, ActionType.FURNACE, "Topaz necklace", "Topaz necklace", null, "Silver bar", "Red topaz", "Necklace mould"),
    TOPAZ_A("Topaz amulets", 21105, CraftType.JEWELRY, ActionType.FURNACE, "Topaz amulet (u)", "Topaz amulet (u)", null, "Silver bar", "Red topaz", "Amulet mould"),
    TOPAZ_B("Topaz bracelet", 21123, CraftType.JEWELRY, ActionType.FURNACE, "Topaz bracelet", "Topaz bracelet", null, "Silver bar", "Red topaz", "Bracelet mould"),

    ;

    private final String choice;
    private final int id;
    private final CraftType type;
    private final ActionType actionType;
    private final String itemInterface;
    private final String itemBank;
    private final String itemBank2;
    private final String mat1;
    private final String mat2;
    private final String mat3;
    private final int minAmountRequired;
    private final int maxAmountMade;

    CraftOptions(String choice, int id, CraftType type, ActionType actionType, String itemInterface, String itemBank, String itemBank2, String mat1, String mat2, String mat3) {
        this(choice, id, type, actionType, itemInterface, itemBank, itemBank2, mat1, mat2, mat3, -1, -1);
    }

    CraftOptions(String choice, int id, CraftType type, ActionType actionType, String itemInterface, String itemBank, String mat1, String mat2) {
        this(choice, id, type, actionType, itemInterface, itemBank, null, mat1, mat2);
    }

    CraftOptions(String choice, int id, CraftType type, String itemInterface, String itemBank, String mat1, String mat2) {
        this(choice, id, type, ActionType.BANK_STAND, itemInterface, itemBank, null, mat1, mat2, null, -1, -1);
    }

    CraftOptions(String choice, int id, CraftType type, ActionType actionType, String itemInterface, String itemBank, String itemBank2, String mat1, String mat2) {
        this(choice, id, type, actionType, itemInterface, itemBank, itemBank2, mat1, mat2, null, -1, -1);
    }

    CraftOptions(String choice, int id, CraftType type, String itemInterface, String itemBank, String mat1, String mat2, String mat3, int minAmountRequired) {
        this(choice, id, type, ActionType.BANK_STAND, itemInterface, itemBank, null, mat1, mat2, mat3, minAmountRequired, 0);
    }

    CraftOptions(String choice, int id, CraftType type, String itemInterface, String itemBank, String mat1, String mat2, String mat3, int minAmountRequired, int maxAmountMade) {
        this(choice, id, type, ActionType.BANK_STAND, itemInterface, itemBank, null, mat1, mat2, mat3, minAmountRequired, maxAmountMade);
    }

    CraftOptions(String choice, int id, CraftType type, ActionType actionType, String itemInterface, String itemBank, String itemBank2, String mat1, String mat2, String mat3, int minAmountRequired, int maxAmountMade) {
        this.choice = choice;
        this.id = id;
        this.type = type;
        this.actionType = actionType;
        this.itemInterface = itemInterface;
        this.itemBank = itemBank;
        this.itemBank2 = itemBank2;
        this.mat1 = mat1;
        this.mat2 = mat2;
        this.mat3 = mat3;
        this.minAmountRequired = minAmountRequired;
        this.maxAmountMade = maxAmountMade;
    }

    public static String[] getChoices() {
        return Arrays.stream(values()).map(CraftOptions::getChoice).toArray(String[]::new);
    }

    public static List<CraftOptions> getFilteredByType(CraftType type) {
        List<CraftOptions> temp = new ArrayList<>();
        for (CraftOptions craftOptions : values()) {
            CraftType val = craftOptions.getType();
            if (val.equals(type)) {
                temp.add(craftOptions);
            }
        }
        return temp;
    }

    public static String[] getChoices(CraftType type) {
        return Arrays.stream(values()).filter((val) -> val.getType().equals(type)).map(CraftOptions::getChoice).toArray(String[]::new);
    }

    public static String[] getChoices(CraftType type, String filter) {
        List<CraftOptions> start = getFilteredByType(type);
        return start.stream().filter((val) -> val.getItemBank().contains(filter)).map(CraftOptions::getChoice).toArray(String[]::new);
    }

    public static CraftOptions getValues(String choice) {
        return Arrays.stream(values()).filter((val) -> val.getChoice().equals(choice)).findFirst().orElse(null);
    }

    public CraftType getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public int getMaxAmountMade() {
        return maxAmountMade;
    }

    public int getMinAmountRequired() {
        return minAmountRequired;
    }

    public String getChoice() {
        return choice;
    }

    public String getItemBank() {
        return itemBank;
    }

    public String getItemBank2() {
        return itemBank2;
    }

    public String getItemInterface() {
        return itemInterface;
    }

    public String getMat1() {
        return mat1;
    }

    public String getMat2() {
        return mat2;
    }

    public String getMat3() {
        return mat3;
    }

    public enum ActionType {
        FURNACE,
        BANK_STAND,
    }
}
