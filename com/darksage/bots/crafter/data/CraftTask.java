package com.darksage.bots.crafter.data;

import com.runemate.game.api.hybrid.location.Area;

public class CraftTask {

    private int goalLevel;
    private int goalAmount;
    private int minAmountRequired;
    private int maxAmountMade;
    private int bankQuantity;
    private int iconId;

    private boolean hasGoal;
    private boolean isNotedBanking;

    private String mat1;
    private String mat2;
    private String mat3;
    private String location;
    private String itemBank;
    private String itemBank2;
    private String itemInterface;

    private CraftOptions.ActionType actionType;
    private CraftType craftType;

    private Area bankArea;
    private Area furnaceArea;

    public CraftTask() {

    }

    public String[] getMatsArray() {
        if (mat1 != null && mat2 != null && mat3 != null)
            return new String[] { mat1, mat2, mat3 };
        if (mat1 != null && mat2 != null)
            return new String[] { mat1, mat2 };
        return new String[] { mat1 };
    }

    public int getGoalLevel() {
        return goalLevel;
    }

    public void setGoalLevel(int goalLevel) {
        this.goalLevel = goalLevel;
    }

    public int getGoalAmount() {
        return goalAmount;
    }

    public void setGoalAmount(int goalAmount) {
        this.goalAmount = goalAmount;
    }

    public int getMinAmountRequired() {
        return minAmountRequired;
    }

    public void setMinAmountRequired(int minAmountRequired) {
        this.minAmountRequired = minAmountRequired;
    }

    public int getMaxAmountMade() {
        return maxAmountMade;
    }

    public void setMaxAmountMade(int maxAmountMade) {
        this.maxAmountMade = maxAmountMade;
    }

    public boolean isHasGoal() {
        return hasGoal;
    }

    public void setHasGoal(boolean hasGoal) {
        this.hasGoal = hasGoal;
    }

    public boolean isNotedBanking() {
        return isNotedBanking;
    }

    public void setNotedBanking(boolean notedBanking) {
        isNotedBanking = notedBanking;
    }

    public String getMat1() {
        return mat1;
    }

    public void setMat1(String mat1) {
        this.mat1 = mat1;
    }

    public String getMat2() {
        return mat2;
    }

    public void setMat2(String mat2) {
        this.mat2 = mat2;
    }

    public String getMat3() {
        return mat3;
    }

    public void setMat3(String mat3) {
        this.mat3 = mat3;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getItemBank() {
        return itemBank;
    }

    public void setItemBank(String itemBank) {
        this.itemBank = itemBank;
    }

    public String getItemInterface() {
        return itemInterface;
    }

    public void setItemInterface(String itemInterface) {
        this.itemInterface = itemInterface;
    }

    public CraftOptions.ActionType getActionType() {
        return actionType;
    }

    public void setActionType(CraftOptions.ActionType actionType) {
        this.actionType = actionType;
    }

    public CraftType getCraftType() {
        return craftType;
    }

    public void setCraftType(CraftType craftType) {
        this.craftType = craftType;
    }

    public Area getBankArea() {
        return bankArea;
    }

    public void setBankArea(Area bankArea) {
        this.bankArea = bankArea;
    }

    public Area getFurnaceArea() {
        return furnaceArea;
    }

    public void setFurnaceArea(Area furnaceArea) {
        this.furnaceArea = furnaceArea;
    }

    public int getBankQuantity() {
        return bankQuantity;
    }

    public void setBankQuantity(int bankQuantity) {
        this.bankQuantity = bankQuantity;
    }

    public String getItemBank2() {
        return itemBank2;
    }

    public void setItemBank2(String itemBank2) {
        this.itemBank2 = itemBank2;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}
