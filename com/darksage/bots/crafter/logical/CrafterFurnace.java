package com.darksage.bots.crafter.logical;

import com.DarkAPI.framework.treeTasks.checks.DarkIsItemSelected;
import com.DarkAPI.framework.treeTasks.checks.DarkIsMakeAll;
import com.DarkAPI.framework.treeTasks.executable.*;
import com.DarkAPI.util.Delay;
import com.darksage.bots.crafter.Crafter;
import com.darksage.bots.crafter.data.CraftTask;
import com.regal.regal_bot.tree_tasks.bank.IsBankOpen;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.Banks;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

import java.util.HashMap;
import java.util.regex.Pattern;

public class CrafterFurnace extends BranchTask {

    private final Crafter bot = (Crafter) Environment.getBot();
    private CraftTask current;

    @Override
    public boolean validate() {
        current = bot.vars.taskQueue.get(bot.vars.taskCounter);
        return Inventory.containsAllOf(current.getMatsArray());
    }

    @Override
    public TreeTask successTask() {
        if (Bank.isOpen())
            return new DarkBankTask(false, true);
        if (current.getFurnaceArea().contains(bot.getPlayer())) {
            if (current.getCraftType().isGlass())
                return new DarkIsMakeAll(0, current.getItemInterface(), Delay.animating4T(current.getMat1()),
                        new DarkIsItemSelected(current.getMat1(), new DarkGameObject(Delay.ticks1(), new String[]{ "Furnace" })));
            if (!Interfaces.newQuery().containers(446, 6).visible().results().isEmpty())
                return craftAll();
            return new DarkGameObject(Delay.ticks1(), new String[]{ "Furnace" });
        } else {
            GameObject furnace = GameObjects.newQuery().names("Furnace").results().nearest();
            if (furnace != null && furnace.isVisible())
                return new DarkGameObject(Delay.untilArea(current.getFurnaceArea()), new String[]{ "Furnace" });
            return new DarkTraversal(current.getFurnaceArea(), current.getLocation() + " Furnace Area", bot);
        }
    }

    @Override
    public TreeTask failureTask() {
        HashMap<Pattern, Integer> withdraw = setWithdraw();
        HashMap<Pattern, Integer> deposits = setDeposit();
        if (Bank.isOpen()) {
            if (bot.vars.taskBank != bot.vars.taskCounter) {
                bot.vars.taskBank = bot.vars.taskCounter;
                return newTaskDeposit();
            }
            if (!isOutOfItems()) {
                bot.vars.taskCounter++;
                return new DarkEmptyLeaf("We are out of items for current Task at : " + (bot.vars.taskCounter - 1));
            }
            return bank(withdraw, deposits);
        }
        return openBank(withdraw, deposits);
    }

    private TreeTask newTaskDeposit() {
        return new DarkBankTask(true, true, false);
    }

    private boolean isOutOfItems() {
        boolean weHaveItems = Bank.contains(current.getMat1()) || Inventory.contains(current.getMat1());
        if (!Bank.contains(current.getMat2()) && !Inventory.contains(current.getMat2()))
            weHaveItems = false;
        if (current.getMat3() != null && !Bank.contains(current.getMat3()) && !Inventory.contains(current.getMat3()))
            weHaveItems = false;
        return weHaveItems;
    }

    private TreeTask openBank(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> deposit) {
        LocatableEntity bank = Banks.newQuery().results().first();
        if (bank != null && bank.isVisible())
            return new DarkGameObject(Delay.bankOpen(), new String[]{ "Bank booth", "Bank chest", "Grand Exchange booth"});
        return new IsBankOpen(bank(withdraw, deposit));
    }

    private HashMap<Pattern, Integer> setDeposit() {
        HashMap<Pattern, Integer> temp = new HashMap<>(0);
        temp.put(toPattern(current.getItemBank()), 0);
        if (current.getItemBank2() != null)
            temp.put(toPattern(current.getItemBank2()), 0);
        return temp;
    }

    private HashMap<Pattern, Integer> setWithdraw() {
        HashMap<Pattern, Integer> temp = new HashMap<>(0);
        if (current.getCraftType().isGlass()) {
            temp.put(toPattern(current.getMat1()), 14);
            temp.put(toPattern(current.getMat2()), 14);
            return temp;
        }
        if (current.getMatsArray().length == 2) {
            temp.put(toPattern(current.getMat2()), 1);
            temp.put(toPattern(current.getMat1()), 0);
        } else {
            temp.put(toPattern(current.getMat1()), 13);
            temp.put(toPattern(current.getMat2()), 13);
            temp.put(toPattern(current.getMat3()), 1);
        }
        return temp;
    }

    private static Pattern toPattern(String text) {
        return Regex.getPatternForExactString(text);
    }

    private TreeTask bank(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> deposits) {
        return new DarkBankTask(withdraw, withdraw, deposits, false, false, Bank.DefaultQuantity.X, bot);
    }

    private TreeTask craftAll() {
        if (current.getMat1().equals("Gold bar"))
            return new DarkInterfaceHandler(446, "Make " + current.getItemBank(), null, Delay.animating4T(current.getMat1()));
        return new DarkInterfaceHandler(6, current.getItemBank(), "Craft", Delay.animating4T(current.getMat1()));
    }
}
