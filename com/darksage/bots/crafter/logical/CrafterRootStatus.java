package com.darksage.bots.crafter.logical;

import com.DarkAPI.framework.treeTasks.executable.DarkEmptyLeaf;
import com.darksage.bots.crafter.Crafter;
import com.darksage.bots.crafter.data.CraftOptions;
import com.darksage.bots.crafter.data.CraftTask;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

public class CrafterRootStatus extends BranchTask {

    private final Crafter bot = (Crafter) Environment.getBot();

    @Override
    public boolean validate() {
        return isStart();
    }

    @Override
    public TreeTask successTask() {
        if (bot.vars.taskQueue.size() >= bot.vars.taskCounter) {
            CraftTask task = bot.vars.taskQueue.get(bot.vars.taskCounter);
            if (task != null) {
                bot.vars.itemID = task.getIconId();
                if (task.isHasGoal()) {
                    if (task.getGoalLevel() > 0) {
                        if (Skill.CRAFTING.getBaseLevel() >= task.getGoalLevel()) {
                            incrementTask();
                            return new DarkEmptyLeaf("We have reached the goal level for task : " + task.getItemBank() + " until level " + task.getGoalLevel());
                        }
                    } else if (task.getGoalAmount() > 0) {
                        if (bot.vars.itemCounter >= task.getGoalAmount()) {
                            incrementTask();
                            return new DarkEmptyLeaf("we have reached the goal amount for task : " + task.getItemBank() + " x " + task.getGoalAmount());
                        }
                    }
                }
                if (bot.vars.taskQueue.get(bot.vars.taskCounter).getActionType().equals(CraftOptions.ActionType.FURNACE))
                    return new CrafterFurnace();
                return new CrafterBankStanding();
            } else {
                incrementTask();
            }
        } else {
            bot.stop("We have reached end of queue");
        }
        return new DarkEmptyLeaf("we have reached end of queue :  failsafe");
    }

    @Override
    public TreeTask failureTask() {
        return new DarkEmptyLeaf("Please choose your settings, and press start.");
    }

    private void incrementTask() {
        bot.vars.taskCounter++;
        bot.vars.itemCounter = 0;
    }

    private boolean isStart() {
        if (!bot.vars.isStart) {
            Execution.delayUntil(() -> bot.vars.isStart, 30000);
            if (isStopFail())
                bot.stop("We are stopping cause settings weren't set...");
        }
        return bot.vars.isStart;
    }

    private boolean isStopFail() {
        return bot.getUI().getRunTimer().getRuntime() >= 600000;
    }
}
