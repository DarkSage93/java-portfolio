package com.darksage.bots.crafter.logical;

import com.DarkAPI.framework.treeTasks.checks.DarkIsItemSelected;
import com.DarkAPI.framework.treeTasks.checks.DarkIsMakeAll;
import com.DarkAPI.framework.treeTasks.executable.*;
import com.DarkAPI.util.Delay;
import com.darksage.bots.crafter.Crafter;
import com.darksage.bots.crafter.data.CraftTask;
import com.regal.regal_bot.tree_tasks.bank.IsBankOpen;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;

import java.util.HashMap;
import java.util.regex.Pattern;

public class CrafterBankStanding extends BranchTask {

    private final Crafter bot = (Crafter) Environment.getBot();
    private CraftTask current;

    @Override
    public boolean validate() {
        current = bot.vars.taskQueue.get(bot.vars.taskCounter);
        if (isSnakeSkin()) {
            boolean noted = Inventory.contains(6290) && Inventory.getQuantity(6289) >= current.getMinAmountRequired();
            return noted && Inventory.containsAllOf(current.getMat2(), current.getMat3());
        }
        if (current.getCraftType().isLeather())
            return Inventory.containsAllOf(current.getMat2(), current.getMat3()) && Inventory.getQuantity(current.getMat1()) >= current.getMinAmountRequired();
        return Inventory.containsAllOf(current.getMatsArray());
    }

    @Override
    public TreeTask successTask() {
        if (Bank.isOpen()) return new DarkBankTask(false, true);
        if (isSnakeSkin())
            return new DarkIsMakeAll(0, current.getItemInterface(), Delay.animating4T(current.getMat1(), current.getMinAmountRequired()),
                    new DarkIsItemSelected(current.getMat1(), new DarkInventoryItemAction(current.getMat2())));
        return new DarkIsMakeAll(0, current.getItemInterface(), Delay.animating4T(current.getMat1()),
                new DarkIsItemSelected(current.getMat1(), new DarkInventoryItemAction(current.getMat2())));
    }

    @Override
    public TreeTask failureTask() {
        HashMap<Pattern, Integer> deposits = setDeposit();
        HashMap<Pattern, Integer> withdraw = setWithdraw();
        if (Bank.isOpen()) {
            if (bot.vars.taskBank != bot.vars.taskCounter) {
                bot.vars.taskBank = bot.vars.taskCounter;
                return newTaskDeposit();
            }
            if (isSnakeSkin())
                return snakeSkinBanking(withdraw);
            if (!isOutOfItems()) {
                bot.vars.taskCounter++;
                return new DarkEmptyLeaf("We have ran out of items for this task...");
            }
            if (current.getCraftType().isStaff())
                return bankX(withdraw, deposits);
            return bankAll(withdraw, deposits);
        }
        if (current.getCraftType().isStaff())
            return new IsBankOpen(bankX(withdraw, deposits));
        return new IsBankOpen(bankAll(withdraw, deposits));
    }

    private TreeTask snakeSkinBanking(HashMap<Pattern, Integer> withdraw) {
        if (!Inventory.contains(6290)) {
            HashMap<Pattern, Integer> noted = new HashMap<>(0);
            noted.put(toPattern(current.getMat1()), 0);
            return new IsBankOpen(new DarkBankTask(noted, true, false, Bank.DefaultQuantity.ALL, bot));
        }
        if (Inventory.getQuantity(current.getItemBank()) >= current.getMaxAmountMade() || !Inventory.containsAllOf(current.getMat2(), current.getMat3()))
            return new IsBankOpen(bankAll(withdraw, setDeposit()));
        if (Bank.isOpen())
            return new DarkBankTask(false, true);
        if (ChatDialog.isOpen() || !ChatDialog.getOptions().isEmpty())
            return new DarkChatDialog("Yes");
        return new DarkIsItemSelected(6290, new DarkGameObject(Delay.ticks3(), new String[]{"Bank booth", "Bank chest"}));
    }

    private boolean isOutOfItems() {
        boolean weHaveItems = Bank.contains(current.getMat1()) || Inventory.contains(current.getMat1());
        if (!Bank.contains(current.getMat2()) && !Inventory.contains(current.getMat2()))
            weHaveItems = false;
        if (current.getMat3() != null && !Bank.contains(current.getMat3()) && !Inventory.contains(current.getMat3()))
            weHaveItems = false;
        return weHaveItems;
    }

    private TreeTask newTaskDeposit() {
        return new DarkBankTask(true, true, false);
    }

    private TreeTask bankAll(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> deposit) {
        return new DarkBankTask(withdraw, deposit, false, false, Bank.DefaultQuantity.ALL, bot);
    }

    private TreeTask bankX(HashMap<Pattern, Integer> withdraw, HashMap<Pattern, Integer> deposit) {
        return new DarkBankTask(withdraw, withdraw, deposit, false, false, Bank.DefaultQuantity.X, bot);
    }

    private HashMap<Pattern, Integer> setWithdraw() {
        HashMap<Pattern, Integer> temp = new HashMap<>(0);
        switch (current.getCraftType()) {
            case LEATHER:
                if (isSnakeSkin()) {
                    temp.put(toPattern(current.getMat2()), 0);
                    temp.put(toPattern(current.getMat3()), 0);
                    break;
                }
                temp.put(toPattern(current.getMat2()), 0);
                temp.put(toPattern(current.getMat3()), 0);
                temp.put(toPattern(current.getMat1()), 0);
                break;
            case GEMS:
            case GLASS:
                temp.put(toPattern(current.getMat2()), 1);
                temp.put(toPattern(current.getMat1()), 0);
                break;
            case STAFF:
                temp.put(toPattern(current.getMat1()), 14);
                temp.put(toPattern(current.getMat2()), 14);
                break;
        }
        return temp;
    }

    private boolean isSnakeSkin() {
        return current.getCraftType().isLeather() && current.getMat1().equals("Snakeskin");
    }

    private HashMap<Pattern, Integer> setDeposit() {
        HashMap<Pattern, Integer> temp = new HashMap<>(0);
        temp.put(toPattern(current.getItemBank()), 0);
        if (current.getItemBank2() != null)
            temp.put(toPattern(current.getItemBank2()), 0);
        return temp;
    }

    private static Pattern toPattern(String text) {
        return Regex.getPatternForExactString(text);
    }
}
