package com.darksage.bots.crafter;

import com.DarkAPI.data.general.DarkPlayerSense;
import com.darksage.bots.crafter.data.CrafterVars;
import com.darksage.bots.crafter.logical.CrafterRootStatus;
import com.darksage.bots.crafter.ui.CrafterSettings;
import com.regal.regal_bot.Premium;
import com.regal.regal_bot.RegalTreeBot;
import com.regal.regal_bot.tree_tasks.IsPlayerLoaded;
import com.regal.utility.LimitChecker;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import javafx.scene.layout.VBox;

import java.util.concurrent.Callable;

public class Crafter extends RegalTreeBot implements Premium, InventoryListener {

    public CrafterVars vars = new CrafterVars();

    @Override
    public Skill[] setSkillsToTrack() {
        return new Skill[]{Skill.CRAFTING};
    }

    @Override
    public VBox setSettingsVBox() {
        return new CrafterSettings(this);
    }

    @Override
    public TreeTask setRootTask() {
        getPathing().setUseTeleports(true);
        return new IsPlayerLoaded(this, new CrafterRootStatus());
    }

    @Override
    public void onStart(String... args) {
        super.onStart(args);
        getEventDispatcher().addListener(this);
        DarkPlayerSense.initializeKeys();
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        InventoryListener.super.onItemAdded(event);
        if (event.getItem().getId() == vars.itemID)
            vars.itemCounter += event.getQuantityChange();
    }

    @Override
    public Callable<Boolean> ignoreAdditionsWhen() {
        return Bank::isOpen;
    }

    @Override
    public boolean isSafeToLogout() {
        return !isAnimating() && !isMoving();
    }

    @Override
    public long getFreeTime() {
        return LimitChecker.TimeUnit.HOURS.getMilli() * 3;
    }

    @Override
    public long getResetPeriod() {
        return LimitChecker.TimeUnit.DAYS.getMilli() * 7;
    }

    @Override
    public boolean whitelisted(int id) {
        return false;
    }
}
